package org.gcube.informationsystem.utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeVersionTest {

	String[] validVersions = new String[] {"1.0.0", "13.23.45", "1.12.3"};
	String[] inValidVersions = new String[] {"1.0", "01.23.45", "1.02.3", "0.1.0"};
	
	@Test
	public void testPatterns() throws Exception {
		for(String version : validVersions) {
			Version typeVersion = new Version(version);
			Assert.assertTrue(version.compareTo(typeVersion.toString())==0);
		}
		
		for(String version : inValidVersions) {
			try {
				Version typeVersion = new Version(version);
				throw new Exception("The version " + version + " is not valid but the validation succeded. Parsed version is " + typeVersion.toString());
			}catch (RuntimeException e) {
				// OK
			}
		}
		
	}
	
}
