package org.gcube.informationsystem.utils;

import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UUIDManagerTest {

	private static final Logger logger = LoggerFactory.getLogger(UUIDManagerTest.class);
	
	@Test
	public void testReservedUUID() {
		UUIDManager uuidManager = UUIDManager.getInstance();
		
		Set<UUID> allUUID = uuidManager.getAllReservedUUID();
		logger.debug("Reserved UUIDs are {}", allUUID);
		
		Set<String> allUUIDAsString = uuidManager.getAllReservedUUIDAsStrings();
		Assert.assertTrue(allUUID.size()==allUUIDAsString.size());
		Assert.assertTrue(allUUID.size()==16);
		Assert.assertTrue(allUUIDAsString.size()==16);
		for(UUID uuid : allUUID) {
			Assert.assertTrue(uuidManager.isReservedUUID(uuid));
			Assert.assertTrue(uuidManager.isReservedUUID(uuid.toString()));
		}
		
		for(String uuidString : allUUIDAsString) {
			Assert.assertTrue(uuidManager.isReservedUUID(UUID.fromString(uuidString)));
			Assert.assertTrue(uuidManager.isReservedUUID(uuidString));
		}
		
	}
	
	
	@Test
	public void testGenerateUUID() {
		UUIDManager uuidManager = UUIDManager.getInstance();
		
		Set<UUID> allReservedUUID = uuidManager.getAllReservedUUID();
		
		UUID uuid = uuidManager.generateValidUUID();
		
		Assert.assertTrue(!allReservedUUID.contains(uuid));
		
		uuid = UUID.randomUUID();
		
		uuid = uuidManager.generateValidUUID(uuid);
		
		Assert.assertTrue(!allReservedUUID.contains(uuid));
		
		for(UUID reservedUUID : allReservedUUID) {
			uuid = uuidManager.generateValidUUID(reservedUUID);
			Assert.assertTrue(reservedUUID.compareTo(uuid)!=0);
		}
		
	}
	
	
	
}
