package org.gcube.informationsystem.queries.templates;

import java.io.File;
import java.net.URL;

import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.queries.templates.impl.entities.QueryTemplateImpl;
import org.gcube.informationsystem.queries.templates.impl.properties.TemplateVariableImpl;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class QueryTemplateTest {

	private static Logger logger = LoggerFactory.getLogger(QueryTemplateTest.class);
	
	public File getQueryTemplatesDirectory() throws Exception {
		URL logbackFileURL = QueryTemplateTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "queryTemplates");
	}
	
	@Test
	public void test() throws Exception {
		File queryTemplatesDirectory = getQueryTemplatesDirectory();
		File jsonQueryTemplateFile = new File(queryTemplatesDirectory, "queryTemplate" + 1 + ".json");
		ObjectMapper objectMapper = new ObjectMapper();
		JsonNode jsonNode = objectMapper.readTree(jsonQueryTemplateFile);
		
		String name = "Test";
		
		QueryTemplate queryTemplate = new QueryTemplateImpl();
		queryTemplate.setName(name);
		queryTemplate.setDescription("A Test Query Template");
		queryTemplate.setTemplate(jsonNode);
		
		TemplateVariable stateTemplateVariable = new TemplateVariableImpl();
		String stateVariableName = "$state";
		stateTemplateVariable.setName(stateVariableName);
		stateTemplateVariable.setDescription("StateFacet value");
		stateTemplateVariable.setDefaultValue("running");
		stateTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(stateTemplateVariable);
		
		TemplateVariable nameTemplateVariable = new TemplateVariableImpl();
		String nameVariableName = "$name";
		nameTemplateVariable.setName(nameVariableName);
		nameTemplateVariable.setDescription("SoftwareFacet name");
		nameTemplateVariable.setDefaultValue("resource-registry");
		nameTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(nameTemplateVariable);
		
		TemplateVariable groupTemplateVariable = new TemplateVariableImpl();
		String groupVariableName = "$group";
		groupTemplateVariable.setName(groupVariableName);
		groupTemplateVariable.setDescription("SoftwareFacet group");
		groupTemplateVariable.setDefaultValue("information-system");
		groupTemplateVariable.setPropertyType("String");
		queryTemplate.addTemplateVariable(groupTemplateVariable);
		
		String json = ElementMapper.marshal(queryTemplate);
		logger.info("Query Template is {}", json);
		
		QueryTemplate gotQueryTemplate = ElementMapper.unmarshal(QueryTemplate.class, json);
		logger.info("{} {}", QueryTemplate.NAME, ElementMapper.marshal(gotQueryTemplate));
		
		JsonNode gotTemplate = gotQueryTemplate.getTemplate();
		logger.info("Template is {}", gotTemplate.toString());
		
		logger.info("Query with default values {} is {}", queryTemplate.getParamsFromDefaultValues(), queryTemplate.getJsonQuery());
		
		ObjectNode params = objectMapper.createObjectNode();
		params.put(stateVariableName, "running");
		params.put(groupVariableName, "DataAccess");
		params.put(nameVariableName, "StorageHub");
		
		logger.info("Query with values {} is {}", params, queryTemplate.getJsonQuery(params));
		
	}
	
}
