package org.gcube.informationsystem.types.knowledge;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.knowledge.UsageKnowledge;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ModelKnowledgeTest{
	
	private static final Logger logger = LoggerFactory.getLogger(ModelKnowledgeTest.class);
	
	protected File getTypesDirectory() throws Exception {
		URL logbackFileURL = ModelKnowledgeTest.class.getClassLoader().getResource("logback-test.xml");
		File logbackFile = new File(logbackFileURL.toURI());
		File resourcesDirectory = logbackFile.getParentFile();
		return new File(resourcesDirectory, "types");
	}
	
	protected String readFile(File file) throws IOException {
		byte[] encoded = Files.readAllBytes(file.toPath());
		return new String(encoded, Charset.defaultCharset());
	}
	
	@Test
	public void test() throws Exception {
		ModelKnowledge<Type, TypeInformation> modelKnowledge = TypesKnowledge.getInstance().getModelKnowledge();
		
		File typesDirectory = getTypesDirectory();
		
		AccessType[] modelTypes = AccessType.getModelTypes();
		for(AccessType modelType : modelTypes) {
			File file = new File(typesDirectory, modelType.getName() + ".json");
			String json = readFile(file);
			List<Type> types = TypeMapper.deserializeTypeDefinitions(json);
			modelKnowledge.addAllType(types);
			logger.trace("-------------------------------------------------------");
		}
		
		for(AccessType modelType : modelTypes) {
			Tree<Type> tree = modelKnowledge.getTree(modelType);
			
			tree.elaborate(new NodeElaborator<Type>() {
				
				@Override
				public void elaborate(Node<Type> node, int level) throws Exception {
					StringBuffer stringBuffer = new StringBuffer();
					for (int i = 0; i < level; ++i) {
						stringBuffer.append(Node.INDENTATION);
					}
					
					String typeName = node.getNodeElement().getName();
					logger.info("{}- {}{}", stringBuffer.toString(), typeName, level==0 ? " (ROOT)" : "");
					
				}
			});			
		}
		
		logger.info("---------------------------------------------------\n\n\n");
		
		for(AccessType modelType : modelTypes) {
			Tree<Type> tree = modelKnowledge.getTree(modelType);
			UsageKnowledge<?> usageKnowledge = modelKnowledge.getModelTypesUsage(modelType);
			
			tree.elaborate(new NodeElaborator<Type>() {
				
				@Override
				public void elaborate(Node<Type> node, int level) throws Exception {
					StringBuffer stringBuffer = new StringBuffer();
					for (int i = 0; i < level; ++i) {
						stringBuffer.append(Node.INDENTATION);
					}
					
					Type type = node.getNodeElement();
					String typeName = type.getName();
					
					
					if(type.getAccessType()!=AccessType.PROPERTY) {
						@SuppressWarnings("unchecked")
						List<LinkedEntity> usage = (List<LinkedEntity>) usageKnowledge.getUsage(typeName);
						logger.info("{}- {}{} {}", stringBuffer.toString(), typeName, level==0 ? " (ROOT) " : "", usage!=null ? "known static usage "  + usage : "No known static usage");
					}else {
						@SuppressWarnings("unchecked")
						List<Map.Entry<String,PropertyDefinition>> usage = (List<Entry<String, PropertyDefinition>>) usageKnowledge.getUsage(typeName);
						logger.info("{}- {}{} {}", stringBuffer.toString(), typeName, level==0 ? " (ROOT) " : "", usage!=null ? "known static usage "  + usage : "No known static usage");
					}
				}
			});			

			
			tree.elaborate(new NodeElaborator<Type>() {
				
				@Override
				public void elaborate(Node<Type> node, int level) throws Exception {
					StringBuffer stringBuffer = new StringBuffer();
					for (int i = 0; i < level; ++i) {
						stringBuffer.append(Node.INDENTATION);
					}
					
					Type type = node.getNodeElement();
					String typeName = type.getName();
					
					String definition = TypeMapper.serializeTypeDefinition(type);
					
					logger.info("{}- {}{} {}", stringBuffer.toString(), typeName, level==0 ? " (ROOT) " : "", definition);
					
				}
			});			
		}
	}	
}
