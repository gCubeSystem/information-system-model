package org.gcube.informationsystem.types;

import org.gcube.com.fasterxml.jackson.databind.JsonMappingException;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.types.reference.relations.ConsistsOfType;
import org.gcube.informationsystem.types.reference.relations.IsRelatedToType;
import org.gcube.informationsystem.types.reference.relations.RelationType;
import org.gcube.informationsystem.utils.Version;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class SerializationTest {

	private static Logger logger = LoggerFactory.getLogger(SerializationTest.class);
	
	@Test
	public void serialize() throws Exception{
		TypeMapper.serializeType(EntityTest.class);
	}
	
	@Test
	public void serializePropertyDefinition() throws Exception{
		TypeMapper.serializeType(PropertyDefinition.class);
	}
	
	@Test
	public void makeFacetTypeDefinition() throws Exception{
		EntityType facetTypeDefinitionSelf = (EntityType) TypeMapper.createTypeDefinition(FacetType.class);
		logger.info(ElementMapper.marshal(facetTypeDefinitionSelf));
	}
	
	
	@Test
	public void makeResourceTypeDefinition() throws Exception{
		EntityType resourceTypeDefinitionSelf = (EntityType) TypeMapper.createTypeDefinition(ResourceType.class);
		logger.info(ElementMapper.marshal(resourceTypeDefinitionSelf));
	}

	@Test
	public void entityElementDefinition() throws Exception{
		Type type = TypeMapper.createTypeDefinition(EntityElement.class);
		logger.info(ElementMapper.marshal(type));
	}
	
	
	@Test
	public void testPropertyTypeDefinition() throws Exception{
		PropertyType propertyElement = (PropertyType) TypeMapper.createTypeDefinition(PropertyElement.class);
		Assert.assertTrue(propertyElement.getAccessType()==AccessType.PROPERTY_ELEMENT);
		logger.info(ElementMapper.marshal(propertyElement));
		
		PropertyType propertyType = (PropertyType) TypeMapper.createTypeDefinition(PropertyType.class);
		Assert.assertTrue(propertyType.getAccessType()==AccessType.PROPERTY_TYPE);
		logger.info(ElementMapper.marshal(propertyType));
		
		PropertyType propertyDefinition = (PropertyType) TypeMapper.createTypeDefinition(PropertyDefinition.class);
		Assert.assertTrue(propertyDefinition.getAccessType()==AccessType.PROPERTY_DEFINITION);
		logger.info(ElementMapper.marshal(propertyDefinition));
		
		PropertyType linkedEntity = (PropertyType) TypeMapper.createTypeDefinition(LinkedEntity.class);
		Assert.assertTrue(linkedEntity.getAccessType()==AccessType.PROPERTY_ELEMENT);
		logger.info(ElementMapper.marshal(linkedEntity));
		
		PropertyType property = (PropertyType) TypeMapper.createTypeDefinition(Property.class);
		Assert.assertTrue(property.getAccessType()==AccessType.PROPERTY);
		logger.info(ElementMapper.marshal(property));
		
		PropertyType metadataType = (PropertyType) TypeMapper.createTypeDefinition(Metadata.class);
		Assert.assertTrue(metadataType.getAccessType()==AccessType.PROPERTY);
		logger.info(ElementMapper.marshal(metadataType));
		
		PropertyType propagationConstraint = (PropertyType) TypeMapper.createTypeDefinition(PropagationConstraint.class);
		Assert.assertTrue(propagationConstraint.getAccessType()==AccessType.PROPERTY);
		logger.info(ElementMapper.marshal(propagationConstraint));
		
		PropertyType encrypted = (PropertyType) TypeMapper.createTypeDefinition(Encrypted.class);
		Assert.assertTrue(encrypted.getAccessType()==AccessType.PROPERTY);
		logger.info(ElementMapper.marshal(encrypted));
		
		PropertyType templateVariable = (PropertyType) TypeMapper.createTypeDefinition(TemplateVariable.class);
		Assert.assertTrue(templateVariable.getAccessType()==AccessType.PROPERTY_ELEMENT);
		logger.info(ElementMapper.marshal(templateVariable));
	}
	
	@Test
	public void testEntityTypeDefinition() throws Exception{
		EntityType entityElement = (EntityType) TypeMapper.createTypeDefinition(EntityElement.class);
		Assert.assertTrue(entityElement.getAccessType()==AccessType.ENTITY_ELEMENT);
		logger.info(ElementMapper.marshal(entityElement));
		
		EntityType entity = (EntityType) TypeMapper.createTypeDefinition(Entity.class);
		Assert.assertTrue(entity.getAccessType()==AccessType.ENTITY);
		logger.info(ElementMapper.marshal(entity));
		
		ResourceType resource = (ResourceType) TypeMapper.createTypeDefinition(Resource.class);
		Assert.assertTrue(resource.getAccessType()==AccessType.RESOURCE);
		logger.info(ElementMapper.marshal(resource));
		
		FacetType facet = (FacetType) TypeMapper.createTypeDefinition(Facet.class);
		Assert.assertTrue(facet.getAccessType()==AccessType.FACET);
		logger.info(ElementMapper.marshal(facet));
		
		EntityType context = (EntityType) TypeMapper.createTypeDefinition(Context.class);
		Assert.assertTrue(context.getAccessType()==AccessType.CONTEXT);
		logger.info(ElementMapper.marshal(context));
		
		EntityType entityType = (EntityType) TypeMapper.createTypeDefinition(EntityType.class);
		Assert.assertTrue(entityType.getAccessType()==AccessType.ENTITY_TYPE);
		logger.info(ElementMapper.marshal(entityType));
		
		EntityType resourceType = (EntityType) TypeMapper.createTypeDefinition(ResourceType.class);
		Assert.assertTrue(resourceType.getAccessType()==AccessType.ENTITY_TYPE);
		logger.info(ElementMapper.marshal(resourceType));
		
		EntityType facetType = (EntityType) TypeMapper.createTypeDefinition(FacetType.class);
		Assert.assertTrue(facetType.getAccessType()==AccessType.ENTITY_TYPE);
		logger.info(ElementMapper.marshal(facetType));
		
		EntityType queryTemplateType = (EntityType) TypeMapper.createTypeDefinition(QueryTemplate.class);
		Assert.assertTrue(queryTemplateType.getAccessType()==AccessType.QUERY_TEMPLATE);
		logger.info(ElementMapper.marshal(queryTemplateType));
		
	}
	
	@Test
	public void testRelationTypeDefinition() throws Exception{
		@SuppressWarnings("unchecked")
		RelationType<EntityType,EntityType> relation = (RelationType<EntityType,EntityType>) TypeMapper.createTypeDefinition(Relation.class);
		Assert.assertTrue(relation.getAccessType()==AccessType.RELATION);
		logger.info(ElementMapper.marshal(relation));
		
		IsRelatedToType isRelatedTo = (IsRelatedToType) TypeMapper.createTypeDefinition(IsRelatedTo.class);
		Assert.assertTrue(isRelatedTo.getAccessType()==AccessType.IS_RELATED_TO);
		logger.info(ElementMapper.marshal(isRelatedTo));
		
		ConsistsOfType consistsOf = (ConsistsOfType) TypeMapper.createTypeDefinition(ConsistsOf.class);
		Assert.assertTrue(consistsOf.getAccessType()==AccessType.CONSISTS_OF);
		logger.info(ElementMapper.marshal(consistsOf));
		
		@SuppressWarnings("unchecked")
		RelationType<EntityType,EntityType> isParentOf = (RelationType<EntityType,EntityType>) TypeMapper.createTypeDefinition(IsParentOf.class);
		Assert.assertTrue(isParentOf.getAccessType()==AccessType.IS_PARENT_OF);
		logger.info(ElementMapper.marshal(isParentOf));
		
		@SuppressWarnings("unchecked")
		RelationType<EntityType,EntityType> relationType = (RelationType<EntityType,EntityType>) TypeMapper.createTypeDefinition(RelationType.class);
		Assert.assertTrue(relationType.getAccessType()==AccessType.RELATION_TYPE);
		logger.info(ElementMapper.marshal(relationType));

		@SuppressWarnings("unchecked")
		RelationType<EntityType,EntityType> isRelatedToType = (RelationType<EntityType,EntityType>) TypeMapper.createTypeDefinition(IsRelatedToType.class);
		Assert.assertTrue(isRelatedToType.getAccessType()==AccessType.RELATION_TYPE);
		logger.info(ElementMapper.marshal(isRelatedToType));
		
		@SuppressWarnings("unchecked")
		RelationType<EntityType,EntityType> consistsOfType = (RelationType<EntityType,EntityType>) TypeMapper.createTypeDefinition(ConsistsOfType.class);
		Assert.assertTrue(consistsOfType.getAccessType()==AccessType.RELATION_TYPE);
		logger.info(ElementMapper.marshal(consistsOfType));
	}
	
	@Test
	public void testGetEnumcostants(){
		Class<?> clz = PropagationConstraint.RemoveConstraint.class;
		
		Object[] constants = clz.getEnumConstants();
		for(Object constant : constants){
			logger.trace("{}", constant.toString());
		}
		
	}
	
	@Test
	public void testTypeSerialization() throws Exception {
		String serialized = TypeMapper.serializeType(Metadata.class);
		logger.info(serialized);
		
		PropertyType propertyType = (PropertyType) TypeMapper.deserializeTypeDefinition(serialized);
		Version typeVersion = propertyType.getVersion();
		logger.debug("Version {}", typeVersion.toString());
		logger.info(ElementMapper.marshal(propertyType));
		
		String json = "{\"" + Element.TYPE_PROPERTY + "\":\"PropertyType\",\"metadata\":null,\"name\":\"Metadata\",\"description\":\"This class provides metadata per every IdentifiableElement\",\"typeSuperTypes\":[\"Property\"],\"properties\":[{\"" + Element.TYPE_PROPERTY + "\":\"PropertyDefinition\",\"name\":\"creationTime\",\"description\":\"Creation time. It represents the difference, measured in milliseconds, between the creation time and midnight, January 1, 1970, UTC.\",\"mandatory\":true,\"readonly\":true,\"notnull\":true,\"max\":null,\"min\":null,\"regexp\":null,\"propertyType\":\"Date\"},{\"" + Element.TYPE_PROPERTY + "\":\"PropertyDefinition\",\"name\":\"lastUpdateTime\",\"description\":\"Last Update time. At creation time it assumes the same value of creationTime. It represents the difference, measured in milliseconds, between the creation time and midnight, January 1, 1970, UTC.\",\"mandatory\":true,\"readonly\":false,\"notnull\":true,\"max\":null,\"min\":null,\"regexp\":null,\"propertyType\":\"Date\"},{\"" + Element.TYPE_PROPERTY + "\":\"PropertyDefinition\",\"name\":\"createdBy\",\"description\":\"The user that created the Entity or the Relation. It is initialized at creation time. \",\"mandatory\":true,\"readonly\":true,\"notnull\":true,\"max\":null,\"min\":null,\"regexp\":null,\"propertyType\":\"String\"},{\"" + Element.TYPE_PROPERTY + "\":\"PropertyDefinition\",\"name\":\"" + IdentifiableElement.ID_PROPERTY + "\",\"description\":\"This ID is be used to identify the Entity or the Relation univocally.\",\"mandatory\":true,\"readonly\":true,\"notnull\":true,\"max\":null,\"min\":null,\"regexp\":\"^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}$\",\"propertyType\":\"String\"},{\"" + Element.TYPE_PROPERTY + "\":\"PropertyDefinition\",\"name\":\"lastUpdateBy\",\"description\":\"The user that made the last update to the Entity or the Relation. At creation time, it assumes the same value of creator.\",\"mandatory\":true,\"readonly\":false,\"notnull\":true,\"max\":null,\"min\":null,\"regexp\":null,\"propertyType\":\"String\"}],\"accessType\":\"PROPERTY\",\"abstract\":false,\"version\":\"1.0.0\",\"changelog\":{\"1.0.0\":\"First Version\"}}";
		logger.info(json);

		PropertyType metadataType = (PropertyType) TypeMapper.deserializeTypeDefinition(json);
		logger.info(ElementMapper.marshal(metadataType));
		
	}
	
	@TypeMetadata(name = TestVersionNotInChangelog.NAME, description = "This type is a test to check version compliancy", version = "2.1.0")
	@Change(version = "1.1.0", description = "A test of the declared version which is not in changelog")
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
	@Final
	interface TestVersionNotInChangelog extends Property {
		
		public static final String NAME = "TestVersionNotInChangelog"; // TestVersionNotInChangelog.class.getSimpleName();
		
	}
	
	@TypeMetadata(name = TestVersionNotHighest.NAME, description = "This type is a test to check version compliancy", version = "1.0.0")
	@Change(version = "1.1.0", description = "A test of the declared version which is not the highest in changelog")
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
	@Final
	interface TestVersionNotHighest extends Property {
		
		public static final String NAME = "TestVersionNotHighest"; // TestVersionNotHighest.class.getSimpleName();
		
	}
	
	@Test(expected = RuntimeException.class)
	public void testVersionNotInChangelog() throws Exception{
		TypeMapper.serializeType(TestVersionNotInChangelog.class);
	}
	
	@Test(expected = RuntimeException.class)
	public void testVersionNotHighest() throws Exception{
		TypeMapper.serializeType(TestVersionNotHighest.class);
	}
	
	public static final String PG_OK = "{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascadeWhenOrphan\"}";
	public static final String PG_NO = "{\"type\":\"PropagationConstraint\",\"add\":\"propagate\",\"delete\":\"propagate\",\"remove\":\"cascadeWhenOrphan\"}";
	public static final String PG_EXT = "{\"type\":\"MyPropagationConstraint\",\"supertypes\":[\"PropagationConstraint\",\"Property\"], \"add\":\"propagate\",\"delete\":\"cascade\",\"remove\":\"cascadeWhenOrphan\"}";
	
	@Test
	public void testPropagationConstraintMarshalling() throws Exception {
		ElementMapper.unmarshal(PropagationConstraint.class, PG_OK);
		ElementMapper.unmarshal(PropagationConstraint.class, PG_EXT);
	}
	
	@Test(expected = JsonMappingException.class)
	public void testWrongPropagationConstraintMarshalling() throws Exception {
		ElementMapper.unmarshal(PropagationConstraint.class, PG_NO);
	}
	
}
