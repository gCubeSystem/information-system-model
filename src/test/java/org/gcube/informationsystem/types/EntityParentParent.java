package org.gcube.informationsystem.types;

import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.annotations.ISProperty;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface EntityParentParent extends Facet {

	@ISProperty(name="asd", description="desc")
	public String getAs();
	
	
	
}
