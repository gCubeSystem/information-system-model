package org.gcube.informationsystem.types;

import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.Encrypted;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeNameTest {

	private static Logger logger = LoggerFactory.getLogger(TypeNameTest.class);
	
	@Test
	public void getBaseType() throws Exception {
		Class<?> clz = Metadata.class;
		BaseType baseType = PropertyTypeName.getBaseTypeByClass(clz);
		logger.debug("{} for class {} is {}", BaseType.class.getSimpleName(), clz, baseType);
	}
	
	@Test
	public void testValidBaseType() throws Exception {
		for(BaseType baseType : BaseType.values()) {
			BaseType b = BaseType.getBaseTypeFromString(baseType.toString());
			logger.trace("{} == {}", baseType, b);
			Assert.assertTrue(baseType == b);
		}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testInvaliBaseType() throws Exception {
		try {
			BaseType.getBaseTypeFromString("Map<String, String>");
		}catch (IllegalArgumentException e) {
			logger.debug("As expect got {} trying to convert invalid string value to {} Enum. {}", IllegalArgumentException.class.getSimpleName(), BaseType.class.getSimpleName(), e.getMessage());
			throw e;
		}
	}
	
	private void checkPropertyTypeName(String value, BaseType expectedBaseType) {
		// logger.debug("Going to convert {} string as {}", value, BasePropertyType.class.getSimpleName());
		PropertyTypeName basePropertyType = new PropertyTypeName(value);
		BaseType b = basePropertyType.getBaseType();
		Assert.assertTrue(expectedBaseType == b);
		Assert.assertTrue(value.compareTo(basePropertyType.toString())==0);
		if(basePropertyType.isGenericType()) {
			logger.debug("{} : BaseType {} - Generic {}", value, basePropertyType.getBaseType(), basePropertyType.getGenericBaseType()!=null ? basePropertyType.getGenericBaseType().toString() : basePropertyType.getGenericClassName());
		}else {
			logger.debug("{} == {}", value, basePropertyType.toString());
		}
		
	}
	
	@Test
	public void testBasePropertType() {
		for(BaseType baseType : BaseType.values()) {
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append(baseType.toString());
			if(baseType.ordinal()>=BaseType.PROPERTY.ordinal()) {
				if(baseType == BaseType.PROPERTY) {
					stringBuffer = new StringBuffer();
					stringBuffer.append(Encrypted.NAME);
					checkPropertyTypeName(stringBuffer.toString(), baseType);
				} else {
					for(BaseType b : BaseType.values()) {
						if(b.ordinal()>BaseType.PROPERTY.ordinal()) {
							continue;
						}
						StringBuffer sb = new StringBuffer();
						sb.append(stringBuffer);
						sb.append("<");
						if(baseType == BaseType.MAP) {
							sb.append(BaseType.STRING.toString());
							sb.append(",");
						}
						
						if(b == BaseType.PROPERTY) {
							sb.append(Encrypted.NAME);
						} else {
							sb.append(b.toString());
						}
						sb.append(">");
						checkPropertyTypeName(sb.toString(), baseType);
					}
				}
			} else {
				checkPropertyTypeName(stringBuffer.toString(), baseType);
			}
		}
	}
	
	protected void assertInvalidBasePropertyType(String value, BaseType expectedBaseType) throws Exception {
		try {
			checkPropertyTypeName(value, expectedBaseType);
			throw new Exception(value + " should not be a valid value");
		}catch (IllegalArgumentException e) {
			logger.debug("{} is not a valid value as expected : {}", value, e.getMessage());
			// This is what we want
		} 
	}
	
	@Test
	public void testInvalidBasePropertType() throws Exception {
		for(BaseType baseType : BaseType.values()) {
			StringBuffer stringBuffer = new StringBuffer();
			stringBuffer.append(baseType.toString());
			if(baseType.ordinal()>BaseType.PROPERTY.ordinal()) {
				stringBuffer.append("<");
				switch (baseType) {
					case MAP:
						stringBuffer.append(BaseType.BYTE.toString());
						stringBuffer.append(",");
						stringBuffer.append(BaseType.STRING.toString());
						stringBuffer.append(">");
						break;
						
					case SET: case LIST:
						stringBuffer.append(BaseType.SET.toString());
						stringBuffer.append(">");
						break;
						
					default:
						break;
				}
				
				
				assertInvalidBasePropertyType(stringBuffer.toString(), baseType);
				
				
			} else {
				for(BaseType b : BaseType.values()) {
					StringBuffer sb = new StringBuffer();
					sb.append(stringBuffer);
					sb.append("<");
					sb.append(b.toString());
					sb.append(">");
					assertInvalidBasePropertyType(sb.toString(), baseType);
				}
			}
		}
	}
}
