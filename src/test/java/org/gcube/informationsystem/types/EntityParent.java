package org.gcube.informationsystem.types;

import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@TypeMetadata(name = "EntityParent", description = "This is a test Entity", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface EntityParent extends EntityParentParent {
	
	@ISProperty(nullable=false)
	public String getNullableP();
	
	@ISProperty(mandatory=false)
	public String getMandatoryP();
	
	@ISProperty(name="different", description="desc")
	public String getMynameP();
	
}
