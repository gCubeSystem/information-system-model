/**
 * 
 */
package org.gcube.informationsystem.types;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class EntitySchemaDefinition {

	private static Logger logger = LoggerFactory.getLogger(EntitySchemaDefinition.class);

	@Test
	public void test() throws Exception {
		Class<? extends PropertyElement> clz = Metadata.class;
		String json = TypeMapper.serializeType(clz);
		logger.trace(json);
	}
	
	@Test
	public void testRelationSerialization() throws Exception {
		Class<? extends Element> clz = IsRelatedTo.class;
		String json = TypeMapper.serializeType(clz);
		logger.trace(json);
	}
	
	@Test
	public void testResourceSerialization() throws Exception {
		Class<? extends Resource> clz = Resource.class;
		String json = TypeMapper.serializeType(clz);
		logger.trace(json);
	}
}
