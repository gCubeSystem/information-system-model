package org.gcube.informationsystem.discovery;

import java.util.Map;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.knowledge.Knowledge;
import org.gcube.informationsystem.discovery.knowledge.ModelKnowledge;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.utils.Version;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DiscoveryTest {

	private static final Logger logger = LoggerFactory.getLogger(DiscoveryTest.class);
	
	@Test
	public void testDiscovery() throws Exception {
		ModelKnowledge modelKnowledge = Knowledge.getInstance().getAllKnowledge();
		
		AccessType[] accessTypes = AccessType.getModelTypes();
		for(AccessType accessType : accessTypes) {
			Tree<Class<Element>> classesTree = modelKnowledge.getClassesTree(accessType);
			logger.info("Classes tree for {} is\n{}", accessType.getName(), classesTree.toString());
		}
	}
	
	private void checkType(Type type, Type expected) {
		Assert.assertTrue(type.getName().compareTo(expected.getName())==0);
		Assert.assertTrue(type.getVersion().compareTo(expected.getVersion())==0);
		Assert.assertTrue(type.getDescription().compareTo(expected.getDescription())==0);
		Assert.assertTrue(type.isAbstract() == expected.isAbstract());
		
		Assert.assertTrue(type.getExtendedTypes().containsAll(expected.getExtendedTypes()));
		Assert.assertTrue(expected.getExtendedTypes().containsAll(type.getExtendedTypes()));
		
		
		Map<Version, String> typeChangelog = type.getChangelog();
		Map<Version, String> typeManagedChangelog = expected.getChangelog();
		
		Assert.assertTrue(typeChangelog.keySet().containsAll(typeManagedChangelog.keySet()));
		Assert.assertTrue(typeManagedChangelog.keySet().containsAll(typeChangelog.keySet()));
		
		for(Version typeVersion : typeChangelog.keySet()) {
			Assert.assertTrue(typeChangelog.get(typeVersion).compareTo(typeManagedChangelog.get(typeVersion))==0);
		}
		
		if(type.getProperties()!=null || expected.getProperties()!=null) {
			Assert.assertTrue(type.getProperties().containsAll(expected.getProperties()));
			Assert.assertTrue(expected.getProperties().containsAll(type.getProperties()));
		}
	}
	
	@Test
	public void testcheckType() throws Exception {
		Class<? extends Element> clz = PropertyType.class;
		Type type = TypeMapper.createTypeDefinition(clz);
		Type expected = TypeMapper.createTypeDefinition(PropertyType.class);
		checkType(type, expected);
		
		String json = TypeMapper.serializeType(clz);
		expected = TypeMapper.deserializeTypeDefinition(json);
		
		checkType(type, expected);
	}
	
	@Test
	public void testTypeDefinition() throws Exception {
		Class<? extends Element> clz = PropertyType.class;
		Type type = TypeMapper.createTypeDefinition(clz);
		Assert.assertTrue(type.getName().compareTo(PropertyType.NAME)==0);
		for(PropertyDefinition propertyDefinition : type.getProperties()) {
			if(propertyDefinition.getName().compareTo(PropertyType.PROPERTIES_PROPERTY)==0) {
				logger.debug("{}", propertyDefinition);
				Assert.assertTrue(propertyDefinition.getPropertyType().compareTo("Set<PropertyDefinition>")==0);
			}
		}
		String typeDefinitionJsonString = TypeMapper.serializeTypeDefinition(type);
		logger.debug(typeDefinitionJsonString);
	}
}
