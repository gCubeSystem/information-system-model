package org.gcube.informationsystem.contexts;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TestsForContext {

	private static Logger logger = LoggerFactory.getLogger(TestsForContext.class);
	
	@Test
	public void testSerialization() throws Exception {
		Context gcube = new ContextImpl("gcube");
		String s = gcube.toString();
		logger.debug(s);
		gcube = ElementMapper.unmarshal(Context.class, s);
		logger.debug(gcube.toString());
		gcube = (Context) ElementMapper.unmarshal(Element.class, s);
		logger.debug(gcube.toString());
		
		Context devsec = new ContextImpl("devsec");
		devsec.setParent(gcube);
		logger.debug(devsec.toString());
		
		Context devVRE = new ContextImpl("devVRE");
		devVRE.setParent(devsec);
		logger.debug(devVRE.toString());
		
		Context devNext = new ContextImpl("devNext");
		devNext.setParent(gcube);
		logger.debug(devNext.toString());
		
		Context nextNext = new ContextImpl("NextNext");
		nextNext.setParent(devNext);
		logger.debug(nextNext.toString());
	}
	
}
