package org.gcube.informationsystem.model.impl.properties;

import java.util.Calendar;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PropagationConstraintTest {
	
	private static Logger logger = LoggerFactory.getLogger(PropagationConstraintTest.class);
	
	@Test
	public void testPropagationConstraint() throws Exception {
		PropagationConstraint propagationConstraint = new PropagationConstraintImpl();
		propagationConstraint.setAddConstraint(AddConstraint.propagate);
		propagationConstraint.setRemoveConstraint(RemoveConstraint.cascadeWhenOrphan);
		propagationConstraint.setDeleteConstraint(DeleteConstraint.cascade);
		String pg = ElementMapper.marshal(propagationConstraint);
		
		Property property = ElementMapper.unmarshal(Property.class, pg);
		Assert.assertTrue(property instanceof PropagationConstraint);
		logger.debug(ElementMapper.marshal(property));
		
		PropagationConstraint pgUnm  = ElementMapper.unmarshal(PropagationConstraint.class, pg);
		Assert.assertTrue(property instanceof PropagationConstraint);
		logger.debug(ElementMapper.marshal(pgUnm));
	}
	
	@Test
	public void testPropagationConstraintByString() throws Exception {
		AddConstraint addConstraint = AddConstraint.propagate;
		RemoveConstraint removeConstraint = RemoveConstraint.cascade;
		DeleteConstraint deleteConstraint = DeleteConstraint.keep;
		
		PropagationConstraint pc = new PropagationConstraintImpl();
		pc.setAddConstraint(addConstraint);
		pc.setRemoveConstraint(removeConstraint);
		pc.setDeleteConstraint(deleteConstraint);
				
		String json = ElementMapper.marshal(pc);
		logger.debug(json);
		
		Property property = ElementMapper.unmarshal(Property.class, json);
		Assert.assertTrue(property instanceof PropagationConstraint);
		Assert.assertTrue(((PropagationConstraint) property).getAddConstraint().compareTo(addConstraint)==0);
		Assert.assertTrue(((PropagationConstraint) property).getRemoveConstraint().compareTo(removeConstraint)==0);
		Assert.assertTrue(((PropagationConstraint) property).getDeleteConstraint().compareTo(deleteConstraint)==0);
		logger.debug(ElementMapper.marshal(property));
		
		PropagationConstraint propagationConstraint = ElementMapper.unmarshal(PropagationConstraint.class, json);
		Assert.assertTrue(propagationConstraint instanceof PropagationConstraint);
		Assert.assertTrue(propagationConstraint.getAddConstraint().compareTo(addConstraint)==0);
		Assert.assertTrue(propagationConstraint.getRemoveConstraint().compareTo(removeConstraint)==0);
		Assert.assertTrue(propagationConstraint.getDeleteConstraint().compareTo(deleteConstraint)==0);
		logger.debug(ElementMapper.marshal(property));
	}
	
	@Test
	public void testPropagationConstraintSpecilization() throws Exception {
		AddConstraint addConstraint = AddConstraint.propagate;
		RemoveConstraint removeConstraint = RemoveConstraint.cascade;
		DeleteConstraint deleteConstraint = DeleteConstraint.keep;
		String marshalled = "{\"" + 
				Element.TYPE_PROPERTY + "\":\"MyPropagationConstraint\",\"" + 
				ERElement.SUPERTYPES_PROPERTY + "\":[\"" + PropagationConstraint.NAME + "\", \"" + Property.NAME + "\"],\"" + 
				PropagationConstraint.ADD_PROPERTY + "\":\""+ addConstraint + "\",\""+ 
				PropagationConstraint.REMOVE_PROPERTY + "\":\"" + removeConstraint + "\",\""+ 
				PropagationConstraint.DELETE_PROPERTY + "\":\"" + deleteConstraint +
				"\"}";
		logger.debug(marshalled);
		
		Property property = ElementMapper.unmarshal(Property.class, marshalled);
		Assert.assertTrue(property instanceof PropagationConstraint);
		Assert.assertTrue(((PropagationConstraint) property).getAddConstraint().compareTo(addConstraint)==0);
		Assert.assertTrue(((PropagationConstraint) property).getRemoveConstraint().compareTo(removeConstraint)==0);
		Assert.assertTrue(((PropagationConstraint) property).getDeleteConstraint().compareTo(deleteConstraint)==0);
		
		logger.debug(ElementMapper.marshal(property));
		
		PropagationConstraint propagationConstraint = ElementMapper.unmarshal(PropagationConstraint.class, marshalled);
		Assert.assertTrue(propagationConstraint instanceof PropagationConstraint);
		Assert.assertTrue(propagationConstraint.getAddConstraint().compareTo(addConstraint)==0);
		Assert.assertTrue(propagationConstraint.getRemoveConstraint().compareTo(removeConstraint)==0);
		Assert.assertTrue(propagationConstraint.getDeleteConstraint().compareTo(deleteConstraint)==0);
		logger.debug(ElementMapper.marshal(property));
	}
	
	@Test
	public void testPropagationConstraintSpecilizationInside() throws Exception {
		AddConstraint addConstraint = AddConstraint.propagate;
		RemoveConstraint removeConstraint = RemoveConstraint.cascade;
		DeleteConstraint deleteConstraint = DeleteConstraint.keep;
		
		String pcString = "{" +
				"\"" + Element.TYPE_PROPERTY + "\":\"My" + PropagationConstraint.NAME + "\"," +
				"\"" + ERElement.SUPERTYPES_PROPERTY + "\":[\"" + PropagationConstraint.NAME + "\", \"" + Property.NAME + "\"],\"" + 
				PropagationConstraint.ADD_PROPERTY + "\":\""+ addConstraint + "\",\"" +
				PropagationConstraint.REMOVE_PROPERTY + "\":\"" + removeConstraint + "\",\"" +
				PropagationConstraint.DELETE_PROPERTY + "\":\"" + deleteConstraint + "\"" +
		"}";
		logger.debug(pcString);
		
		MetadataImpl metadata = new MetadataImpl();
		metadata.createdBy = "luca.frosini";
		metadata.lastUpdateBy = "luca.frosini";
		metadata.creationTime = Calendar.getInstance().getTime();
		metadata.lastUpdateTime = Calendar.getInstance().getTime();
		
		String hString = ElementMapper.marshal(metadata);
		
		
		String consistsOfString = "{" +
			"\"" + Element.TYPE_PROPERTY + "\":\"" + ConsistsOf.NAME + "\"," +
			"\"" + ConsistsOf.PROPAGATION_CONSTRAINT_PROPERTY + "\":"+ pcString + "," +
			"\"" + ConsistsOf.METADATA_PROPERTY + "\": " + hString + "," +
			"\"" + ConsistsOf.SOURCE_PROPERTY + "\":{" +
				"\"" + IdentifiableElement.ID_PROPERTY + "\":\"4a971915-ca90-48cf-9975-63ee2dd28605\"," +
				"\"" + Element.TYPE_PROPERTY + "\":\"MyEService\"," +
				"\"" + ERElement.SUPERTYPES_PROPERTY + "\":[\"EService\", \"Service\", \"Resource\"]," +
				"\"" + ConsistsOf.METADATA_PROPERTY + "\": " + hString +
				"}," +
			"\"" + ConsistsOf.TARGET_PROPERTY + "\":{" +
				"\"" + IdentifiableElement.ID_PROPERTY + "\":\"3ace4bd0-e5cd-49a3-97a8-a0a9468ce6d4\"," +
				"\"" + Element.TYPE_PROPERTY + "\":\"MySoftwareFacet\"," +
				"\"" + ERElement.SUPERTYPES_PROPERTY + "\":[\"SoftwareFacet\", \"Facet\"]," +
				"\"" + ConsistsOf.METADATA_PROPERTY + "\": " + hString + "," + 
				"\"name\":\"WhnManager\"," +
				"\"group\":\"VREManagement\"," +
				"\"version\":\"2.0.0-SNAPSHOT\"," +
				"\"description\":\"Web Hosting Node Service\"," +
				"\"qualifier\":null,\"optional\":false" +
			"}" + 
		"}";
		logger.debug("--- {}", consistsOfString);
		
		@SuppressWarnings("unchecked")
		ConsistsOf<Resource,Facet> consistsOf = ElementMapper.unmarshal(ConsistsOf.class, consistsOfString);
		Assert.assertTrue(consistsOf instanceof ConsistsOf);
		
		PropagationConstraint propagationConstraint = consistsOf.getPropagationConstraint();
		Assert.assertTrue(propagationConstraint instanceof PropagationConstraint);
		Assert.assertTrue(propagationConstraint.getAddConstraint().compareTo(addConstraint)==0);
		Assert.assertTrue(propagationConstraint.getRemoveConstraint().compareTo(removeConstraint)==0);
		Assert.assertTrue(propagationConstraint.getDeleteConstraint().compareTo(deleteConstraint)==0);
		logger.debug(ElementMapper.marshal(propagationConstraint));
		
		
		Facet facet = consistsOf.getTarget();
		logger.debug(ElementMapper.marshal(facet));
		
		Assert.assertTrue(facet.getAdditionalProperties().containsKey("version"));
	}
}
