package org.gcube.informationsystem.model.impl.entities;

import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ISContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ISContextTest.class);
	
	@Test
	public void testMarshalling() throws Exception {
		Context gcube = new ContextImpl("gcube");
		logger.debug("gcube : {}", ElementMapper.marshal(ElementMapper.unmarshal(Context.class, ElementMapper.marshal(gcube))));

		Context devsec = new ContextImpl("devsec");
		gcube.addChild(devsec);
		logger.debug("devsec : {}", ElementMapper.marshal(ElementMapper.unmarshal(Context.class, ElementMapper.marshal(devsec))));

		Context devVRE = new ContextImpl("devVRE");
		devsec.addChild(devVRE);
		logger.debug("devVRE : {}", ElementMapper.marshal(ElementMapper.unmarshal(Context.class, ElementMapper.marshal(devVRE))));

		Context devNext = new ContextImpl("devNext");
		gcube.addChild(devNext);
		logger.debug("devNext : {}", ElementMapper.marshal(ElementMapper.unmarshal(Context.class, ElementMapper.marshal(devNext))));

		Context NextNext = new ContextImpl("NextNext");
		devNext.addChild(NextNext);
		logger.debug("NextNext : {}", ElementMapper.marshal(ElementMapper.unmarshal(Context.class, ElementMapper.marshal(NextNext))));

		logger.debug("------------------------------------");

		logger.debug("gcube : {}", ElementMapper.marshal(gcube));
		logger.debug("devsec : {}", ElementMapper.marshal(devsec));
		logger.debug("devVRE : {}", ElementMapper.marshal(devVRE));
		logger.debug("devNext : {}", ElementMapper.marshal(devNext));
		logger.debug("NextNext : {}", ElementMapper.marshal(NextNext));
	}
}
