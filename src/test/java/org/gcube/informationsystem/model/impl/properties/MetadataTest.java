package org.gcube.informationsystem.model.impl.properties;

import java.util.Calendar;
import java.util.Date;

import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class MetadataTest {

	private static Logger logger = LoggerFactory.getLogger(MetadataTest.class);
	
	@Test
	public void metadataTest() throws Exception {
		MetadataImpl metadata = new MetadataImpl();
		Date date = Calendar.getInstance().getTime();
		metadata.creationTime = date;
		metadata.lastUpdateTime = date;
		metadata.createdBy = Metadata.UNKNOWN_USER;
		metadata.lastUpdateBy = Metadata.UNKNOWN_USER;
		
		String json = ElementMapper.marshal(metadata);
		logger.debug(json);
		
		
		Property property = ElementMapper.unmarshal(Property.class, json);
		Assert.assertTrue(property instanceof Metadata);
		Assert.assertTrue(((Metadata) property).getCreationTime().compareTo(date)==0);
		Assert.assertTrue(((Metadata) property).getLastUpdateTime().compareTo(date)==0);
		logger.debug(ElementMapper.marshal(property));
		
		Metadata h  = ElementMapper.unmarshal(Metadata.class, json);
		Assert.assertTrue(property instanceof Metadata);
		Assert.assertTrue(h.getCreationTime().compareTo(date)==0);
		Assert.assertTrue(h.getLastUpdateTime().compareTo(date)==0);
		logger.debug(ElementMapper.marshal(h));
	}
	
}
