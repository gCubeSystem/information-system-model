package org.gcube.informationsystem.utils;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class JsonUtility {
	
	private static Logger logger = LoggerFactory.getLogger(JsonUtility.class);
	
	public static JsonNode getJsonNode(String json) throws JsonProcessingException, IOException {
		if(json==null || json.compareTo("")==0){
			return null;
		}
		logger.trace("Trying to get Jsonnode from {}", json);
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNode = mapper.readTree(json);
		return jsonNode;
	}
}
