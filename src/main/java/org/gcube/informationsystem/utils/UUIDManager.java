package org.gcube.informationsystem.utils;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public final class UUIDManager {

	private static final Set<String> RESERVED_UUID_STRING;
	private static final Set<UUID> RESERVED_UUID;
	
	private static UUIDManager uuidManager;
	
	public static UUIDManager getInstance() {
		if(uuidManager==null) {
			uuidManager = new UUIDManager();
		}
		return uuidManager;
	}
	
	private UUIDManager() {
		
	}
	
	static {
		RESERVED_UUID_STRING = new HashSet<>();
		RESERVED_UUID = new HashSet<>();
		
		String[] uuidValidCharacters = {"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"};
		
		for(String string : uuidValidCharacters) {
			String uuidString = "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX";
			uuidString = uuidString.replaceAll("X", string);
			RESERVED_UUID_STRING.add(uuidString);
			RESERVED_UUID.add(UUID.fromString(uuidString));
		}
	}
	
	public boolean isReservedUUID(UUID uuid) {
		return RESERVED_UUID.contains(uuid);
	}
	
	public boolean isReservedUUID(String uuid) {
		return RESERVED_UUID_STRING.contains(uuid);
	}
	
	public Set<String> getAllReservedUUIDAsStrings(){
		return new TreeSet<>(RESERVED_UUID_STRING);
	}
	
	public Set<UUID> getAllReservedUUID(){
		return new TreeSet<>(RESERVED_UUID);
	}
	
	public UUID generateValidUUID() {
		return generateValidUUID(null);
	}

	public UUID generateValidUUID(UUID uuid) {
		uuid = uuid==null ? UUID.randomUUID() : uuid;
		while(RESERVED_UUID.contains(uuid)) {
			uuid = UUID.randomUUID();
		}
		return uuid;
	}
	
	public UUID validateUUID(UUID uuid) throws Exception {
		if(RESERVED_UUID.contains(uuid)) {
			throw new Exception(uuid.toString() + " UUID is reserved. All the following UUID are reserved " + getAllReservedUUIDAsStrings().toString());
		}
		return uuid;
	}
	
	
	
	
}
