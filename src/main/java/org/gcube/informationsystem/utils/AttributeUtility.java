package org.gcube.informationsystem.utils;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AttributeUtility {

	protected static Logger logger = LoggerFactory.getLogger(AttributeUtility.class);
	
	public static void checkRegex(String regex, String text) {
		try {
			if(regex==null) {
				return;
			}
			Pattern pattern = Pattern.compile(regex);
			if(text!=null) {
		    	Matcher matcher = pattern.matcher(text);
		    	if(!matcher.find()) {
		    		throw new RuntimeException("The value '" + text + "' does not match the requested regular expression '" + regex + "'.");
		    	}
		    }
		} catch (PatternSyntaxException e) {
			throw new RuntimeException("'" + regex + "' is not a valid regular expression", e);
		} catch (RuntimeException e) {
			throw e;
		}
	}
	
	public static String evaluateNullForDefaultValue(String defaultValueAsString) {
		if(defaultValueAsString==null || defaultValueAsString.compareTo("null")==0) {
			return null;
		}
		return defaultValueAsString;
	}
	
	public static Object evaluateValueStringAccordingBaseType(BaseType baseType, String valueAsString) {
		
		if(valueAsString==null || valueAsString.compareTo("null")==0) {
			return null;
		}
		
		if(valueAsString!=null) {
			switch (baseType) {
				case BOOLEAN:
					return Boolean.parseBoolean(valueAsString);
				
				case INTEGER:
					return Integer.parseInt(valueAsString);
					
				case SHORT:
					return Short.parseShort(valueAsString);
				
				case LONG:
					return Long.parseLong(valueAsString);
					
				case FLOAT:
					return Float.parseFloat(valueAsString);
				
				case DOUBLE:
					return Double.parseDouble(valueAsString);
				
				case DATE:
					SimpleDateFormat sdf = new SimpleDateFormat(Element.DATETIME_PATTERN);
					try {
						return sdf.parse(valueAsString);
					} catch (ParseException e) {
						StringBuffer stringBuffer = new StringBuffer();
						stringBuffer.append("Error while parsing annotated default ");
						stringBuffer.append(Date.class.getSimpleName());
						stringBuffer.append(". The provided default value is '");
						stringBuffer.append(valueAsString);
						stringBuffer.append("' which does not match the pattern '");
						stringBuffer.append(Element.DATETIME_PATTERN);
						stringBuffer.append("'.");
						throw new RuntimeException(stringBuffer.toString(), e);
					}
				
				case STRING:
					return valueAsString;
					
				case BINARY:
					return valueAsString.getBytes();
					
				case BYTE:
					return Byte.parseByte(valueAsString);
				
				case PROPERTY:
					return null;
					
				default:
					break;
			}
		}
		
		return null;

	}

	/**
	 * Check if the string is a valid date time string according to the pattern
	 * defined in the {@link Element#DATETIME_PATTERN}
	 * @param dateTimeString
	 * @return
	 * @throws DateTimeParseException
	 */
	public static LocalDateTime checkDateTimeString(String dateTimeString) throws DateTimeParseException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(Element.DATETIME_PATTERN);
		return LocalDateTime.parse(dateTimeString, formatter);
	}

}

