package org.gcube.informationsystem.utils;

import java.util.regex.Pattern;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A class representing and validating a version in the following format X.X.X (Major.Minor.Revision)
 * Each part is an integer with no trailing zeros (e.g 1 and not 01).
 * The version is validated by the regex defined in the static field {@link Version#VERSION_REGEX}
 * 
 * Accepted initial version is {@link Version#MINIMAL_VERSION_STRING}
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public final class Version implements Comparable<Version> {

	/**
	 * Regex validating the version
	 */
	public final static String VERSION_REGEX = "^[1-9][0-9]{0,}\\.(0|([1-9][0-9]{0,}))\\.(0|([1-9][0-9]{0,}))$";
	
	private final static Pattern VERSION_PATTERN; 
	
	/**
	 * Accepted initial version
	 */
	public static final String MINIMAL_VERSION_STRING = "1.0.0";
	
	/**
	 * Accepted initial version as TypeVersion instance
	 */
	public static final Version MINIMAL_VERSION;
	
	/**
	 * Default changelog description for the initial version  
	 */
	public static final String MINIMAL_VERSION_DESCRIPTION = "First Version";
	
	static {
		VERSION_PATTERN  = Pattern.compile(VERSION_REGEX);
		MINIMAL_VERSION = new Version(MINIMAL_VERSION_STRING);
	}
	
	@JsonIgnore
	protected int major;
	@JsonIgnore
	protected int minor;
	@JsonIgnore
	protected int revision;
	
	protected Version(){}
	
	public Version(String version) {
		setVersion(version);
	}
	
	public Version(int major, int minor, int revision) {
		this.major = major;
		this.minor = minor;
		this.revision = revision;
		check();
	}
	
	public void setVersion(String version) {
		if(!VERSION_PATTERN.matcher(version).matches()) {
			throw new RuntimeException("The provided version (i.e. " + version + ") MUST respect the regex " + VERSION_REGEX);
		}
		String[] parts = version.split("\\.");
		this.major = Integer.valueOf(parts[0]);
		this.minor = Integer.valueOf(parts[1]);
		this.revision = Integer.valueOf(parts[2]);
		check();
	}
	
	
	protected void check() {
		/*
		 * The REGEX does not allow a 0.X.X Version. 
		 * Anyway I added this check in case we decide to change the minimal version and the
		 * regex.   
		 */
		if(this.compareTo(MINIMAL_VERSION)<0) {
			throw new RuntimeException("Minimal Allowed version is " + MINIMAL_VERSION_STRING);
		}
	}
	
	public int getMajor() {
		return major;
	}

	protected void setMajor(int major) {
		this.major = major;
	}

	public int getMinor() {
		return minor;
	}

	protected void setMinor(int minor) {
		this.minor = minor;
	}

	public int getRevision() {
		return revision;
	}

	protected void setRevision(int revision) {
		this.revision = revision;
	}

	@Override
	public String toString() {
		return major + "." + minor + "." + revision;
	}
	@Override
	public int compareTo(Version other) {
		if(other == null) {
            return 1;
		}
		
		int compare = Integer.compare(major, other.major);
		if(compare!=0) {
			return compare;
		}
		
		compare = Integer.compare(minor, other.minor);
		if(compare!=0) {
			return compare;
		}
		
		compare = Integer.compare(revision, other.revision);
		return compare;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + major;
		result = prime * result + minor;
		result = prime * result + revision;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Version other = (Version) obj;
		if (major != other.major)
			return false;
		if (minor != other.minor)
			return false;
		if (revision != other.revision)
			return false;
		return true;
	}
	
}