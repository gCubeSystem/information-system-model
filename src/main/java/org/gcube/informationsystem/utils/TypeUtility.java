package org.gcube.informationsystem.utils;

import java.io.IOException;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.types.TypeMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeUtility {
	
	public static String getTypeName(JsonNode jsonNode){
		if(jsonNode.has(Element.TYPE_PROPERTY)) {
			return jsonNode.get(Element.TYPE_PROPERTY).asText();
		}
		return null;
	}
	
	public static String getTypeName(String json) throws JsonProcessingException, IOException{
		JsonNode jsonNode = JsonUtility.getJsonNode(json);
		return getTypeName(jsonNode);
	}
	
	public static <E extends Element> String getTypeName(Class<E> clz){
		return TypeMapper.getType(clz);
	}
	
}
