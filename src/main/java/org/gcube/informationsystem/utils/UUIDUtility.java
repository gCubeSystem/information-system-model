package org.gcube.informationsystem.utils;

import java.io.IOException;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class UUIDUtility {

	private static Logger logger = LoggerFactory.getLogger(UUIDUtility.class);
	
	public static UUID getUUID(JsonNode jsonNode){
		if(jsonNode.has(IdentifiableElement.ID_PROPERTY)) {
			JsonNode jsonNodeID = jsonNode.get(IdentifiableElement.ID_PROPERTY);
			if(jsonNodeID!=null && jsonNodeID.isTextual()) {
				return UUID.fromString(jsonNodeID.asText());
			}
		}
		return null;
	}
	
	public static UUID getUUID(String json) throws JsonProcessingException, IOException {
		logger.trace("Trying to get UUID of {} ", json);
		JsonNode jsonNode = JsonUtility.getJsonNode(json);
		return getUUID(jsonNode);
	}
	
	public static String getUUIDAsString(JsonNode jsonNode){
		UUID uuid = getUUID(jsonNode);
		if(uuid!=null) {
			return uuid.toString();
		}
		return null;
	}
	
	public static String getUUIDAsString(String json) throws JsonProcessingException, IOException{
		return getUUID(json).toString();
	}
	
	public static UUID fromString(String uuidString) throws Exception {
		UUID uuid = UUID.fromString(uuidString);
		UUIDManager.getInstance().validateUUID(uuid);
		return uuid;
	}
	
}
