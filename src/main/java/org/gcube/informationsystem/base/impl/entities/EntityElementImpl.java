package org.gcube.informationsystem.base.impl.entities;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.ElementImpl;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=EntityElement.NAME)
public abstract class EntityElementImpl extends ElementImpl implements EntityElement {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4488771434017342703L;
	
	protected UUID uuid;
	protected Metadata metadata;
	
	protected EntityElementImpl(){
		super();
	}
	
	@Override
	public UUID getID() {
		return uuid;
	}

	@Override
	public void setID(UUID uuid) {
		this.uuid = uuid;
	}
	
	@Override
	public Metadata getMetadata() {
		return metadata;
	}
	
	@Override
	public void setMetadata(Metadata metadata){
		this.metadata = metadata;
	}
	
}
