/**
 * 
 */
package org.gcube.informationsystem.base.impl;

import java.io.StringWriter;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.TypeUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Element.NAME)
public class ElementImpl implements Element {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7338083489551084860L;
	
	
	@Override
	public String getTypeName() {
		return TypeUtility.getTypeName(this.getClass());
	}

	@Override
	public String toString(){
		StringWriter stringWriter = new StringWriter();
		try {
			ElementMapper.marshal(this, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			return super.toString();
		}
	}

}
