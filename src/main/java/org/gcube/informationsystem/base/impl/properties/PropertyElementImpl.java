package org.gcube.informationsystem.base.impl.properties;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.ElementImpl;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.serialization.ElementMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=PropertyElement.NAME)
public class PropertyElementImpl extends ElementImpl implements Property {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1396998430221747445L;

	protected List<String> supertypes;
	protected String expectedtype;
	
	@JsonIgnore
	protected Map<String, Object> additionalProperties;
	
	/**
	 * Used to allow to have an additional property starting with '_' or '@'
	 */
	@JsonIgnore
	protected final Set<String> allowedAdditionalKeys;
	
	public PropertyElementImpl() {
		super();
		this.additionalProperties = new HashMap<>();
		this.allowedAdditionalKeys = new HashSet<>();
	}

	@Override
	public List<String> getSupertypes() {
		return this.supertypes;
	}
	
	@Override
	public String getExpectedtype() {
		return this.expectedtype;
	}

	
	@Override
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@Override
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@Override
	public Object getAdditionalProperty(String key) {
		return additionalProperties.get(key);
	}

	@Override
	public void setAdditionalProperty(String key, Object value) {
		if(!allowedAdditionalKeys.contains(key)){
			if(key.startsWith("_")) {
				return;
			}
			if(key.startsWith("@")) {
				return;
			}
		}
		
		/*
		Additional properties are not deserialized to the proper property type.
		The first attempt was to try to write a specific deserializer but it fails.	
		This fix the issue.
		*/
		try {
			if(value instanceof Map<?,?>) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String,Object>) value;
				if(map.containsKey(Element.TYPE_PROPERTY)) {
					String reserialized = ElementMapper.getObjectMapper().writeValueAsString(map);
					Property property = ElementMapper.unmarshal(Property.class, reserialized);
					value = property;
				}
			}
		}catch (Throwable e) {
			e.getMessage();
			// Any type of error/exception must be catched
		}
		/* END of fix to properly deserialize Property types*/
		
		this.additionalProperties.put(key, value);
	}
	
	public void addAllowedAdditionalKey(String allowedAdditionalKey){
		this.allowedAdditionalKeys.add(allowedAdditionalKey);
	}
	
}