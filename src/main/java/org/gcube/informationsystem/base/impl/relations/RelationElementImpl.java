package org.gcube.informationsystem.base.impl.relations;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.ElementImpl;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=RelationElement.NAME)
public abstract class RelationElementImpl<S extends EntityElement, T extends EntityElement> extends ElementImpl implements RelationElement<S, T> {
	
	/**
	 * Generated Serial Version UID 
	 */
	private static final long serialVersionUID = 28704968813390512L;
	
	protected UUID uuid;
	protected Metadata metadata;
	
	protected S source;
	protected T target;

	protected RelationElementImpl() {
		super();
	}
	
	protected RelationElementImpl(S source, T target) {
		this();
		this.source = source;
		this.target = target;
	}
	
	@Override
	public UUID getID() {
		return uuid;
	}

	@Override
	public void setID(UUID uuid) {
		this.uuid = uuid;
	}
	
	@Override
	public Metadata getMetadata() {
		return metadata;
	}
	
	@Override
	public void setMetadata(Metadata metadata){
		this.metadata = metadata;
	}
	
	@Override
	public S getSource() {
		return source;
	}
	
	@Override
	public void setSource(S source) {
		this.source = source;
	}

	@Override
	public T getTarget() {
		return target;
	}

	@Override
	public void setTarget(T target) {
		this.target = target;
	}

}