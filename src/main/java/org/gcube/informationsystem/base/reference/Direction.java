package org.gcube.informationsystem.base.reference;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum Direction {
	
	IN, OUT, BOTH;

	public Direction opposite() {
		if(this.equals(IN)) {
			return OUT;
		}else if(this.equals(OUT)) {
			return IN;
		} else {
			return BOTH;
		}
	}
	
}
