/**
 * 
 */
package org.gcube.informationsystem.base.reference;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.gcube.informationsystem.base.impl.entities.EntityElementImpl;
import org.gcube.informationsystem.base.impl.properties.PropertyElementImpl;
import org.gcube.informationsystem.base.impl.relations.RelationElementImpl;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.impl.relations.IsParentOfImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.impl.entities.DummyFacet;
import org.gcube.informationsystem.model.impl.entities.DummyResource;
import org.gcube.informationsystem.model.impl.entities.EntityImpl;
import org.gcube.informationsystem.model.impl.entities.FacetImpl;
import org.gcube.informationsystem.model.impl.entities.ResourceImpl;
import org.gcube.informationsystem.model.impl.properties.PropertyImpl;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.impl.relations.DummyIsRelatedTo;
import org.gcube.informationsystem.model.impl.relations.IsRelatedToImpl;
import org.gcube.informationsystem.model.impl.relations.RelationImpl;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.queries.templates.impl.entities.QueryTemplateImpl;
import org.gcube.informationsystem.queries.templates.impl.properties.TemplateVariableImpl;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.types.impl.entities.EntityTypeImpl;
import org.gcube.informationsystem.types.impl.properties.PropertyDefinitionImpl;
import org.gcube.informationsystem.types.impl.properties.PropertyTypeImpl;
import org.gcube.informationsystem.types.impl.relations.RelationTypeImpl;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;
import org.gcube.informationsystem.types.reference.relations.RelationType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enumerates the basic type names.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public enum AccessType {
	
	PROPERTY_ELEMENT(PropertyElement.class, PropertyElement.NAME, PropertyElementImpl.class, null),
	PROPERTY_DEFINITION(PropertyDefinition.class, PropertyDefinition.NAME, PropertyDefinitionImpl.class, null),
	PROPERTY_TYPE(PropertyType.class, PropertyType.NAME, PropertyTypeImpl.class, null),
	TEMPLATE_VARIABLE(TemplateVariable.class, TemplateVariable.NAME, TemplateVariableImpl.class, null),
	PROPERTY(Property.class, Property.NAME, PropertyImpl.class, null),
	
	ENTITY_ELEMENT(EntityElement.class, EntityElement.NAME, EntityElementImpl.class, null),
	ENTITY_TYPE(EntityType.class, EntityType.NAME, EntityTypeImpl.class, null),
	QUERY_TEMPLATE(QueryTemplate.class, QueryTemplate.NAME, QueryTemplateImpl.class, null),
	CONTEXT(Context.class, Context.NAME, ContextImpl.class, null),
	ENTITY(Entity.class, Entity.NAME, EntityImpl.class, null),
	RESOURCE(Resource.class, Resource.NAME, ResourceImpl.class, DummyResource.class),
	FACET(Facet.class, Facet.NAME, FacetImpl.class, DummyFacet.class),
		
	RELATION_ELEMENT(RelationElement.class, RelationElement.NAME, RelationElementImpl.class, null),
	RELATION_TYPE(RelationType.class, RelationType.NAME, RelationTypeImpl.class, null),
	IS_PARENT_OF(IsParentOf.class, IsParentOf.NAME, IsParentOfImpl.class, null),
	RELATION(Relation.class, Relation.NAME, RelationImpl.class, null),
	IS_RELATED_TO(IsRelatedTo.class, IsRelatedTo.NAME, IsRelatedToImpl.class, DummyIsRelatedTo.class),
	CONSISTS_OF(ConsistsOf.class, ConsistsOf.NAME, ConsistsOfImpl.class, null);
	
	
	private static Logger logger = LoggerFactory.getLogger(AccessType.class);
	
	private static AccessType[] modelTypes;
	private static AccessType[] erTypes;
	private static Set<String> names;
	
	static {
		names = new HashSet<>();
		AccessType[] accessTypes = AccessType.values();
		for (AccessType accessType : accessTypes) {
			String name = accessType.getName();
			names.add(name);
		}
		
		modelTypes = new AccessType[] {
				AccessType.PROPERTY, 
				AccessType.RESOURCE, AccessType.FACET, 
				AccessType.IS_RELATED_TO, AccessType.CONSISTS_OF
		};
		
		erTypes = new AccessType[] {
				AccessType.RESOURCE, AccessType.FACET, 
				AccessType.IS_RELATED_TO, AccessType.CONSISTS_OF
		};
	}
	
	private final Class<? extends Element> clz;
	private final Class<? extends Element> implementationClass;
	private final Class<? extends Element> dummyImplementationClass;
	
	private final String name;
	private final String lowerCaseFirstCharacter;
	
	<ISM extends Element, ISMC extends ISM, ISMD extends ISMC>
	AccessType(Class<ISM> clz, String name, Class<ISMC> implementationClass, Class<ISMD> dummyImplementationClass){
		this.clz = clz;
		this.implementationClass = implementationClass;
		this.dummyImplementationClass = dummyImplementationClass;
		this.name = name;
		this.lowerCaseFirstCharacter = name.substring(0, 1).toLowerCase() + name.substring(1);
	}
	
	@SuppressWarnings("unchecked")
	public <ISM extends Element> Class<ISM> getTypeClass(){
		return (Class<ISM>) clz;
	}
	
	@SuppressWarnings("unchecked")
	public <ISM extends Element, ISMC extends ISM> Class<ISMC> getImplementationClass() {
		return (Class<ISMC>) implementationClass;
	}
	
	@SuppressWarnings("unchecked")
	public <ISM extends Element, ISMC extends ISM, ISMD extends ISMC> Class<ISMD> getDummyImplementationClass() {
		return (Class<ISMD>) dummyImplementationClass;
	}
	
	public String getName(){
		return name;
	}
	
	public String lowerCaseFirstCharacter() {
		return lowerCaseFirstCharacter;
	}
	
	@Override
	public String toString(){
		return name;
	}
	
	public static Set<String> names(){
		return names;
	}
	
	/**
	 * Provide the access type from the name provided type name as argument
	 * @param typeName type name
	 * @return the AccessType from the name provided type name as argument, null otherwise
	 */
	public static AccessType getAccessType(String typeName) {
		AccessType[] accessTypes = AccessType.values();
		for (AccessType accessType : accessTypes) {
			String name = accessType.getName();
			if(name.compareTo(typeName)==0) {
				return accessType;
			}
		}
		return null;
	}
	
	public static AccessType getAccessType(Class<?> clz) {
		AccessType ret  =null;
		
		AccessType[] accessTypes = AccessType.values();
		for (AccessType accessType : accessTypes) {
			Class<? extends Element> typeClass = accessType.getTypeClass();
			if (typeClass.isAssignableFrom(clz)) {
				if(ret==null || ret.getTypeClass().isAssignableFrom(typeClass)){
					ret = accessType;
				}
			}
		}
		
		if(ret !=null){
			return ret;
		}else{
			String error = String
					.format("The provided class %s does not belong to any of defined AccessTypes %s",
							clz.getSimpleName(), Arrays.toString(accessTypes));
			logger.trace(error);
			throw new RuntimeException(error);
		}
	}
	
	/**
	 * @return an array of AccessTypes containing only 
	 * Model Types (i.e. Property, Resource, Facet, IsRelatedTo, ConsistsOf)
	 */
	public static AccessType[] getModelTypes() {
		return modelTypes;
	}
	
	/**
	 * @return an array of AccessTypes containing only 
	 * Entity/Relation Model Types (i.e. Resource, Facet, IsRelatedTo, ConsistsOf)
	 */
	public static AccessType[] getERTypes() {
		return erTypes;
	}
	
}
