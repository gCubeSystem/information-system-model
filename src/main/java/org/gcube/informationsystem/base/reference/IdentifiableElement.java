package org.gcube.informationsystem.base.reference;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * This interfaces is an helper to identify elements could be identified via {@link Metadata}
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonPropertyOrder({ Element.TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY })
public interface IdentifiableElement extends Element {
	
	public static final String ID_PROPERTY = "id";
	public static final String METADATA_PROPERTY = "metadata";
	
	@JsonGetter(value = ID_PROPERTY)
	public UUID getID();

	@JsonSetter(value = ID_PROPERTY)
	public void setID(UUID uuid);
	
	@JsonInclude(Include.NON_NULL)
	@JsonGetter(value = METADATA_PROPERTY)
	public Metadata getMetadata();
	
	@JsonSetter(value = METADATA_PROPERTY)
	public void setMetadata(Metadata metadata);
	
}
