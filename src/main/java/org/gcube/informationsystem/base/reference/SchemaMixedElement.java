package org.gcube.informationsystem.base.reference;

import java.util.Map;

/**
 * This interfaces is an helper to identify elements supporting Schema Mixed.
 * i.e. elements which instances could have additional properties in respect to the ones defined in the schema
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public interface SchemaMixedElement extends Element {
	
	public Map<String,Object> getAdditionalProperties();
	
	public void setAdditionalProperties(Map<String,Object> additionalProperties);
	
	public Object getAdditionalProperty(String key);
	
	public void setAdditionalProperty(String key, Object value);
	
}
