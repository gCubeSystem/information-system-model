package org.gcube.informationsystem.base.reference;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.informationsystem.types.PropertyTypeName;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface Attribute {
	
	public static final String NAME = "Attribute"; // Attribute.class.getSimpleName();
	
	public static final String NAME_PROPERTY = "name";
	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String MAX_PROPERTY = "max";
	public static final String MIN_PROPERTY = "min";
	public static final String REGEX_PROPERTY = "regexp";
	public static final String PROPERTY_TYPE_PROPERTY = "propertyType";
	public static final String DEFAULT_VALUE_PROPERTY = "defaultValue";
	
	public String getName();
	
	public void setName(String name);
	
	public String getDescription();
	
	public void setDescription(String description);
	
	public String getPropertyType();
	
	public void setPropertyType(String type);
	
	@JsonIgnore
	public PropertyTypeName getPropertyTypeName();
	
	public Object getDefaultValue();

	public void setDefaultValue(Object defaultValue);
	
	public Integer getMax();
	
	public void setMax(Integer max);
	
	public Integer getMin();
	
	public void setMin(Integer min);
	
	public String getRegexp();
	
	public void setRegexp(String regexp);
	
}