package org.gcube.informationsystem.base.reference.entities;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@Abstract
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY })
//@JsonDeserialize(as=EntityElementImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = EntityElement.NAME, description = "This is the base type for any EntityElement", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface EntityElement extends Element, IdentifiableElement {
	
	public static final String NAME = "EntityElement"; //Entity.class.getSimpleName();

	@ISProperty(name = ID_PROPERTY, description = "This UUID is be used to identify the instance univocally.", readonly = true, mandatory = true, nullable = false)
	@Override
	public UUID getID();

	@Override
	public void setID(UUID uuid);
	
	@ISProperty(name=METADATA_PROPERTY, mandatory=true, nullable=false, description="Metadata associated with the instance that is automatically created/updated by the system.")
	@Override
	public Metadata getMetadata();
	
	@Override
	public void setMetadata(Metadata metadata);
	
}