package org.gcube.informationsystem.base.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.impl.properties.PropertyElementImpl;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * Root Class for Property types. 
 * It creates a base common type, which is useful for management purpose.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=PropertyElementImpl.class)
@TypeMetadata(name = PropertyElement.NAME, description = "This is the base type for any PropertyElement", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface PropertyElement extends Element {
	
	public static final String NAME = "PropertyElement"; //PropertyElement.class.getSimpleName();
	
}
