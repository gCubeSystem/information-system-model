package org.gcube.informationsystem.base.reference;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface AttributeDefinition extends Attribute {
	
	public static final String MANDATORY_PROPERTY = "mandatory";
	public static final String NOT_NULL_PROPERTY = "notnull";
	
	public boolean isMandatory();
	
	public void setMandatory(boolean mandatory);
	
	public boolean isNotnull();
	
	public void setNotnull(boolean notnull);
	
}
