package org.gcube.informationsystem.base.reference.relations;

import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@Abstract
//@JsonDeserialize(as=RelationElementImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = RelationElement.NAME, description = "This is the base type for any RelationElement", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface RelationElement<S extends EntityElement, T extends EntityElement> extends Element, IdentifiableElement {
	
	public static final String NAME = "RelationElement"; // RelationElement.class.getSimpleName();
	
	public static final String SOURCE_PROPERTY = "source";
	public static final String TARGET_PROPERTY = "target";
	
	@ISProperty(name = ID_PROPERTY, description = "This UUID is be used to identify the instance univocally.", readonly = true, mandatory = true, nullable = false)
	@Override
	public UUID getID();

	@Override
	public void setID(UUID uuid);
	
	@ISProperty(name=METADATA_PROPERTY, mandatory=true, nullable=false, description="Metadata associated with the instance that is automatically created/updated by the system.")
	@Override
	public Metadata getMetadata();
	
	@Override
	public void setMetadata(Metadata metadata);
	
	@JsonGetter(value=SOURCE_PROPERTY)
	public S getSource();
	
	@JsonIgnore
	public void setSource(S source);
	
	@JsonGetter(value=TARGET_PROPERTY)
	public T getTarget();
	
	@JsonIgnore
	public void setTarget(T target);
	
}