/**
 * 
 */
package org.gcube.informationsystem.base.reference;

import java.io.Serializable;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonPropertyOrder({ Element.TYPE_PROPERTY })
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = Element.TYPE_PROPERTY)
// @JsonTypeIdResolver(ElementTypeIdResolver.class)
@TypeMetadata(name = Element.NAME, description = "This is the base type for Element", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Element extends Serializable {

	public static final String NAME = "Element"; //Element.class.getSimpleName();
	
	public static final String TYPE_PROPERTY = "type";
	
	/**
	 * DateTime Pattern to be used to serialize Dates in every element 
	 */
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS Z";
	
	@JsonIgnore
	public String getTypeName();
	
}
