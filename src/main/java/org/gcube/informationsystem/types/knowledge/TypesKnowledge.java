package org.gcube.informationsystem.types.knowledge;

import java.util.Calendar;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.knowledge.ModelKnowledge;
import org.gcube.informationsystem.model.knowledge.TypesDiscoverer;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypesKnowledge {

	private static TypesKnowledge instance;
	
	public synchronized static TypesKnowledge getInstance() {
		if(instance==null) {
			instance = new TypesKnowledge();
		}
		return instance;
	}
	
	// in millisec
	public static final long DEFAULT_EXPIRING_TIMEOUT;
	
	static {
		DEFAULT_EXPIRING_TIMEOUT = TimeUnit.HOURS.toMillis(6);
		
	}
	
	protected boolean initialized;
	public int expiringTimeout;
	
	// in millisec used only for logging and debugging
	protected Calendar creationTime;
	// in millisec
	protected Calendar expiringTime;
		
	protected ModelKnowledge<Type, TypeInformation> modelKnowledge;
	protected TypesDiscoverer<Type> typesDiscoverer;
	
	public TypesKnowledge() {
		initialized = false;
		expiringTimeout = (int) DEFAULT_EXPIRING_TIMEOUT;
		modelKnowledge = new ModelKnowledge<>(new TypeInformation());
	}
	
	public void setExpiringTimeout(int expiringTimeout) {
		this.expiringTimeout = expiringTimeout;
	}
	
	public TypesDiscoverer<Type> getTypesDiscoverer() {
		return typesDiscoverer;
	}

	public void setTypesDiscoverer(TypesDiscoverer<Type> typesDiscoverer) {
		this.typesDiscoverer = typesDiscoverer;
	}
	
	public ModelKnowledge<Type, TypeInformation> getModelKnowledge() {
		if(!initialized) {
			discover();
		}else {
			Calendar now = Calendar.getInstance();
			if(now.after(expiringTime)) {
				renew();
			}
		}
		return modelKnowledge;
	}
	
	protected synchronized void init(boolean forceReinitialization) {
		if(typesDiscoverer!=null && (initialized==false || forceReinitialization)) {
			initialized = false;
			modelKnowledge = new ModelKnowledge<>(new TypeInformation());
			AccessType[] modelTypes = AccessType.getModelTypes();
			for(AccessType modelType : modelTypes) {
				Collection<Type> types = typesDiscoverer.discover(modelType);
				modelKnowledge.addAllType(types);
			}
			initialized = true;
			this.creationTime = Calendar.getInstance();
			this.expiringTime = Calendar.getInstance();
			this.expiringTime.setTimeInMillis(creationTime.getTimeInMillis());
			this.expiringTime.add(Calendar.MILLISECOND, -1);
			this.expiringTime.add(Calendar.MILLISECOND, expiringTimeout);
		}
	}
	
	/**
	 * This method do nothing if TypesDiscoverer
	 * was not set. 
	 * Otherwise initialized the ModelKnowledge
	 * if it was not already initialized.
	 * To enforce rediscovery use renew method.
	 */
	public void discover() {
		init(false);
	}
	
	/**
	 * Force reinitialization of 
	 */
	public void renew() {
		init(true);
	}
}
