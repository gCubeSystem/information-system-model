package org.gcube.informationsystem.types.knowledge;

import java.util.LinkedHashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TypeInformation implements org.gcube.informationsystem.model.knowledge.TypeInformation<Type> {

	@Override
	public String getIdentifier(Type type) {
		return type.getName();
	}

	@Override
	public Set<String> getParentIdentifiers(Type root, Type type) {
		if(root!=null && type.getName().compareTo(root.getName())==0) {
			return new LinkedHashSet<>();
		}
		if(root==null && AccessType.getAccessType(type.getName())!=null) {
			return new LinkedHashSet<>();
		}
		return type.getExtendedTypes();
	}

	@Override
	public AccessType getAccessType(Type type) {
		return type.getAccessType();
	}

	@Override
	public Type getRoot(AccessType accessType) {
		return TypeMapper.createTypeDefinition(accessType.getTypeClass());
	}

}
