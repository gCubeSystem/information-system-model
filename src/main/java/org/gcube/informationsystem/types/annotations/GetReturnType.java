package org.gcube.informationsystem.types.annotations;

import org.gcube.informationsystem.types.PropertyTypeName.BaseType;

/**
 * This is a facade class.
 * {@link ISProperty} has the possibility to specify the type.
 * If we don't specify a default in the annotation definition the type is mandatory.
 * We need a way to specify a default which means get the return of the annotated method.
 * This avoid to the developer to specify something which can be get from the annotated method.
 * We can't use Object as default because Object is mapped as {@link BaseType#ANY}
 *
 * This interface has been defined for this reason.
 */
public interface GetReturnType {

}
