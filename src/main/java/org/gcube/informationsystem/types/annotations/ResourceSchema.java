package org.gcube.informationsystem.types.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResourceSchema {

	ResourceSchemaEntry[] facets() default {};
	
	RelatedResourcesEntry[] resources() default {};
}
