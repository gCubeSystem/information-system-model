package org.gcube.informationsystem.types.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;

/**
 * It is used by {@link TypeMapper} to identify which getter method are
 * related to and {@link Entity} {@link PropertyDefinition}.
 * The name of the property is obtained by removing "get" or "is" from method 
 * name and lower casing the first letter.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ISProperty {

	String name() default "";
	/**
	 * If we don't specify a default in the annotation definition, the type becomes mandatory. 
	 * We wanted a way to specify a default that means using the return type of the annotated method. 
	 * This avoids the need for developers to specify something that can be obtained from the annotated method itself. 
	 * We can't use Object as the default because Object is mapped as {@link BaseType#ANY}. 
	 * For this reason, the type {@link GetReturnType} has been set as the default.
	 * 
	 * Please note that you can't use this functionality for generic types 
	 * like List<String> because it is not accepted by the annotation.
	 */
	Class<?> type() default GetReturnType.class;
	String description() default "";
	boolean mandatory() default false;
	boolean readonly() default false;
	boolean nullable() default true;
	int min() default -1;
	int max() default -1;
	String regexpr() default "";
	String defaultValue() default "null";
	
}
