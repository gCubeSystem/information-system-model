package org.gcube.informationsystem.types.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.gcube.informationsystem.base.reference.Element;

/**
 * It indicates that the {@link Element} is final and cannot be extended/derived.
 * This is needed because the type definition is made with interface.
 * @author Luca Frosini (ISTI - CNR)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Final {

}
