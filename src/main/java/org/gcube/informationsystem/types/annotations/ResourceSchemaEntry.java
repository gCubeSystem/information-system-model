package org.gcube.informationsystem.types.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;

/**
 * It is used by {@link TypeMapper} to identify which getter method are
 * related to and {@link Entity} {@link PropertyDefinition}.
 * The name of the property is obtained by removing "get" or "is" from method 
 * name and lower casing the first letter.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ResourceSchemaEntry {

	@SuppressWarnings("rawtypes")
	@JsonProperty
	Class<? extends ConsistsOf> relation() default ConsistsOf.class;
	@JsonProperty
	Class<? extends Facet> facet() default Facet.class;
	
	@JsonProperty
	String description() default "";
	
	@JsonProperty
	int min() default 0;
	@JsonProperty
	int max() default -1;
	

}
