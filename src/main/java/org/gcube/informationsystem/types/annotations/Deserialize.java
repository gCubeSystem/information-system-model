package org.gcube.informationsystem.types.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * It is used in place of @JsonDeserialize
 * to allow to override the implementation class
 * @author Luca Frosini (ISTI - CNR)
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Deserialize {
	
	public Class<?> as() default Void.class;

}

