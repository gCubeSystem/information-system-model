package org.gcube.informationsystem.types.impl.relations;

import java.lang.reflect.Type;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.types.impl.entities.FacetTypeImpl;
import org.gcube.informationsystem.types.impl.entities.ResourceTypeImpl;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.relations.ConsistsOfType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ConsistsOfType.NAME)
public final class ConsistsOfTypeImpl extends RelationTypeImpl<ResourceType, FacetType> implements ConsistsOfType {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2891976493165330476L;
	
	protected ConsistsOfTypeImpl() {
		super();
	}
	
	public ConsistsOfTypeImpl(Class<? extends ConsistsOf<? extends Resource, ? extends Facet>> clz) {
		super(clz);
		
		this.extendedTypes = retrieveSuperClasses(clz, ConsistsOf.class, Relation.NAME);
		
		this.properties = retrieveListOfProperties(clz);
		
		discoverSourceAndTarget(clz);
	}
	
	@SuppressWarnings("unchecked")
	private void discoverSourceAndTarget(Class<? extends ConsistsOf<? extends Resource, ? extends Facet>> clz) {
		Type[] typeParameters = clz.getTypeParameters();
		
		Class<? extends Resource> sourceClass;
		Class<? extends Facet> targetClass;
		if(typeParameters.length==0) {
			typeParameters = getParametersFromSuperClasses(clz);
			sourceClass = (Class<? extends Resource>) typeParameters[0];
			targetClass = (Class<? extends Facet>) typeParameters[1];
		}else {
			sourceClass = (Class<? extends Resource>) getGenericClass(typeParameters[0]);
			targetClass = (Class<? extends Facet>) getGenericClass(typeParameters[1]);
		}
		
		this.source = new ResourceTypeImpl((Class<? extends Resource>) sourceClass);
		this.target = new FacetTypeImpl((Class<? extends Facet>) targetClass);
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		return AccessType.CONSISTS_OF;
	}
	
}
