package org.gcube.informationsystem.types.impl.validator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ValidatorReport {

    protected Boolean valid;
    protected Map<String, AttributeValidatorReport> attributeValidatorReports;
    protected Map<Boolean, List<AttributeValidatorReport>> attributeValidatorReportByValidity;
    
    public ValidatorReport() {
        this.valid = null;
        this.attributeValidatorReports = new HashMap<>();
        this.attributeValidatorReportByValidity = new HashMap<>();
        for (Boolean b : new Boolean[] { true, false }) {
            this.attributeValidatorReportByValidity.put(b, new ArrayList<>());
        }
    }

    public Collection<AttributeValidatorReport> getAttributeValidatorReports() {
        return attributeValidatorReports.values();
    }

    public void addAttributeValidatorReport(AttributeValidatorReport attributeValidatorReport) {
        boolean avrValid = attributeValidatorReport.isValid();
        this.valid = avrValid && (valid == null ? true : valid);
        List<AttributeValidatorReport> reports = this.attributeValidatorReportByValidity.get(avrValid);
        reports.add(attributeValidatorReport);
        this.attributeValidatorReports.put(attributeValidatorReport.getFieldName(), attributeValidatorReport);
    }

    public AttributeValidatorReport getAttributeValidatorReport(String fieldName) {
        return this.attributeValidatorReports.get(fieldName);
    }

    public Boolean isValid() {
        return valid;
    }

    public List<String> getErrors() {
        List<String> errors = new ArrayList<>();
        for (AttributeValidatorReport attributeValidatorReport : attributeValidatorReportByValidity.get(false)) {
            if (!attributeValidatorReport.isValid()) {
                errors.addAll(attributeValidatorReport.getMessages());
            }
        }
        return errors;
    }

}
