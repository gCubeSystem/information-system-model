package org.gcube.informationsystem.types.impl.validator;

import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.Iterator;

import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.NullNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.AttributeDefinition;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.PropertyTypeName;
import org.gcube.informationsystem.types.PropertyTypeName.BaseType;
import org.gcube.informationsystem.types.knowledge.TypesKnowledge;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.utils.AttributeUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Used to validate the value fo one attribute against the attribute definition
 * @author Luca Frosini (ISTI - CNR)
 */
public class AttributeValidator {

    protected static Logger logger = LoggerFactory.getLogger(AttributeValidator.class);

    protected final boolean stopOnError;
    protected final AttributeDefinition attributeDefinition;
    protected final JsonNode value;
    protected final AttributeValidatorReport attributeValidatorReport;
    
    public AttributeValidator(AttributeDefinition attributeDefinition, JsonNode value) {
        this(attributeDefinition, value, false);
    }

    public AttributeValidator(AttributeDefinition attributeDefinition, JsonNode value, boolean stopOnError) {
        this.stopOnError = stopOnError;
        this.attributeDefinition = attributeDefinition;
        this.value = value;
        this.attributeValidatorReport = new AttributeValidatorReport();
    }

    public boolean isStopOnError() {
        return stopOnError;
    }

    public AttributeDefinition getAttributeDefinition() {
        return attributeDefinition;
    }

    public JsonNode getValue() {
        return value;
    }

    public AttributeValidatorReport getAttributeValidatorReport() {
        return attributeValidatorReport;
    }

    /**
     * Add an error message to the report and log it
     * @param sb
     */
    protected void addError(StringBuffer sb){
        logger.trace(sb.toString());
        attributeValidatorReport.addMessage(sb.toString());
    }

    protected StringBuffer getNewStringBuffer(StringBuffer sb){
        StringBuffer sbNew = null;
        if(sb==null){
            sbNew = new StringBuffer();
            sbNew.append("The field '").append(attributeDefinition.getName()).append("'");
        } else {
            sbNew = new StringBuffer(sb);
        }
        return sbNew;
    }

    /**
     * Validate a number against the attribute definition
     * @param number
     * @param sb
     * @return
     */
    protected boolean validateNumber(Number number, StringBuffer sb){ 
        Integer max = null;
        Integer min = null;
        
        if(sb==null){
            max = attributeDefinition.getMax();
            min = attributeDefinition.getMin();
        }
        
        boolean valid = true;

        if(max!=null && number.doubleValue() > max){
            valid = false;
            StringBuffer sbInner = getNewStringBuffer(sb);
            sbInner.append(number).append("is greater than the maximum value ").append(max);
            addError(sbInner);
            if(stopOnError){
                return valid;
            }
        }

        if(min!=null && number.doubleValue() < min){
            valid = false;
            StringBuffer sbInner = getNewStringBuffer(sb);
            sbInner.append(number).append(" is lower than the minimum value ").append(min);
            addError(sbInner);
        }

        return valid;
    }

    protected boolean typeNotCompliant(JsonNode value, String expectedType, StringBuffer sb){ 
        if(sb==null){
            sb = new StringBuffer();
            sb.append("The field '").append(attributeDefinition.getName());
        }
        StringBuffer sbInner = new StringBuffer(sb);
        sbInner.append(value).append("' is not a ").append(expectedType).append(" but it is a ").append(value.getNodeType().name());
        addError(sbInner);
        return false;
    }
    
    protected boolean validateString(JsonNode value, StringBuffer sb) {
        boolean valid = true;  
        
        String regex = null;
        if(sb==null){
            regex = attributeDefinition.getRegexp();
        }

        if (!value.isTextual()) {
            valid = typeNotCompliant(value, BaseType.STRING.toString(), sb);
        }else{
            String stringValue = value.asText();
            if(regex!=null && regex.length()>0 && !stringValue.matches(regex)){
                valid = false;
                StringBuffer sbInner = getNewStringBuffer(sb);
                sbInner.append(" does not match the pattern ").append(regex);
                addError(sbInner);
            }
        }

        return valid;
    }

    protected boolean validateDate(JsonNode value, StringBuffer sb) {
        boolean valid = true;  
        
        if (!value.isTextual()) {
            valid = typeNotCompliant(value, BaseType.DATE.toString(), sb);
        }else{
            String stringValue = value.asText();
            try {
                AttributeUtility.checkDateTimeString(stringValue);
            } catch (DateTimeParseException e) {
                valid = false;
                StringBuffer sbInner = getNewStringBuffer(sb);
                sbInner.append(" is not a valid date according to the pattern ").append(Element.DATETIME_PATTERN);
                addError(sbInner);
            }
        }

        return valid;
    }

    protected boolean validateByte(JsonNode value, StringBuffer sb) {
        boolean valid = true;

        if (!value.isBinary()) {
            valid = typeNotCompliant(value, BaseType.BYTE.toString(), sb);
        }else {
            byte[] binaryValue;
            try {
                binaryValue = value.binaryValue();
                if(binaryValue.length > 1) {
                    valid = false;
                    StringBuffer sbInner = getNewStringBuffer(sb);
                    sbInner.append(" is a bynary but is longer than a byte (i.e. 8 bits)");
                    addError(sbInner);
                }
            } catch (IOException e) {
                valid = false;
                StringBuffer sbInner = getNewStringBuffer(sb);
                sbInner.append("' is not a byte");
                addError(sbInner);
            }
        }
        return valid;
    }

    protected StringBuffer getPropertyStringBuffer(StringBuffer sb, String expectedPropertyType, boolean fromMultipleInstaces){
        StringBuffer sbInner = getNewStringBuffer(sb);
        if(fromMultipleInstaces){
            sbInner.append(" has one instance that is not a '").append(expectedPropertyType).append("'");
        }else{
            sbInner.append(" is not a '").append(expectedPropertyType).append("'");
        }
        return sbInner;

    }

    protected boolean checkPropertyTypeCompliance(JsonNode value, String expectedPropertyType, StringBuffer sb, boolean fromMultipleInstaces) {
        boolean valid = true;
        
       if (!value.isObject() || !value.has(Element.TYPE_PROPERTY)) {
            valid = false;
            StringBuffer sbInner = getPropertyStringBuffer(sb, expectedPropertyType, fromMultipleInstaces);
            addError(sbInner);
            return valid;
        }
       
        try {
            Property p = ElementMapper.getObjectMapper().treeToValue(value, Property.class);
            StringBuffer log = new StringBuffer();
            if(fromMultipleInstaces){
                log.append("For ");
                log.append(getNewStringBuffer(sb)); // The field 'Pippo'
                log.append(" one of the instances is '");
            }else{
                log.append(getNewStringBuffer(sb));
                log.append(" is ");
            }

            /* 
             * I don't want to trust the information available in the provided instance
             * otherwise I could have used the following code:
             */
            /* 
            if (expectedPropertyType.compareTo(p.getTypeName()) == 0) {
                log.append(expectedPropertyType);
                log.append("' as expected.");
                logger.trace(log.toString());
            } else if (expectedPropertyType.compareTo(p.getExpectedtype()) == 0) {
                log.append(expectedPropertyType);
                log.append("' as expected. Jackson used the best available supertype '");
                log.append(p.getTypeName());
                log.append("' to instatiate it.");
                logger.trace(log.toString());
            } else if (p.getSupertypes().contains(expectedPropertyType)) {
                log.append(p.getTypeName());
                log.append("' that is a subclass of '");
                log.append(expectedPropertyType);
                log.append("' and this is totally fine.");
                logger.trace(log.toString());
            } else {
                valid = false;
                StringBuffer sbInner = getPropertyStringBuffer(sb, expectedPropertyType, fromMultipleInstaces);
                addError(sbInner);
            }
            */
            
            TypesKnowledge tk = TypesKnowledge.getInstance();
            Tree<Type> tree = tk.getModelKnowledge().getTree(AccessType.PROPERTY);

            if (expectedPropertyType.compareTo(p.getTypeName()) == 0) {
                log.append(expectedPropertyType);
                log.append("' as expected.");
                logger.trace(log.toString());
            } else if(tree.isChildOf(expectedPropertyType, p.getTypeName())){
                log.append(p.getTypeName());
                log.append("' that is a subclass of '");
                log.append(expectedPropertyType);
                log.append("' and this is totally fine.");
                logger.trace(log.toString()); 
            }else if(tree.isParentOf(expectedPropertyType, p.getTypeName()) && expectedPropertyType.compareTo(p.getExpectedtype()) == 0){
                log.append(expectedPropertyType);
                log.append("' as expected. Jackson used the best supertype available in the classpath i.e. '");
                log.append(p.getTypeName());
                log.append("' to instatiate it.");
                logger.trace(log.toString());
            } else {
                valid = false;
                StringBuffer sbInner = getPropertyStringBuffer(sb, expectedPropertyType, fromMultipleInstaces);
                addError(sbInner);
            }

        }catch(JsonProcessingException e){
            valid = false;
            StringBuffer sbInner = getPropertyStringBuffer(sb, expectedPropertyType, fromMultipleInstaces);
            addError(sbInner);
        }
        
        return valid;
    }

    private boolean checkArrayOrSet(JsonNode value, String expectedType) {
        boolean valid = true;

        Integer max = attributeDefinition.getMax();
        Integer min = attributeDefinition.getMin();

        ArrayNode arrayNode = (ArrayNode) value;
        
        if(min!=null && arrayNode.size()<min){
            valid = false;
            StringBuffer sbInner = getNewStringBuffer(null);
            sbInner.append(" contains less than the minimum number of elements").append(min);
            addError(sbInner);
            if(stopOnError){
                return valid;
            }
        }

        if(max!=null && arrayNode.size()>max){
            valid = false;
            StringBuffer sbInner = getNewStringBuffer(null);
            sbInner.append(" contains more than the maximum number of elements ").append(max);
            addError(sbInner);
            if(stopOnError){
                return valid;
            }
        }

        PropertyTypeName pt = new PropertyTypeName(expectedType);
        BaseType genericBaseType = pt.getGenericBaseType();
        String expectedPropertyType = pt.getGenericClassName();
        if(expectedPropertyType==null){
            expectedPropertyType = genericBaseType.toString();
        }

        for(JsonNode element : arrayNode){
            valid = validate(expectedPropertyType, element, getNewStringBuffer(null)) && valid;
            if(!valid && stopOnError){
                return valid;
            }
        }
        return valid;
    }
    
    protected boolean validate(String expectedType, JsonNode value, StringBuffer sb){
        boolean valid = true;
        
        PropertyTypeName pt = new PropertyTypeName(expectedType);
        BaseType baseType = pt.getBaseType();
        
        boolean entry = false;
        if(sb==null){
            entry = true;
        }

        switch (baseType) {
            case ANY:
                // Everything is valid. Nothing to do.
                break;

            case BOOLEAN:
                if (!value.isBoolean()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }
                break;
            
            case INTEGER:
                if (!value.isInt()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                } else {
                    Integer intValue = value.asInt();
                    valid = validateNumber(intValue, sb) && valid;
                }
                break;
            
            case SHORT:
                if (!value.isShort()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                } else {
                    Short shortValue = value.shortValue();
                    valid = validateNumber(shortValue, sb) && valid;
                }
                break;
            
            case LONG:
                if (!value.isLong()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                } else {
                    Long longValue = value.asLong();
                    valid = validateNumber(longValue, sb) && valid;
                }
                break;
            
            case FLOAT:
                if (!value.isFloat()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                } else {
                    Float floatValue = value.floatValue();
                    valid = validateNumber(floatValue, sb) && valid;
                }
                break;
            
            case DOUBLE:
                if (!value.isDouble()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                } else {
                    Double doubleValue = value.asDouble();
                    valid = validateNumber(doubleValue, sb) && valid;
                }
                break;
            
            case DATE:
                valid = validateDate(value, sb) && valid;;
                break;

            case STRING:
                valid = validateString(value, sb) && valid;;
                break;

            case BINARY:
                if (!value.isBinary()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }
                break;
            
            case BYTE:
                valid = validateByte(value, sb) && valid;
                break;
            
            case JSON_OBJECT:
                if (!value.isObject()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }
                // Nothing more to check
                break;
            
            case JSON_ARRAY:
                if (!value.isArray()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }
                // Nothing more to check
                break;
            
            case PROPERTY:
                valid = checkPropertyTypeCompliance(value, expectedType, sb, false) && valid;
                break;
            
            case LIST:
            case SET:
                if(!entry){
                    StringBuffer sbInner = getNewStringBuffer(null);
                    sbInner.append(" has nested ").append(baseType.toString().toLowerCase());
                    sbInner.append(" and this is not allowed.");
                    addError(sb);
                    valid = false;
                    break;
                }
                if (!value.isArray()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }else{
                    checkArrayOrSet(value, expectedType);
                }
                break;
            
            case MAP:
                if(!entry){
                    StringBuffer sbInner = getNewStringBuffer(null);
                    sbInner.append(" has nested ").append(baseType.toString().toLowerCase());
                    sbInner.append(" and this is not allowed.");
                    addError(sbInner);
                    valid = false;
                    break;
                }
                if (!value.isObject()) {
                    valid = typeNotCompliant(value, expectedType, sb);
                }else{
                    ObjectNode objectNode = (ObjectNode) value;
                    Iterator<JsonNode> iterator = objectNode.elements();
                    while(iterator.hasNext()){
                        JsonNode element = iterator.next();
                        valid = checkPropertyTypeCompliance(element, pt.getGenericClassName(), getNewStringBuffer(sb), true) && valid;
                        if(!valid && stopOnError){
                            return valid;
                        }
                    }
                }
                break;
            
            default:
                break;
        }

        return valid;
    }

    public boolean validate() {
        boolean valid = true;
        if(value!=null){
            if(value instanceof NullNode){
                if(attributeDefinition.isNotnull()){
                    valid = false;
                    StringBuffer sb = getNewStringBuffer(null);
                    sb.append(" cannot be null");
                    addError(sb);
                }
                /* 
                 * The validation is done because
                 * there are no more controls to do
                 * on null values
                 */
            }else{
                valid = validate(attributeDefinition.getPropertyType(), value, null);
            }
        }else{
            // Value is null
            if(attributeDefinition.isMandatory()){
                valid = false;
                StringBuffer sb = getNewStringBuffer(null);
                sb.append(" is mandatory but it is not present in the instance");
                addError(sb);
            }
        }
        attributeValidatorReport.setValid(valid);
        return valid;
    }
    
}
