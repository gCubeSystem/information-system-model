package org.gcube.informationsystem.types.impl.relations;

import java.io.StringWriter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.impl.TypeImpl;
import org.gcube.informationsystem.types.impl.entities.EntityTypeImpl;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.relations.ConsistsOfType;
import org.gcube.informationsystem.types.reference.relations.IsRelatedToType;
import org.gcube.informationsystem.types.reference.relations.RelationType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = RelationType.NAME)
public class RelationTypeImpl<S extends EntityType, T extends EntityType>
		extends TypeImpl implements RelationType<S,T> {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2221831081869571296L;
	
	protected RelationTypeImpl() {
		super();
	}
	
	public static RelationType<?,?> getRelationTypeDefinitionInstance(
			Class<? extends RelationElement<?,?>> clz) {
		if(IsRelatedTo.class.isAssignableFrom(clz)) {
			@SuppressWarnings("unchecked")
			Class<? extends IsRelatedTo<? extends Resource,? extends Resource>> c = (Class<? extends IsRelatedTo<? extends Resource,? extends Resource>>) clz;
			return new IsRelatedToTypeImpl(c);
		} else if(ConsistsOf.class.isAssignableFrom(clz)) {
			@SuppressWarnings("unchecked")
			Class<? extends ConsistsOf<? extends Resource,? extends Facet>> c = (Class<? extends ConsistsOf<? extends Resource,? extends Facet>>) clz;
			return new ConsistsOfTypeImpl(c);
		}
		return new RelationTypeImpl<EntityType,EntityType>(clz);
	}
	
	public RelationTypeImpl(Class<? extends RelationElement<?,?>> clz) {
		super(clz);
		
		if(RelationType.class.isAssignableFrom(clz)) {
			@SuppressWarnings({"unchecked"})
			Class<RelationType<?,?>> c = (Class<RelationType<?,?>>) clz;
			this.extendedTypes = retrieveSuperClasses(c, RelationType.class, RelationElement.NAME);
		} else if(IsParentOf.class.isAssignableFrom(clz)) {
			@SuppressWarnings("unchecked")
			Class<? extends IsParentOf> c = (Class<? extends IsParentOf>) clz;
			this.extendedTypes = retrieveSuperClasses(c, IsParentOf.class, RelationElement.NAME);
		} else if(RelationElement.class.isAssignableFrom(clz)) {
			this.extendedTypes = retrieveSuperClasses(clz, RelationElement.class, null);
		} else {
			throw new RuntimeException("Type Hierachy Error");
		}
		
		this.properties = retrieveListOfProperties(clz);
		
		discoverSourceAndTarget(clz);
		
	}
	
	@SuppressWarnings("unchecked")
	protected Type[] getParametersFromSuperClasses(Class<? extends RelationElement<?,?>> clz) {
		for(Type t : clz.getGenericInterfaces()) {
			if(t instanceof ParameterizedType) {
				if(RelationElement.class.isAssignableFrom((Class<?>) ((ParameterizedType) t).getRawType())) {
					return ((ParameterizedType) t).getActualTypeArguments();
				}
			} else if(RelationElement.class.isAssignableFrom((Class<?>) t)) {
				return getParametersFromSuperClasses((Class<? extends RelationElement<?,?>>) t);
			}
		}
		throw new RuntimeException("Unable to find Generic Parameters From SuperClasses to set source and target");
	}
	
	@SuppressWarnings("unchecked")
	private void discoverSourceAndTarget(Class<? extends RelationElement<?,?>> clz) {
		Type[] typeParameters = clz.getTypeParameters();
		
		Class<? extends EntityElement> sourceClass;
		Class<? extends EntityElement> targetClass;
		if(typeParameters.length == 0) {
			typeParameters = getParametersFromSuperClasses(clz);
			sourceClass = (Class<? extends EntityElement>) typeParameters[0];
			targetClass = (Class<? extends EntityElement>) typeParameters[1];
		} else {
			sourceClass = (Class<? extends EntityElement>) getGenericClass(typeParameters[0]);
			targetClass = (Class<? extends EntityElement>) getGenericClass(typeParameters[1]);
		}
		
		this.source = (S) new EntityTypeImpl(sourceClass);
		this.target = (T) new EntityTypeImpl(targetClass);
		
	}
	
	/* 
	 * Java does not support class multiple inheritance. 
	 * TypeDefinitionImpl is the superclass so that this class does not inherits the methods and field of BaseRelationImpl
	 * We need to copy them. 
	 */
	
	protected S source;
	protected T target;
	
	@Override
	public S getSource() {
		return source;
	}
	
	@Override
	public void setSource(S source) {
		this.source = source;
	}
	
	@Override
	public T getTarget() {
		return target;
	}
	
	@Override
	public void setTarget(T target) {
		this.target = target;
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		
		if(name.compareTo(IsParentOf.NAME)==0) {
			return AccessType.IS_PARENT_OF;
		}
		
		if(name.compareTo(RelationElement.NAME)==0) {
			return AccessType.RELATION_ELEMENT;
		}
		
		if(name.compareTo(RelationType.NAME)==0 ||
		   name.compareTo(IsRelatedToType.NAME)==0 ||
		   name.compareTo(ConsistsOfType.NAME)==0) {
			return AccessType.RELATION_TYPE;
		}
		
		return AccessType.RELATION;
	}

	@Override
	public String toString(){
		StringWriter stringWriter = new StringWriter();
		try {
			ElementMapper.marshal(this, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			try {
				ElementMapper.marshal(this.metadata, stringWriter);
				return stringWriter.toString();
			} catch(Exception e1){
				return super.toString();
			}
		}
	}
}
