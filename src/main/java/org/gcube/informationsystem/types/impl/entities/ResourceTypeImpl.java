package org.gcube.informationsystem.types.impl.entities;

import java.util.ArrayList;
import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.impl.properties.LinkedEntityImpl;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ResourceType.NAME)
public final class ResourceTypeImpl extends EntityTypeImpl implements ResourceType {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5964819173421808432L;
	
	protected List<LinkedEntity> facets;
	protected List<LinkedEntity> resources;
	
	protected ResourceTypeImpl() {
		super();
	}
	
	public ResourceTypeImpl(Class<? extends Resource> clz) {
		super(clz);
		
		this.extendedTypes = retrieveSuperClasses(clz, Resource.class, Entity.NAME);
		
		setResourceSchemaEntries(clz);
	}
	
	private void setResourceSchemaEntries(Class<? extends Resource> clz){
		if(clz.isAnnotationPresent(ResourceSchema.class)) {
			this.facets = new ArrayList<>();
			this.resources = new ArrayList<>();
			ResourceSchema[] resourceSchemaArray = clz.getAnnotationsByType(ResourceSchema.class);
			for(ResourceSchemaEntry resourceSchemaEntry : resourceSchemaArray[0].facets()) {
				LinkedEntity linkedEntity = new LinkedEntityImpl();
				linkedEntity.setSource(TypeMapper.getType(clz));
				linkedEntity.setRelation(TypeMapper.getType(resourceSchemaEntry.relation()));
				linkedEntity.setTarget(TypeMapper.getType(resourceSchemaEntry.facet()));
				linkedEntity.setDescription(resourceSchemaEntry.description());
				linkedEntity.setMin(resourceSchemaEntry.min());
				linkedEntity.setMax(resourceSchemaEntry.max());
				this.facets.add(linkedEntity);
			}
			for(RelatedResourcesEntry resourceSchemaRelatedEntry : resourceSchemaArray[0].resources()) {
				LinkedEntity linkedEntity = new LinkedEntityImpl();
				linkedEntity.setSource(TypeMapper.getType(resourceSchemaRelatedEntry.source()));
				linkedEntity.setRelation(TypeMapper.getType(resourceSchemaRelatedEntry.relation()));
				linkedEntity.setTarget(TypeMapper.getType(resourceSchemaRelatedEntry.target()));
				linkedEntity.setDescription(resourceSchemaRelatedEntry.description());
				/* 
				 * We cannot impose any constraint on related resources.
				 * The annotation are only for documentation purpose.
				 */
				linkedEntity.setMin(0);
				linkedEntity.setMax(-1);
				this.resources.add(linkedEntity);
			}
		}
	}
	
	@Override
	public List<LinkedEntity> getFacets() {
		return facets;
	}
	
	@Override
	public List<LinkedEntity> getResources() {
		return resources;
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		return AccessType.RESOURCE;
	}
	
}
