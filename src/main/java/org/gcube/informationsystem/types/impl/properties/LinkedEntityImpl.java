/**
 * 
 */
package org.gcube.informationsystem.types.impl.properties;

import java.util.Objects;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.properties.PropertyElementImpl;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=LinkedEntity.NAME)
public final class LinkedEntityImpl extends PropertyElementImpl implements LinkedEntity {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -531978995960170532L;
	
	protected String source;
	protected String relation;
	protected String target;
	protected String description;
	protected Integer min;
	protected Integer max;

	public LinkedEntityImpl() {
		super();
	}
	
	@Override
	public String getSource() {
		return source;
	}
	
	@Override
	public void setSource(String source) {
		this.source = source;
	}
	
	@Override
	public String getRelation() {
		return relation;
	}
	
	@Override
	public void setRelation(String relation) {
		this.relation = relation;
	} 

	@Override
	public String getTarget() {
		return target;
	}

	@Override
	public void setTarget(String target) {
		this.target = target;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public Integer getMin() {
		return min;
	}
	
	@Override
	public void setMin(Integer min) {
		this.min = min;
	}
	
	@Override
	public Integer getMax() {
		return max;
	}
	
	@Override
	public void setMax(Integer max) {
		if(max==null || max<=0) {
			this.max = null;
		}else {
			this.max = max;
		}
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(source, relation, target, min, max, description);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LinkedEntityImpl other = (LinkedEntityImpl) obj;
		return Objects.equals(source, other.source) && 
			   Objects.equals(relation, other.relation) && 
			   Objects.equals(target, other.target) && 
			   Objects.equals(min, other.min) && 
			   Objects.equals(max, other.max) &&
			   Objects.equals(description, other.description);
	}
	
	protected int compareIntegers(Integer thisInt, Integer otherInt) {
		Integer thisInteger = thisInt == null ? Integer.MAX_VALUE : thisInt;
		Integer otherInteger = otherInt == null ? Integer.MAX_VALUE : otherInt;
		return thisInteger.compareTo(otherInteger);
	}
	
	@Override
	public int compareTo(LinkedEntity other) {
		if (this == other) {
			return 0;
		}
		if (other == null) {
			return -1;
		}
		if (getClass() != other.getClass()) {
			return -1;
		}
		
		LinkedEntityImpl o = (LinkedEntityImpl) other;
		
		int ret = 0;
		ret = source.compareTo(o.source);
		if(ret != 0) {
			return ret;
		}
		
		ret = relation.compareTo(o.relation);
		if(ret != 0) {
			return ret;
		}
		
		ret = target.compareTo(o.target);
		if(ret != 0) {
			return ret;
		}
		
		ret = compareIntegers(max, o.max);
		if(ret != 0) {
			return ret;
		}
		
		ret = compareIntegers(min, o.min);
		if(ret != 0) {
			return ret;
		}
		
		return description.compareTo(o.description);
		
	}
}
