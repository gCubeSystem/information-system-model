package org.gcube.informationsystem.types.impl.relations;

import java.lang.reflect.Type;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.types.impl.entities.ResourceTypeImpl;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.relations.IsRelatedToType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=IsRelatedToType.NAME)
public final class IsRelatedToTypeImpl extends RelationTypeImpl<ResourceType, ResourceType> implements IsRelatedToType {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 8448665395204514722L;
	
	protected IsRelatedToTypeImpl() {
		super();
	}
	
	public IsRelatedToTypeImpl(Class<? extends IsRelatedTo<? extends Resource, ? extends Resource>> clz) {
		super(clz);
		
		this.extendedTypes = retrieveSuperClasses(clz, IsRelatedTo.class, Relation.NAME);
		
		this.properties = retrieveListOfProperties(clz);
		
		discoverSourceAndTarget(clz);
	}
	
	@SuppressWarnings("unchecked")
	private void discoverSourceAndTarget(Class<? extends IsRelatedTo<? extends Resource, ? extends Resource>> clz) {
		Type[] typeParameters = clz.getTypeParameters();
		
		Class<? extends Resource> sourceClass;
		Class<? extends Resource> targetClass;
		if(typeParameters.length==0) {
			typeParameters = getParametersFromSuperClasses(clz);
			sourceClass = (Class<? extends Resource>) typeParameters[0];
			targetClass = (Class<? extends Resource>) typeParameters[1];
		}else {
			sourceClass = (Class<? extends Resource>) getGenericClass(typeParameters[0]);
			targetClass = (Class<? extends Resource>) getGenericClass(typeParameters[1]);
		}
		
		this.source = new ResourceTypeImpl((Class<? extends Resource>) sourceClass);
		this.target = new ResourceTypeImpl((Class<? extends Resource>) targetClass);
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		return AccessType.IS_RELATED_TO;
	}
	
}
