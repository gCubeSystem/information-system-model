package org.gcube.informationsystem.types.impl.properties;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.impl.TypeImpl;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.properties.PropertyType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = PropertyType.NAME)
public final class PropertyTypeImpl extends TypeImpl
		implements PropertyType {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7532701373450638829L;
	
	protected Set<PropertyDefinition> properties;
	
	private static Set<String> propertyElementAccesTypes;
	
	static {
		propertyElementAccesTypes = new HashSet<>();
		propertyElementAccesTypes.add(PropertyElement.NAME);
		propertyElementAccesTypes.add(LinkedEntity.NAME);
		propertyElementAccesTypes.add(TemplateVariable.NAME);
	}
	
	
	protected PropertyTypeImpl() {
		super();
	}
	
	public PropertyTypeImpl(Class<? extends PropertyElement> clz) {
		super(clz);
		this.extendedTypes = retrieveSuperClasses(clz, PropertyElement.class,
				clz == PropertyElement.class ? null : PropertyElement.NAME);
		
		this.properties = retrieveListOfProperties(clz);
	}
	
	@Override
	public Set<PropertyDefinition> getProperties() {
		return properties;
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		
		if(name.compareTo(PropertyType.NAME)==0) {
			return AccessType.PROPERTY_TYPE;
		}
		
		if(name.compareTo(PropertyDefinition.NAME)==0) {
			return AccessType.PROPERTY_DEFINITION;
		}
		
		if(propertyElementAccesTypes.contains(name)) {
			return AccessType.PROPERTY_ELEMENT;
		}
		
		return AccessType.PROPERTY;
	}
	
	@Override
	public String toString(){
		StringWriter stringWriter = new StringWriter();
		try {
			ElementMapper.marshal(this, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			try {
				ElementMapper.marshal(this.metadata, stringWriter);
				return stringWriter.toString();
			} catch(Exception e1){
				return super.toString();
			}
		}
	}
	
}