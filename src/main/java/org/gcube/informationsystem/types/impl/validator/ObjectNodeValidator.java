package org.gcube.informationsystem.types.impl.validator;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.AttributeDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is used to validate an ObjectNode against a list of AttributeDefinitions.
 * It produces a report with the validation results.
 * @author Luca Frosini (ISTI - CNR)
 */
public class ObjectNodeValidator {

    protected static Logger logger = LoggerFactory.getLogger(ObjectNodeValidator.class);

    
    protected final ObjectNode nodeToValidate;
    protected final List<? extends AttributeDefinition> attributeDefinitions;
    protected final boolean stopOnError;

    protected ValidatorReport validatorReport;
    
    public ObjectNodeValidator(ObjectNode nodeToValidate, List<? extends AttributeDefinition> attributeDefinitions) {
        this(nodeToValidate, attributeDefinitions, false);
    }

    public ObjectNodeValidator(ObjectNode nodeToValidate, List<? extends AttributeDefinition> attributeDefinitions, Boolean stopOnError) {
        this.nodeToValidate = nodeToValidate;
        this.attributeDefinitions = attributeDefinitions;
        this.stopOnError = stopOnError;
    }

    public ValidatorReport getValidatorReport() {
        return validatorReport;
    }

    public boolean isValid() {
        return validatorReport.isValid();
    }

    public boolean validate() {
        this.validatorReport = new ValidatorReport();
        boolean valid = true;
        for(AttributeDefinition ad : attributeDefinitions){
            String key = ad.getName();
            AttributeValidator attributeValidator = new AttributeValidator(ad, nodeToValidate.get(key), stopOnError);
            valid = attributeValidator.validate() && valid;
            validatorReport.addAttributeValidatorReport(attributeValidator.getAttributeValidatorReport());
            if(!valid && stopOnError){
                return false;
            }
        }
        return validatorReport.isValid();
    }

    

}
