package org.gcube.informationsystem.types.impl.entities;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=FacetType.NAME)
public final class FacetTypeImpl extends EntityTypeImpl implements FacetType {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 6268161046955738969L;
	
	protected Set<PropertyDefinition> properties;
	
	protected FacetTypeImpl() {
		super();
	}
	
	public FacetTypeImpl(Class<? extends Facet> clz) {
		super(clz);
		this.extendedTypes = retrieveSuperClasses(clz, Facet.class, Entity.NAME);
		this.properties = retrieveListOfProperties(clz);
	}
	
	@Override
	public Set<PropertyDefinition> getProperties() {
		return properties;
	}

	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		return AccessType.FACET;
	}
	
}
