package org.gcube.informationsystem.types.impl.entities;

import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.types.impl.TypeImpl;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.entities.ResourceType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=EntityType.NAME)
public class EntityTypeImpl extends TypeImpl implements EntityType {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2614315845213942880L;

	private static Set<String> entityTypeAccesTypes;
	
	static {
		entityTypeAccesTypes = new HashSet<>();
		entityTypeAccesTypes.add(EntityType.NAME);
		entityTypeAccesTypes.add(ResourceType.NAME);
		entityTypeAccesTypes.add(FacetType.NAME);
	}
	
	
	// private static final String VERTEX_CLASS_NAME = "V";
	
	public static EntityType getEntityTypeDefinitionInstance(Class<? extends EntityElement> clz) {
		if(Resource.class.isAssignableFrom(clz)) {
			@SuppressWarnings("unchecked")
			Class<? extends Resource> c = (Class<? extends Resource>) clz;
			return new ResourceTypeImpl((Class<? extends Resource>) c);
		} else if(Facet.class.isAssignableFrom(clz)){
			@SuppressWarnings("unchecked")
			Class<? extends Facet> c = (Class<? extends Facet>) clz;
			return new FacetTypeImpl(c);
		} 
		return new EntityTypeImpl(clz);
	}
	
	
	protected EntityTypeImpl() {
		super();
	}
	
	public EntityTypeImpl(Class<? extends EntityElement> clz) {
		super(clz);
		
		if(EntityType.class.isAssignableFrom(clz)){
			@SuppressWarnings("unchecked")
			Class<? extends EntityType> c = (Class<? extends EntityType>) clz;
			this.extendedTypes = retrieveSuperClasses(c, EntityType.class, EntityElement.NAME);
		} else if(Context.class.isAssignableFrom(clz)){
			@SuppressWarnings("unchecked")
			Class<? extends Context> c = (Class<? extends Context>) clz;
			this.extendedTypes = retrieveSuperClasses(c, Context.class, EntityElement.NAME);
		} else if(QueryTemplate.class.isAssignableFrom(clz)){
			@SuppressWarnings("unchecked")
			Class<? extends QueryTemplate> c = (Class<? extends QueryTemplate>) clz;
			this.extendedTypes = retrieveSuperClasses(c, QueryTemplate.class, EntityElement.NAME);
		} else if(EntityElement.class.isAssignableFrom(clz)){
			this.extendedTypes = retrieveSuperClasses(clz, EntityElement.class, null);
		} else {
			throw new RuntimeException("Type Hierachy Error for class " + clz.getSimpleName());
		}

		this.properties = retrieveListOfProperties(clz);

	}

	/* 
	 * Java does not support multiple inheritance. 
	 * TypeDefinitionImpl is the superclass so that this class does not inherits the methods and field of BaseRelationImpl
	 * We need to copy them. 
	 */
	
	protected Metadata metadata;
	
	@Override
	public Metadata getMetadata() {
		return metadata;
	}
	
	@Override
	public void setMetadata(Metadata metadata){
		this.metadata = metadata;
	}
	
	@Override
	public String toString(){
		StringWriter stringWriter = new StringWriter();
		try {
			ElementMapper.marshal(this, stringWriter);
			return stringWriter.toString();
		}catch(Exception e){
			try {
				ElementMapper.marshal(this.metadata, stringWriter);
				return stringWriter.toString();
			} catch(Exception e1){
				return super.toString();
			}
		}
	}
	
	@Override
	@JsonIgnore
	public AccessType getAccessType() {
		
		if(name.compareTo(Context.NAME)==0) {
			return AccessType.CONTEXT;
		}
		
		if(name.compareTo(EntityElement.NAME)==0) {
			return AccessType.ENTITY_ELEMENT;
		}
		
		if(name.compareTo(QueryTemplate.NAME)==0) {
			return AccessType.QUERY_TEMPLATE;
		}
		
		if(entityTypeAccesTypes.contains(name)) {
			return AccessType.ENTITY_TYPE;
		}
		
		return AccessType.ENTITY;
	}

}
