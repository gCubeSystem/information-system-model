package org.gcube.informationsystem.types.impl.validator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AttributeValidatorReport {

    String fieldName;
    boolean valid;
    List<String> messages;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void addMessage(String message) {
        if(this.messages==null) {
            this.messages = new ArrayList<>();
        }
        this.messages.add(message);
    }

}
