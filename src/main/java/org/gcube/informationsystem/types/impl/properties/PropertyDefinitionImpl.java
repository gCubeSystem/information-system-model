package org.gcube.informationsystem.types.impl.properties;

import java.lang.reflect.Method;
import java.net.URI;
import java.net.URL;
import java.util.Objects;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.types.PropertyTypeName;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.annotations.GetReturnType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.AttributeUtility;
import org.gcube.informationsystem.utils.TypeUtility;
import org.gcube.informationsystem.utils.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
// @JsonAutoDetect(fieldVisibility=JsonAutoDetect.Visibility.ANY)
@JsonTypeName(value=PropertyDefinition.NAME)
public final class PropertyDefinitionImpl implements PropertyDefinition {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5925314595659292025L;

	private static Logger logger = LoggerFactory.getLogger(PropertyDefinitionImpl.class);
	
	public final static String UUID_REGEX = "^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}$";
	public final static String URI_REGEX = null;
	public final static String URL_REGEX = null;
	
	private String name= "";
	private String description= "";
	private boolean mandatory = false;
	private boolean readonly = false;
	private boolean notnull = false;
	private Integer max= null;
	private Integer min= null;
	private String regexp= null;
	private PropertyTypeName propertyTypeName = null; 
	private Object defaultValue = null;

	private static String getPropertyNameFromMethodName(Method method){
		String name = method.getName();
		if(name.startsWith("get")){
			name = name.replace("get", "");

		}
		if(name.startsWith("is")){
			name = name.replace("is", "");
		}
		
		if(name.length() > 0){
			name = Character.toLowerCase(name.charAt(0)) + (name.length() > 1 ? name.substring(1) : "");
		}
		
		return name;
	}
	
	protected PropertyDefinitionImpl() {
		
	}
	
	public PropertyDefinitionImpl(ISProperty propertyAnnotation, Method method) {
		String name = propertyAnnotation.name().isEmpty()?getPropertyNameFromMethodName(method):propertyAnnotation.name();
		this.name = name;
		this.description = propertyAnnotation.description();
		this.mandatory= propertyAnnotation.mandatory();
		this.notnull = !propertyAnnotation.nullable();
		this.readonly = propertyAnnotation.readonly();
		if(propertyAnnotation.max()>0) { 
			this.max = propertyAnnotation.max();
		}
		if(propertyAnnotation.max()>=propertyAnnotation.min() && propertyAnnotation.min()>0) {
			this.min = propertyAnnotation.min();
		}
		
		Class<?> clz = propertyAnnotation.type();
		if(clz==GetReturnType.class) {
			// We have to read the return of the method
			this.propertyTypeName = new PropertyTypeName(method);
			clz = method.getReturnType();
			logger.trace("Return Type for method {} is {}", method, clz);
		}else {
			this.propertyTypeName = new PropertyTypeName(clz);
		}
		
		String defaultValueAsString = propertyAnnotation.defaultValue();
		defaultValueAsString = AttributeUtility.evaluateNullForDefaultValue(defaultValueAsString);
		
		// The default value is evaluated to test if compliant with the declared type
		this.defaultValue = AttributeUtility.evaluateValueStringAccordingBaseType(propertyTypeName.getBaseType(), defaultValueAsString);
		
		if(!propertyAnnotation.regexpr().isEmpty()) { 
			this.regexp = propertyAnnotation.regexpr();
		}else {
			if(Enum.class.isAssignableFrom(clz)){
				Object[] constants = clz.getEnumConstants();
				StringBuilder stringBuilder = new StringBuilder("^(");
				for(int i=0; i<constants.length; i++){
					stringBuilder.append(constants[i].toString());
					if(i<constants.length-1){
						stringBuilder.append("|");
					}
				}
				stringBuilder.append(")$");
				this.regexp = stringBuilder.toString();
			}
			if(UUID.class.isAssignableFrom(clz)){
				this.regexp = UUID_REGEX;
			}
			if(URI.class.isAssignableFrom(clz)){
				this.regexp = URI_REGEX;
			}
			if(URL.class.isAssignableFrom(clz)){
				this.regexp = URL_REGEX;
			}
			if(Version.class.isAssignableFrom(clz)){
				this.regexp = Version.VERSION_REGEX;
			}
		}

	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getDescription() {
		return description;
	}
	
	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public boolean isMandatory() {
		return mandatory;
	}

	@Override
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	@Override
	public boolean isReadonly() {
		return readonly;
	}
	
	@Override
	public void setReadonly(boolean readonly) {
		this.readonly = readonly;
	}
	
	@Override
	public boolean isNotnull() {
		return notnull;
	}

	@Override
	public void setNotnull(boolean notnull) {
		this.notnull = notnull;
	}
	
	@Override
	public Integer getMax() {
		return max;
	}
	
	@Override
	public void setMax(Integer max) {
		this.max = max;
	}
	
	@Override
	public Integer getMin() {
		return min;
	}
	
	@Override
	public void setMin(Integer min) {
		this.min = min;
	}
	
	@Override
	public String getRegexp() {
		return regexp;
	}
	
	@Override
	public void setRegexp(String regexp) {
		AttributeUtility.checkRegex(regexp, null);
		this.regexp = regexp;
	}
	
	@Override
	public String getPropertyType() {
		return propertyTypeName.toString();
	}

	@JsonSetter(value = PropertyDefinition.PROPERTY_TYPE_PROPERTY)
	public void setPropertyType(String type) {
		this.propertyTypeName = new PropertyTypeName(type);
	}
	
	@JsonIgnore
	public PropertyTypeName getPropertyTypeName() {
		return propertyTypeName;
	}
	
	@Override
	public Object getDefaultValue() {
		return defaultValue;
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "PropertyDefinition ["
				+ "name=" + name 
				+ ", description=" + description
				+ ", mandatory=" + mandatory 
				+ ", readonly=" + readonly
				+ ", notnull=" + notnull
				+ ", max=" + max 
				+ ", min=" + min
				+ ", regexpr=" + regexp 
				+ ", type=" + propertyTypeName.toString() 
				+ ", defaultValue=" + (defaultValue == null ? "null" : defaultValue.toString())
				+ "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, description, mandatory, readonly, notnull, max, min, regexp, propertyTypeName.toString(), defaultValue);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PropertyDefinitionImpl other = (PropertyDefinitionImpl) obj;
		return Objects.equals(name, other.name) && 
				Objects.equals(description, other.description) && 
				mandatory == other.mandatory &&
				readonly == other.readonly &&
				notnull == other.notnull && 
				Objects.equals(max, other.max) && 
				Objects.equals(min, other.min) && 
				Objects.equals(regexp, other.regexp) &&
				Objects.equals(propertyTypeName.toString(), other.propertyTypeName.toString()) &&
				Objects.equals(defaultValue, other.defaultValue);
	}

	protected int compareIntegers(Integer thisInt, Integer otherInt) {
		Integer thisInteger = thisInt == null ? Integer.MAX_VALUE : thisInt;
		Integer otherInteger = otherInt == null ? Integer.MAX_VALUE : otherInt;
		return thisInteger.compareTo(otherInteger);
	}
	
	@Override
	public int compareTo(PropertyDefinition other) {
		if (this == other) {
			return 0;
		}
		if (other == null) {
			return -1;
		}
		if (getClass() != other.getClass()) {
			return -1;
		}
		
		PropertyDefinitionImpl o = (PropertyDefinitionImpl) other;
		
		int ret = 0;
		ret = name.compareTo(o.name);
		if(ret != 0) {
			return ret;
		}
		
		ret = description.compareTo(o.description);
		if(ret != 0) {
			return ret;
		}
		
		ret = Boolean.compare(mandatory, o.mandatory);
		if(ret != 0) {
			return ret;
		}
		
		ret = Boolean.compare(readonly, o.readonly);
		if(ret != 0) {
			return ret;
		}
		
		ret = Boolean.compare(notnull, o.notnull);
		if(ret != 0) {
			return ret;
		}
		
		ret = compareIntegers(max, o.max);
		if(ret != 0) {
			return ret;
		}
		
		ret = compareIntegers(min, o.min);
		if(ret != 0) {
			return ret;
		}
		
		if(regexp==null && o.regexp!=null) {
			return -1;
		}
		
		if(o.regexp==null && regexp!=null) {
			return -1;
		}
		
		if(!(regexp==null && o.regexp==null)) {
			ret = regexp.compareTo(o.regexp);
			if(ret != 0) {
				return ret;
			}
		}
		
		ret = propertyTypeName.toString().compareTo(o.propertyTypeName.toString());
		if(ret != 0) {
			return ret;
		}
		
		
		if(defaultValue==null && o.defaultValue!=null) {
			return -1;
		}
		
		if(o.defaultValue==null && defaultValue!=null) {
			return -1;
		}
		
		if(defaultValue!=null && o.defaultValue!=null) {
			if(defaultValue.getClass()!=o.defaultValue.getClass()) {
				return -1;
			}
			
			// TODO
		}
		
		return ret;
	}

	@Override
	public String getTypeName() {
		return TypeUtility.getTypeName(this.getClass());
	}
	
}
