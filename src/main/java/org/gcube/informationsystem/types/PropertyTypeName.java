package org.gcube.informationsystem.types;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.net.URI;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.utils.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PropertyTypeName {

	private static Logger logger = LoggerFactory.getLogger(PropertyTypeName.class);
	
	public enum BaseTypeGroup {
		
		ANY( BaseType.values()),
		BOOLEAN( new BaseType[]{ BaseType.BOOLEAN } ),
		NUMERIC( new BaseType[]{ BaseType.INTEGER, BaseType.SHORT, BaseType.LONG, BaseType.FLOAT }),
		STRING( new BaseType[]{ BaseType.STRING }),
		DATE( new BaseType[]{ BaseType.DATE } ),
		BITS( new BaseType[]{ BaseType.BINARY, BaseType.BYTE } ),
		COMPLEX( new BaseType[]{ BaseType.PROPERTY } ),
		COLLECTION( new BaseType[]{ BaseType.LIST, BaseType.SET } ),
		MAP( new BaseType[]{ BaseType.MAP });
		
		private final BaseType[] group;
		
		private BaseTypeGroup(BaseType[] group) {
			this.group = group;
		}
		
		public BaseType[] getGroup() {
			return group;
		}
		
	}
	
	public enum BaseType {
		
		ANY("Any"),
		
		BOOLEAN("Boolean"),
		
		INTEGER("Integer"),
		
		SHORT("Short"),
		
		LONG("Long"),
		
		FLOAT("Float"),
		
		DOUBLE("Double"),
		
		DATE("Date"),
		
		STRING("String"),
		
		BINARY("Binary"),
		
		BYTE("Byte"),

		JSON_OBJECT("JsonObject"),
		
		JSON_ARRAY("JsonArray"),
		
		/* 
		 * All base types which DON'T have a generic must be defined BEFORE BaseType.PROPERTY
		 */
		
		PROPERTY("Property"),
		
		/* 
		 * All base types which have a generic must be defined AFTER BaseType.PROPERTY
		 */
		
		LIST("List"),
		
		SET("Set"),
		
		MAP("Map");
		
		private final String stringValue;
	
		BaseType(final String stringValue) {
			this.stringValue = stringValue;
		}
		
		@Override
		public String toString() {
			return stringValue;
		}
		
		protected static final Map<String, BaseType> map;
		
		static {
			map = new HashMap<>();
			for(BaseType baseType : BaseType.values()) {
				map.put(baseType.stringValue, baseType);
			}
		}
		
		public static BaseType getBaseTypeFromString(String type) {
			BaseType baseType = map.get(type);
			if(baseType==null) {
				throw new IllegalArgumentException("No BaseType having '" + type + "' as string value. Allowed values are " + map.keySet());
			}
			return baseType;
		}
	}
	
	protected static final Map<Class<?>,BaseType> BASE_PROPERTY_TYPES_BY_CLASS;
	protected static final Map<Class<?>,String> REGEX_BY_CLASS_MAPPED_AS_STRING;
	protected static final Map<String,String> REGEX_BY_CLASS_MAPPED_AS_STRING_BY_CLASS_NAME;
	
	static {
		BASE_PROPERTY_TYPES_BY_CLASS = new HashMap<>();
		
		// This is made by hand because not all types should be add.
		BASE_PROPERTY_TYPES_BY_CLASS.put(Boolean.TYPE, BaseType.BOOLEAN);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Boolean.class, BaseType.BOOLEAN);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Integer.TYPE, BaseType.INTEGER);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Integer.class, BaseType.INTEGER);
		BASE_PROPERTY_TYPES_BY_CLASS.put(BigInteger.class, BaseType.INTEGER);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Short.TYPE, BaseType.SHORT);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Short.class, BaseType.SHORT);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Long.TYPE, BaseType.LONG);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Long.class, BaseType.LONG);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Float.TYPE, BaseType.FLOAT);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Float.class, BaseType.FLOAT);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Double.TYPE, BaseType.DOUBLE);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Double.class, BaseType.DOUBLE);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Date.class, BaseType.DATE);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Calendar.class, BaseType.DATE);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(String.class, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Character.TYPE, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Character.class, BaseType.STRING);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Byte.TYPE, BaseType.BYTE);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Byte.class, BaseType.BYTE);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(byte[].class, BaseType.BINARY);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Byte[].class, BaseType.BINARY);

		BASE_PROPERTY_TYPES_BY_CLASS.put(ObjectNode.class, BaseType.JSON_OBJECT);
		BASE_PROPERTY_TYPES_BY_CLASS.put(ArrayNode.class, BaseType.JSON_ARRAY);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(PropertyElement.class, BaseType.PROPERTY);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(List.class, BaseType.LIST);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Set.class, BaseType.SET);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Map.class, BaseType.MAP);
		
		BASE_PROPERTY_TYPES_BY_CLASS.put(Enum.class, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(URI.class, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(URL.class, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(UUID.class, BaseType.STRING);
		BASE_PROPERTY_TYPES_BY_CLASS.put(Version.class, BaseType.STRING);
		
		
		REGEX_BY_CLASS_MAPPED_AS_STRING = new HashMap<>();
		REGEX_BY_CLASS_MAPPED_AS_STRING.put(URI.class, PropertyTypeName.URI_REGEX);
		REGEX_BY_CLASS_MAPPED_AS_STRING.put(URL.class, PropertyTypeName.URL_REGEX);
		REGEX_BY_CLASS_MAPPED_AS_STRING.put(UUID.class,PropertyTypeName.UUID_REGEX);
		REGEX_BY_CLASS_MAPPED_AS_STRING.put(Version.class, Version.VERSION_REGEX);
		
		REGEX_BY_CLASS_MAPPED_AS_STRING_BY_CLASS_NAME = new HashMap<>();
		for(Class<?> clz : REGEX_BY_CLASS_MAPPED_AS_STRING.keySet()) {
			String className = clz.getSimpleName();
			String regex = REGEX_BY_CLASS_MAPPED_AS_STRING.get(clz);
			REGEX_BY_CLASS_MAPPED_AS_STRING_BY_CLASS_NAME.put(className, regex);
		}
		
	}
	
	public final static String URI_REGEX = null;
	public final static String URL_REGEX = null;
	public final static String UUID_REGEX = "^([a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}){1}$";
	
	
	public static String getRegexByClass(Class<?> clz) {
		if(Enum.class.isAssignableFrom(clz)){
			Object[] constants = clz.getEnumConstants();
			StringBuilder stringBuilder = new StringBuilder("^(");
			for(int i=0; i<constants.length; i++){
				stringBuilder.append(constants[i].toString());
				if(i<constants.length-1){
					stringBuilder.append("|");
				}
			}
			stringBuilder.append(")$");
			return stringBuilder.toString();
		}
		return REGEX_BY_CLASS_MAPPED_AS_STRING.get(clz);
	}
	
	public static String getRegexByClassname(String className) {
		return REGEX_BY_CLASS_MAPPED_AS_STRING_BY_CLASS_NAME.get(className);
	}
	
	/**
	 * Return the correspondent type by checking the "assignability" of the 
	 * class received as parameter.
	 * 
	 * @param clazz Class to check
	 * @return BasePropertyType instance if found, otherwise null
	 */
	public static BaseType getBaseTypeByClass(final Class<?> clazz) {
		if(clazz == null) {
			return null;
		}
		
		if(clazz == Object.class) {
			return BaseType.ANY;
		}
		
		BaseType type = BASE_PROPERTY_TYPES_BY_CLASS.get(clazz);
		if(type == null) {
			for(Class<?> c : BASE_PROPERTY_TYPES_BY_CLASS.keySet()) {
				if(c.isAssignableFrom(clazz)) {
					type = BASE_PROPERTY_TYPES_BY_CLASS.get(c);
				}
			}
		}
		return type;
	}
	
	protected BaseType baseType;
	protected boolean genericType;
	protected BaseType genericBaseType;
	protected String genericClassName;
	
	protected Class<?> clz;
	
	public PropertyTypeName(String type) {
		setType(type);
	}
	
	public PropertyTypeName(Class<?> clz) {
		baseType = null;
		genericType = false;
		genericBaseType = null;
		genericClassName = null;
		
		logger.trace("The type is {}", clz);
		
		baseType = PropertyTypeName.getBaseTypeByClass(clz);
		
		if(baseType == BaseType.PROPERTY){
			if(clz != PropertyElement.class){
				@SuppressWarnings("unchecked")
				Class<? extends Element> type = (Class<? extends Element>) clz; 
				genericType = true;
				genericClassName = TypeMapper.getType(type);
				return;
			}
		}
		
	}
	
	public PropertyTypeName(Method method) {
		baseType = null;
		genericType = false;
		genericBaseType = null;
		genericClassName = null;
		
		Class<?> clz = method.getReturnType();
		logger.trace("Return Type for method {} is {}", method, clz);
		
		baseType = PropertyTypeName.getBaseTypeByClass(clz);
		
		if(baseType == BaseType.PROPERTY){
			if(clz != PropertyElement.class){
				@SuppressWarnings("unchecked")
				Class<? extends Element> type = (Class<? extends Element>) clz; 
				genericType = true;
				genericClassName = TypeMapper.getType(type);
				return;
			}
		}
		
		if(baseType!=null) {
			if(baseType.ordinal()>BaseType.PROPERTY.ordinal()){
				genericType = true;
				
				java.lang.reflect.Type genericReturnType = method.getGenericReturnType();
				logger.trace("Generic Type for method {} is {}", method, genericReturnType);
				
				java.lang.reflect.Type[] actualTypeArguments = ((ParameterizedType) genericReturnType).getActualTypeArguments();
				
				java.lang.reflect.Type genericType = null;
				for(java.lang.reflect.Type t : actualTypeArguments){
					logger.trace("Generic Type {} for method {} - Actual Type Argument : {}", genericReturnType, method, t);
					genericType = t;
				}
				
				genericBaseType = PropertyTypeName.getBaseTypeByClass((Class<?>) genericType);
				if(genericBaseType!= null && genericBaseType.ordinal()>BaseType.PROPERTY.ordinal()) {
					throw new IllegalArgumentException("The generic of a " + baseType.toString() + " cannot be a Set, a List or a Map.");
				}
				
				if(genericBaseType == BaseType.PROPERTY){
					@SuppressWarnings("unchecked")
					Class<? extends Element> genericElementType = (Class<? extends Element>) genericType;
					genericClassName = TypeMapper.getType(genericElementType);
				}
				
			}
		}else {
			throw new IllegalArgumentException("Type " + clz.getSimpleName() + " not supported as property type");
		}
	}
	
	public void setType(String type) {
		int indexOfMinor = type.indexOf("<");
		if(indexOfMinor == -1) {
			genericType = false;
		}else {
			genericType = true; 
		}
		
		if(!genericType) {
			try {
				baseType = BaseType.getBaseTypeFromString(type);
				if(baseType.ordinal()>BaseType.PROPERTY.ordinal()) {
					throw new IllegalArgumentException("Set, List and Map must specify a generic type, e.g. Set<Property>, List<Integer>.");
				}
				return;
			}catch (IllegalArgumentException e) {
				baseType = BaseType.PROPERTY;
				genericType = true;
				genericClassName = type;
				return;
			}
		}
		
		String cleanTypeFromGeneric = type.substring(0,indexOfMinor);
		baseType = BaseType.getBaseTypeFromString(cleanTypeFromGeneric);
		if(genericType) {
			if(baseType.ordinal()<=BaseType.PROPERTY.ordinal()) {
				throw new IllegalArgumentException("Only Set, List and Map can be generic. Map can only have String as Key");
			}
		}
		
		// Getting the path inside <....>
		String generic = type.substring(indexOfMinor+1, type.length()-1);
		
		if(baseType == BaseType.MAP) {
			int l = generic.length();
			generic = generic.replace(BaseType.STRING.toString() + ",", "");
			if(l!=(BaseType.STRING.stringValue.length() + 1 + generic.length())) {
				throw new IllegalArgumentException("A Map can only has String as key e.g. Map<String,Integer>");
			}
		}
		
		try {
			genericBaseType = BaseType.getBaseTypeFromString(generic);
		}catch (IllegalArgumentException e) {
			genericBaseType = BaseType.PROPERTY;
			genericClassName = generic;
		}
		
		if(genericBaseType!= null && genericBaseType.ordinal()>BaseType.PROPERTY.ordinal()) {
			throw new IllegalArgumentException("The generic of a " + baseType.toString() + " cannot be a Set, a List or a Map.");
		}
		
		
	}
	
	public String getType() {
		StringBuffer stringBuffer = new StringBuffer();
		
		switch (baseType) {
		
			case PROPERTY:
				if(genericClassName != null) {
					stringBuffer.append(genericClassName);
				}else {
					stringBuffer.append(baseType.toString());
				}
				break;
			
			case SET: case LIST: case MAP:
				stringBuffer.append(baseType.toString());
				stringBuffer.append("<");
				if(baseType == BaseType.MAP) {
					stringBuffer.append(BaseType.STRING.toString());
					stringBuffer.append(",");
				}
				
				if(genericClassName != null) {
					stringBuffer.append(genericClassName);
				}else {
					stringBuffer.append(genericBaseType.toString());
				}
				
				stringBuffer.append(">");
				break;
			
			default:
				stringBuffer.append(baseType.toString());
				break;
		}
		
		return stringBuffer.toString();
	}
	
	@Override
	public String toString() {
		return getType();
	}
	

	public BaseType getBaseType() {
		return baseType;
	}

	public boolean isGenericType() {
		return genericType;
	}
	
	public BaseType getGenericBaseType() {
		return genericBaseType;
	}

	public String getGenericClassName() {
		return genericClassName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(baseType, genericBaseType, genericClassName, genericType);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyTypeName other = (PropertyTypeName) obj;
		return baseType == other.baseType && genericBaseType == other.genericBaseType
				&& Objects.equals(genericClassName, other.genericClassName) && genericType == other.genericType;
	}

}
