package org.gcube.informationsystem.types.reference;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Documented
@Target(TYPE)
@Repeatable(Changelog.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface Change {

	String version();
	
	String description();
	
}
