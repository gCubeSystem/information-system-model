package org.gcube.informationsystem.types.reference.properties;

import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.properties.PropertyTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = PropertyTypeImpl.class)
@TypeMetadata(name = PropertyType.NAME, description = "This type provides information for any PropertyType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface PropertyType extends PropertyElement, Type {
	
	public static final String NAME = "PropertyType"; // PropertyType.class.getSimpleName();
	
	@JsonGetter(value = ID_PROPERTY)
	@ISProperty(name = ID_PROPERTY, description = "This UUID is be used to identify the instance univocally.", readonly = true, mandatory = true, nullable = false)
	@Override
	public UUID getID();

	@JsonSetter(value = ID_PROPERTY)
	@Override
	public void setID(UUID uuid);
	
	@JsonGetter(value = METADATA_PROPERTY)
	@ISProperty(name=METADATA_PROPERTY, mandatory=true, nullable=false, description="Metadata associated with the instance that is automatically created/updated by the system.")
	@Override
	public Metadata getMetadata();
	
	@JsonSetter(value = METADATA_PROPERTY)
	@Override
	public void setMetadata(Metadata metadata);
	
	/* Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
	@Override
	@ISProperty(name = Type.NAME_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	@Override
	@ISProperty(name = Type.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	@Override
	@ISProperty(name = Type.ABSTRACT_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public boolean isAbstract();

	@Override
	@ISProperty(name = Type.EXTENDED_TYPES_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public Set<String> getExtendedTypes();
	
	@ISProperty(name = Type.PROPERTIES_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Set<PropertyDefinition> getProperties();
	
	/*Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
}
