package org.gcube.informationsystem.types.reference.relations;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.relations.RelationTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = RelationTypeImpl.class)
@TypeMetadata(name = RelationType.NAME, description = "This type provides information for any RelationType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface RelationType<S extends EntityType, T extends EntityType>
		extends Type, RelationElement<S,T> {
	
	public static final String NAME = "RelationType"; // RelationType.class.getSimpleName();
	
	/* Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
	@Override
	@ISProperty(name = Type.NAME_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	@Override
	@ISProperty(name = Type.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	@Override
	@ISProperty(name = Type.ABSTRACT_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public boolean isAbstract();
	
	@Override
	@ISProperty(name = Type.EXTENDED_TYPES_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public Set<String> getExtendedTypes();
	
	@Override
	@ISProperty(name = Type.PROPERTIES_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Set<PropertyDefinition> getProperties();
	
	@Override
	public S getSource();
	
	@Override
	public T getTarget();
	
	/* Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
}
