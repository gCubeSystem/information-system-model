package org.gcube.informationsystem.types.reference.relations;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.impl.relations.ConsistsOfTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.entities.FacetType;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = ConsistsOfTypeImpl.class)
@TypeMetadata(name = ConsistsOfType.NAME, description = "This type provides information for any ConsistsOfType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface ConsistsOfType extends RelationType<ResourceType, FacetType> {
	
	public static final String NAME = "ConsistsOfType"; // ConsistsOfType.class.getSimpleName();
	
}
