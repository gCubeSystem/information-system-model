package org.gcube.informationsystem.types.reference;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.properties.Metadata;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY })
@TypeMetadata(name = Type.NAME, description = "This is the base type to define any Type", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
//@JsonDeserialize(as=TypeImpl.class) Do not uncomment to manage subclasses
public interface Type extends IdentifiableElement {
	
	public static final String NAME = "Type"; // Type.class.getSimpleName();
	
	public static final String NAME_PROPERTY = "name";
	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String VERSION_PROPERTY = "version";
	public static final String CHANGELOG_PROPERTY = "changelog";
	public static final String ABSTRACT_PROPERTY = "abstract";
	public static final String FINAL_PROPERTY = "final";
	/* Cannot use 'supertypes' which conflicts with Element */
	public static final String EXTENDED_TYPES_PROPERTY = "extendedTypes";
	public static final String PROPERTIES_PROPERTY = "properties";
	
	@JsonGetter(value = ID_PROPERTY)
	@Override
	public UUID getID();

	@JsonSetter(value = ID_PROPERTY)
	@Override
	public void setID(UUID uuid);
	
	@JsonGetter(value = METADATA_PROPERTY)
	@Override
	public Metadata getMetadata();
	
	@JsonSetter(value = METADATA_PROPERTY)
	@Override
	public void setMetadata(Metadata metadata);
	
	public String getName();
	
	public String getDescription();

	@JsonIgnore
	public Version getVersion();
	
	@JsonGetter(value = VERSION_PROPERTY)
	public String getVersionAsString();

	@JsonIgnore
	public Map<Version, String> getChangelog();
	
	@JsonGetter(value = CHANGELOG_PROPERTY)
	public Map<String, String> getChangelogWithVersionAsString();
	
	@JsonGetter(value = ABSTRACT_PROPERTY)
	public boolean isAbstract();

	@JsonGetter(value = FINAL_PROPERTY)
	public boolean isFinal();
	
	@JsonGetter(value = EXTENDED_TYPES_PROPERTY)
	public Set<String> getExtendedTypes();
	
	public Set<PropertyDefinition> getProperties();

	@JsonIgnore
	public AccessType getAccessType();
	
}
