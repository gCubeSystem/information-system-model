package org.gcube.informationsystem.types.reference;

import static java.lang.annotation.ElementType.TYPE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@Documented
@Target(TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TypeMetadata {

	String name() default "";
	
	String description() default "";
	
	String version() default Version.MINIMAL_VERSION_STRING;
	
}
