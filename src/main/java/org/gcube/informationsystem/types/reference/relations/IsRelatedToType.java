package org.gcube.informationsystem.types.reference.relations;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.impl.relations.IsRelatedToTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = IsRelatedToTypeImpl.class)
@TypeMetadata(name = IsRelatedToType.NAME, description = "This type provides information for any IsRelatedToType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface IsRelatedToType extends RelationType<ResourceType, ResourceType> {
	
	public static final String NAME = "IsRelatedToType"; // IsRelatedToType.class.getSimpleName();
	
}
