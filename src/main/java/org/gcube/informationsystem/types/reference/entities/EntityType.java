package org.gcube.informationsystem.types.reference.entities;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.entities.EntityTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = EntityTypeImpl.class)
@TypeMetadata(name = EntityType.NAME, description = "This type provides information for any EntityType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface EntityType extends Type, EntityElement {

	public static final String NAME = "EntityType"; //EntityType.class.getSimpleName();

	/* Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
	@Override
	@ISProperty(name = Type.NAME_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	@Override
	@ISProperty(name = Type.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	@Override
	@ISProperty(name = Type.ABSTRACT_PROPERTY, mandatory = true, nullable = false)
	public boolean isAbstract();

	@Override
	@ISProperty(name = Type.EXTENDED_TYPES_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public Set<String> getExtendedTypes();

	/* Type is just a Java useful class. The type is not created in the IS. Hence the fields must be redefined */
	
}
