package org.gcube.informationsystem.types.reference.entities;

import java.util.List;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.entities.ResourceTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ResourceTypeImpl.class)
@TypeMetadata(name = ResourceType.NAME, description = "This type provides information for the definition of any ResourceType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface ResourceType extends EntityType {
	
	public static final String NAME = "ResourceType"; //ResourceType.class.getSimpleName();
	
	public static final String FACETS_PROPERTY = "facets";
	public static final String RESOURCES_PROPERTY = "resources";
	
	@ISProperty(name = FACETS_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public List<LinkedEntity> getFacets();
	
	@ISProperty(name = RESOURCES_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public List<LinkedEntity> getResources();
	
}
