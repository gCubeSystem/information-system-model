package org.gcube.informationsystem.types.reference.entities;

import java.util.Set;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.entities.FacetTypeImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = FacetTypeImpl.class)
@TypeMetadata(name = FacetType.NAME, description = "This type provides information for the definition of any FacetType", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface FacetType extends EntityType {

	public static final String NAME = "FacetType"; //FacetType.class.getSimpleName();

	@ISProperty(name = Type.PROPERTIES_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Set<PropertyDefinition> getProperties();
	
}
