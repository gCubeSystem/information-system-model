package org.gcube.informationsystem.types.reference.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Attribute;
import org.gcube.informationsystem.base.reference.AttributeDefinition;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.properties.PropertyDefinitionImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Changelog;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize(as = PropertyDefinitionImpl.class)
@TypeMetadata(name = PropertyDefinition.NAME, description = "This type provides information for the definition of any properties", version = PropertyDefinition.VERSION)
@Changelog ({
	@Change(version = PropertyDefinition.VERSION, description = "The type comply with {@link AttributeDefinition} which add 'defaultValue'"),
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
})
@Final
public interface PropertyDefinition extends PropertyElement, Comparable<PropertyDefinition>, AttributeDefinition {
	
	public static final String NAME = "PropertyDefinition"; // PropertyDefinition.class.getSimpleName();
	public static final String VERSION = "1.1.0";

	public static final String READ_ONLY_PROPERTY = "readonly";
	
	@ISProperty(name = Attribute.NAME_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	@ISProperty(name = Attribute.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	@ISProperty(name = AttributeDefinition.MANDATORY_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "false")
	public boolean isMandatory();
	
	@ISProperty(name = PropertyDefinition.READ_ONLY_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "false")
	public boolean isReadonly();

	public void setReadonly(boolean readonly);
	
	@ISProperty(name = AttributeDefinition.NOT_NULL_PROPERTY, readonly = false, mandatory = true, nullable = false, defaultValue = "false")
	public boolean isNotnull();
	
	@ISProperty(name = Attribute.MAX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Integer getMax();
	
	@ISProperty(name = Attribute.MIN_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Integer getMin();
	
	@ISProperty(name = Attribute.REGEX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getRegexp();
	
	@ISProperty(name = Attribute.PROPERTY_TYPE_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getPropertyType();
	
	@ISProperty(name = Attribute.DEFAULT_VALUE_PROPERTY, readonly = false, mandatory = false, nullable = true)
	public Object getDefaultValue();
	
}
