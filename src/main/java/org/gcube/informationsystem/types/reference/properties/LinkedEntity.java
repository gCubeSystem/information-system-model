package org.gcube.informationsystem.types.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.impl.properties.LinkedEntityImpl;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * A convenient type to define a Resource in terms of:
 * - mandatory/recommended ConsistsOf->Facets;
 * - suggested IsRelatedTo->Resource.
 *  
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=LinkedEntityImpl.class)
@TypeMetadata(name = LinkedEntity.NAME, description = " A convenient type to define a Resource in terms of: mandatory/recommended ConsistsOf->Facets; suggested IsRelatedTo->Resource.", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface LinkedEntity extends PropertyElement, Comparable<LinkedEntity> {

	public static final String NAME = "LinkedEntity"; // LinkedEntity.class.getSimpleName();
	
	public static final String SOURCE_PROPERTY = "source";
	public static final String RELATION_PROPERTY = "relation";
	public static final String TARGET_PROPERTY = "target";
	public static final String DESCRIPTION_PROPERTY = "description";
	public static final String MIN_PROPERTY = "min";
	public static final String MAX_PROPERTY = "max";
	
	@ISProperty(name = SOURCE_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getSource();
	
	public void setSource(String source);
	
	@ISProperty(name = RELATION_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getRelation();
	
	public void setRelation(String relation);

	@ISProperty(name = TARGET_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getTarget();
	
	public void setTarget(String target);

	@ISProperty(name = DESCRIPTION_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public String getDescription();
	
	public void setDescription(String description);
	
	@ISProperty(name = MIN_PROPERTY, readonly = true, mandatory = true, nullable = false)
	public Integer getMin();
	
	public void setMin(Integer min);
	
	@ISProperty(name = MAX_PROPERTY, readonly = true, mandatory = true, nullable = true)
	public Integer getMax();
	
	public void setMax(Integer max);
	
}