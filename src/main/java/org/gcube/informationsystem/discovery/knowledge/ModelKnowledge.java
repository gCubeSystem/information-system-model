package org.gcube.informationsystem.discovery.knowledge;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.Discovery;
import org.gcube.informationsystem.serialization.ElementMappingAction;
import org.gcube.informationsystem.tree.Tree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ModelKnowledge {
	
	private static final Logger logger = LoggerFactory.getLogger(ModelKnowledge.class);
	
	protected boolean created;
	protected Map<AccessType, UsageKnowledge> usageKnowledges;
	protected Map<AccessType, Tree<Class<Element>>> trees;
	protected Map<AccessType, Discovery<Element>> discoveries;
	protected Set<Package> packages;
	
	public ModelKnowledge() {
		this.packages = new HashSet<>();
		reset();
	}
	
	private void reset() {
		this.created = false;
		this.usageKnowledges = new HashMap<>();
		this.trees = new HashMap<>();
		this.discoveries = new HashMap<>();
	}
	
	public UsageKnowledge getUsageKnowledge(AccessType accessType) {
		if(!created) {
			throw new RuntimeException("You must invoke createKnowledge() first");
		}
		return usageKnowledges.get(accessType);
	}
	
	public synchronized void createKnowledge(boolean force) throws Exception {
		if(force) {
			logger.info("Going to force Knowledge recreation");
			reset();
		}
		createKnowledge();
	}
	
	public void addPackages(Collection<Package> packages) {
		this.packages.addAll(packages);
	}
	
	public synchronized void createKnowledge() throws Exception {	
		
		if(created) {
			logger.trace("Knowledge has been already created.");
			return;
		}
		
		AccessType[] modelTypes = AccessType.getModelTypes();
		
		ClassInformation classInformation = new ClassInformation();
		
		for(AccessType accessType : modelTypes) {
			
			Class<Element> clz = accessType.getTypeClass();
			Tree<Class<Element>> tree = new Tree<>(clz, classInformation);
			trees.put(accessType, tree);
			AddElementToTreeAction aetta = new AddElementToTreeAction(tree);
			if(accessType == AccessType.RESOURCE) {
				UsageKnowledge facetKnowledge = new UsageKnowledge();
				this.usageKnowledges.put(AccessType.FACET, facetKnowledge);
				aetta.setFacetKnowledge(facetKnowledge);
				
				UsageKnowledge resourceKnowledge = new UsageKnowledge();
				this.usageKnowledges.put(AccessType.RESOURCE, resourceKnowledge);
				aetta.setResourceKnowledge(resourceKnowledge);
			}
			
			Discovery<Element> discovery = new Discovery<>(clz);
			discoveries.put(accessType, discovery);
			discovery.addPackages(packages);
			discovery.addDiscoveredElementActions(new ElementMappingAction());
			discovery.addDiscoveredElementActions(aetta);
			
			discovery.discover();
		}
		
		this.created = true;
		
	}
	
	public synchronized Tree<Class<Element>> getClassesTree(AccessType accessType) {
		return trees.get(accessType);
	}
	
	
	public synchronized Discovery<Element> getDiscovery(AccessType accessType) {
		return discoveries.get(accessType);
	}
}
