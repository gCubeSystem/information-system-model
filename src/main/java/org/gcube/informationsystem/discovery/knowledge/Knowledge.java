package org.gcube.informationsystem.discovery.knowledge;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ServiceLoader;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.Discovery;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.tree.Tree;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Knowledge {

	public static Logger logger = LoggerFactory.getLogger(Knowledge.class);
	
	private static Knowledge instance;
	
	public static Knowledge getInstance() throws Exception {
		return getInstance(false);
	}
	
	public static Knowledge getInstance(boolean forceRediscover) throws Exception {
		if(forceRediscover) {
			instance = null;
		}
		
		if(instance == null) {
			instance = new Knowledge();
			instance.discover();
		}
		return instance;
	}
	
	protected ModelKnowledge allKnowledge;
	protected Map<String, ModelKnowledge> modelKnowledges;
	protected Map<String, RegistrationProvider> registrationProviders;
	
	private Knowledge() {
	}
	
	public ModelKnowledge getAllKnowledge() {
		return allKnowledge;
	}
	
	public ModelKnowledge getModelKnowledge(RegistrationProvider rp) {
		return modelKnowledges.get(rp.getModelName());
	}
	
	public ModelKnowledge getModelKnowledge(String modelName) {
		return modelKnowledges.get(modelName);
	}
	
	public void validateModelKnowledge(RegistrationProvider rp) throws Exception {
		ModelKnowledge modelKnowledge = getModelKnowledge(rp);
		ModelKnowledgeValidator ra = new ModelKnowledgeValidator(rp);
		
		AccessType[] accessTypes = AccessType.getModelTypes();
		for(AccessType accessType : accessTypes) {
			logger.trace("Going to analise discovered types of '{}' for model '{}'", accessType.getName(), rp.getModelName());
			Discovery<Element> discovery = modelKnowledge.getDiscovery(accessType);
			discovery.executeDiscoveredElementActions(ra);
			
			logger.trace("Going to analise tree of '{}' type for model '{}'", accessType.getName(), rp.getModelName());
			Tree<Class<Element>> tree = modelKnowledge.getClassesTree(accessType);
			tree.elaborate(ra);
		}
	}
	
	public void validateModelKnowledge(String modelName) throws Exception {
		RegistrationProvider rp = registrationProviders.get(modelName);
		validateModelKnowledge(rp);
	}
	
	public void discover() throws Exception {
		
		allKnowledge = new ModelKnowledge();
		
		modelKnowledges = new LinkedHashMap<>();
		registrationProviders = new LinkedHashMap<>();
		
		ServiceLoader<? extends RegistrationProvider> providers = ServiceLoader
				.load(RegistrationProvider.class);
		
		for(RegistrationProvider rp : providers) {
			
			registrationProviders.put(rp.getModelName(), rp);
			
			ModelKnowledge modelKnowledge = new ModelKnowledge();
			modelKnowledges.put(rp.getModelName(), modelKnowledge);
			modelKnowledge.addPackages(rp.getPackagesToRegister());
			modelKnowledge.createKnowledge();
			
			allKnowledge.addPackages(rp.getPackagesToRegister());
		}
		
		allKnowledge.createKnowledge();
		
	}
	
	
	
}
