/**
 * 
 */
package org.gcube.informationsystem.discovery.knowledge;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.DiscoveredElementAction;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.ResourceType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AddElementToTreeAction implements DiscoveredElementAction<Element> {

	protected Tree<Class<Element>> tree;
	
	protected UsageKnowledge facetKnowledge;
	protected UsageKnowledge resourceKnowledge;
	
	public UsageKnowledge getFacetKnowledge() {
		return facetKnowledge;
	}

	public void setFacetKnowledge(UsageKnowledge facetKnowledge) {
		this.facetKnowledge = facetKnowledge;
	}

	public UsageKnowledge getResourceKnowledge() {
		return resourceKnowledge;
	}

	public void setResourceKnowledge(UsageKnowledge resourceKnowledge) {
		this.resourceKnowledge = resourceKnowledge;
	}

	public AddElementToTreeAction(Tree<Class<Element>> tree) {
		this.tree = tree;
	}
	
	@Override
	public void analizeElement(Class<Element> e) throws Exception {
		tree.addNode(e);
		createUsageKnowledge(e);
	}
	
	protected void createUsageKnowledge(Class<Element> e) {
		if (e.isAssignableFrom(Resource.class)) {
			return;
		}
		
		if(facetKnowledge!=null && resourceKnowledge!=null) {
			Type type = TypeMapper.createTypeDefinition(e);
			ResourceType rt = (ResourceType) type;
			facetKnowledge.addAll(rt.getFacets());
			resourceKnowledge.addAll(rt.getResources());
		}
	}

}
