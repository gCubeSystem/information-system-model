package org.gcube.informationsystem.discovery.knowledge;

import java.util.LinkedHashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.knowledge.TypeInformation;
import org.gcube.informationsystem.types.TypeMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ClassInformation implements TypeInformation<Class<Element>> {

	@Override
	public String getIdentifier(Class<Element> clz) {
		return TypeMapper.getType(clz);
	}

	@Override
	public Set<String> getParentIdentifiers(Class<Element> root, Class<Element> clz) {
		Set<String> ret = new LinkedHashSet<>();
		if(clz == root) {
			return ret;
		}
		
		Class<?>[] interfaces = clz.getInterfaces();
		for(Class<?> i : interfaces) {
			if(root.isAssignableFrom(i)) {
				@SuppressWarnings("unchecked")
				String id = getIdentifier((Class<Element>) i); 
				ret.add(id);
			}
		}
		return ret;
	}

	@Override
	public AccessType getAccessType(Class<Element> clz) {
		return AccessType.getAccessType(clz);
	}

	@Override
	public Class<Element> getRoot(AccessType accessType) {
		return accessType.getTypeClass();
	}

}
