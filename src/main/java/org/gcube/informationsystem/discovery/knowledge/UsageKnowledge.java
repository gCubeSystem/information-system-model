package org.gcube.informationsystem.discovery.knowledge;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.gcube.informationsystem.types.reference.properties.LinkedEntity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class UsageKnowledge {
	
	protected Map<String, Set<LinkedEntity>> map;
	
	public UsageKnowledge(){
		this.map = new LinkedHashMap<>();
	}
	
	protected void add(String typeName, LinkedEntity linkedEntity) {
		Set<LinkedEntity> list = map.get(typeName);
		if(list==null) {
			list = new TreeSet<>();
			map.put(typeName, list);
		}
		list.add(linkedEntity);
	}
	
	public void add(LinkedEntity linkedEntity) {
		if(linkedEntity!=null) {
			String source = linkedEntity.getSource();
			add(source, linkedEntity);
			String relation = linkedEntity.getRelation();
			add(relation, linkedEntity);
			String target = linkedEntity.getTarget();
			add(target, linkedEntity);
		}
	}
	
	public void addAll(Collection<LinkedEntity> linkedEntities) {
		if(linkedEntities!=null) {
			for(LinkedEntity le : linkedEntities) {
				add(le);
			}
		}
	}
	
	public Set<LinkedEntity> getUsage(String typeName){
		Set<LinkedEntity> list = map.get(typeName);
		return list;
	}
	
}
