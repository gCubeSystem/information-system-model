/**
 * 
 */
package org.gcube.informationsystem.discovery.knowledge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.DiscoveredElementAction;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.types.TypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ModelKnowledgeValidator implements DiscoveredElementAction<Element>, NodeElaborator<Class<Element>> {

	public static Logger logger = LoggerFactory.getLogger(ModelKnowledgeValidator.class);
	
	protected RegistrationProvider rp;
	private Collection<Package> packages;
	private List<Class<Element>> baseTypes;
	
	public ModelKnowledgeValidator(RegistrationProvider rp) {
		this.rp = rp;
		this.packages = rp.getPackagesToRegister();
		this.baseTypes = new ArrayList<>();
		AccessType[] accessTypes = AccessType.getModelTypes();
		for(AccessType accessType : accessTypes) {
			this.baseTypes.add(accessType.getTypeClass());
		}
	}
	
	@Override
	public void analizeElement(Class<Element> e) throws Exception {
		String typeName = TypeMapper.getType(e);
		if(baseTypes.contains(e)) {
			logger.info("- Type {} is the root type", typeName);
			return;
		}
		
		if(packages.contains(e.getPackage())) {
			logger.info("- Type {} belongs to {} as expected", typeName, rp.getModelName());
		}else {
			logger.error("- Type {} DOES NOT belong to {} as expected. This is very strange and should not occurr.", typeName, rp.getModelName());
			throw new Exception("Type " + typeName + " DOES NOT belong to " + rp.getModelName() +" as expected. This is very strange and should not occurr.");
		}
	}

	@Override
	public void elaborate(Node<Class<Element>> node, int level) throws Exception {
		Class<Element> e = node.getNodeElement();
		
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < level; ++i) {
			stringBuffer.append(Node.INDENTATION);
		}
		
		String typeName = TypeMapper.getType(e);
		if(node.getTree().getRootNode().getNodeElement() == e) {
			logger.info("{}- Type {} is the root type", stringBuffer.toString(), typeName);
			return;
		}

		if(packages.contains(e.getPackage())) {
			logger.info("{}- Type {} belongs to {} as expected", stringBuffer.toString(), typeName, rp.getModelName());
		}else {
			logger.error("{}- Type {} DOES NOT belong to {} as expected. This is very strange and should not occurr.", stringBuffer.toString(), typeName, rp.getModelName());
			throw new Exception("Type " + typeName + " DOES NOT belong to " + rp.getModelName() +" as expected. This is very strange and should not occurr.");
		}
	}

}
