/**
 * 
 */
package org.gcube.informationsystem.discovery;

import org.gcube.informationsystem.base.reference.Element;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface DiscoveredElementAction<E extends Element> {

	public void analizeElement(Class<E> e) throws Exception;
	
}
