package org.gcube.informationsystem.discovery;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;

public class ISModelRegistrationProvider implements RegistrationProvider {

	public static final String IS_MODEL_NAME = "Information System (IS) Model";
	
	private static Set<Package> packages;
	
	static {
		packages = new HashSet<>();
		for(AccessType accessType : AccessType.getModelTypes()) {
			Class<Element> clz = accessType.getTypeClass();
			packages.add(clz.getPackage());
		}
	}
	
	@Override
	public Collection<Package> getPackagesToRegister() {
		return ISModelRegistrationProvider.packages;
	}

	@Override
	public String getModelName() {
		return IS_MODEL_NAME;
	}

}
