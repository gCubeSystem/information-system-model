package org.gcube.informationsystem.discovery;

import java.util.Collection;

/**
 * Any model requires to register the defined types.
 * To accomplish this, it must implement this interface 
 * returning the list of packages containing the interfaces
 * representing the model.
 * 
 * Any registration provider MUST have a constructor with no arguments
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public interface RegistrationProvider {
	
	/**
	 * The name of the Model
	 * @return
	 */
	public String getModelName();
	
	/**
	 * This method must return the list of packages to be registered 
	 * @return the list of packages to register
	 */
	public Collection<Package> getPackagesToRegister();
	
}
