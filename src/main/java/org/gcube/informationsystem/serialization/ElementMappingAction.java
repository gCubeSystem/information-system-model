/**
 * 
 */
package org.gcube.informationsystem.serialization;

import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.DiscoveredElementAction;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ElementMappingAction implements DiscoveredElementAction<Element> {

	@Override
	public void analizeElement(Class<Element> e) throws Exception {
		ElementMapper.registerSubtype(e);
	}

}
