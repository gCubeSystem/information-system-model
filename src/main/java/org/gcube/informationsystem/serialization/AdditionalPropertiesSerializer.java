package org.gcube.informationsystem.serialization;

import java.io.IOException;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.core.JsonGenerator;
import org.gcube.com.fasterxml.jackson.databind.SerializerProvider;
import org.gcube.com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.gcube.informationsystem.model.reference.properties.Property;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class AdditionalPropertiesSerializer extends StdSerializer<Object>{
	
	public AdditionalPropertiesSerializer() {
		super(null, true);
	}
	
	public AdditionalPropertiesSerializer(StdSerializer<Object> stdSerializer) {
		super(stdSerializer);
	}

	/**
	 * Generated Serial verison UID
	 */
	private static final long serialVersionUID = -3198778268947046277L;

	@Override
	public void serialize(Object obj, JsonGenerator generator, SerializerProvider provider) throws IOException {
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String,Object>) obj;
		for(String key : map.keySet()) {
			Object object = map.get(key);
			if(object instanceof Property) {
				generator.writeObjectField(key, (Property) object);
			}else {
				generator.writeObjectField(key, object);
			}
		}
	}
	
}
