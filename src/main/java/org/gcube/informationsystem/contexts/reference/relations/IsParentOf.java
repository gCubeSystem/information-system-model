/**
 * 
 */
package org.gcube.informationsystem.contexts.reference.relations;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.contexts.impl.relations.IsParentOfImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#IsParentOf
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as = IsParentOfImpl.class)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY})
@TypeMetadata(name = IsParentOf.NAME, description = "This type is the used to define parental relations between Context", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsParentOf extends RelationElement<Context,Context> {
	
	public static final String NAME = "IsParentOf"; //IsParentOf.class.getSimpleName();
	
	@JsonIgnoreProperties({Context.PARENT_PROPERTY, Context.CHILDREN_PROPERTY})
	@JsonGetter(value = SOURCE_PROPERTY)
	public Context getSource();
	
	@JsonIgnore
	public void setSource(Context source);
	
	@JsonIgnoreProperties({Context.PARENT_PROPERTY, Context.CHILDREN_PROPERTY})
	@JsonGetter(value = TARGET_PROPERTY)
	public Context getTarget();
	
	@JsonIgnore
	public void setTarget(Context target);
	
}
