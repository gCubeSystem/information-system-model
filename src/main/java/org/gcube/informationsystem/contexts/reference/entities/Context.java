/**
 * 
 */
package org.gcube.informationsystem.contexts.reference.entities;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.SchemaMixedElement;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.contexts.impl.entities.ContextImpl;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.reference.relations.Relation;
import org.gcube.informationsystem.serialization.AdditionalPropertiesSerializer;
import org.gcube.informationsystem.types.annotations.Deserialize;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Context
 * 
 * @author Luca Frosini (ISTI - CNR) 
 */
//@JsonDeserialize(as = ContextImpl.class)
@Deserialize(as = ContextImpl.class)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY})
@TypeMetadata(name = Context.NAME, description = "This type is the used to define a Context", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface Context extends EntityElement, SchemaMixedElement {

	public static final String NAME = "Context"; // Context.class.getSimpleName();

	public static final String NAME_PROPERTY = "name";
	public static final String PARENT_PROPERTY = "parent";
	public static final String CHILDREN_PROPERTY = "children";
	
	/** 
	 * Indicates the state of Context. 
	 * See #27706
	 * 
	 *  The resource-registry only knows:
	 *  - created
	 *  - active
	 *  - deleted
	 * 
	 * Other statuses can be set by a Manager.
	 * The resource-registry will allow the management of instances 
	 * from non-managers if and only if the state is active.
	 * 
	 * The resource-registry sets the states according to the following:
	 * 
	 * A newly created Context is set as created. Only a manager can use the instances collection;
	 * The active state can be set only by a Manager;
	 * When a context is deleted, the state is set to deleted, and the security space is removed, 
	 * but the Vertex representing the Context will be maintained see #19428.
	 */
	public static final String STATE = "state";
	
	@ISProperty(name = NAME_PROPERTY, mandatory = true, nullable = false)
	public String getName();

	public void setName(String name);
	
	@ISProperty(name = STATE, mandatory=true, nullable=false, regexpr = "[a-z]+")
	public String getState();
	
	public void setState(String state);

	@JsonGetter
	@JsonIgnoreProperties({ Relation.TARGET_PROPERTY })
	public IsParentOf getParent();

	@JsonIgnore
	public void setParent(UUID uuid);

	@JsonIgnore
	public void setParent(Context context);

	@JsonIgnore
	public void setParent(IsParentOf isParentOf);

	@JsonGetter
	@JsonIgnoreProperties({ Relation.SOURCE_PROPERTY })
	public List<IsParentOf> getChildren();

	public void addChild(UUID uuid);

	public void addChild(Context child);

	public void addChild(IsParentOf isParentOf);
	
	
	@JsonAnyGetter
	@JsonSerialize(using = AdditionalPropertiesSerializer.class)
	@Override
	public Map<String,Object> getAdditionalProperties();
	
	@Override
	public void setAdditionalProperties(Map<String,Object> additionalProperties);
	
	@Override
	public Object getAdditionalProperty(String key);
	
	@JsonAnySetter
	@Override
	public void setAdditionalProperty(String key, Object value);

}
