package org.gcube.informationsystem.contexts.reference;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum ContextState {

	CREATED("created"),
	ACTIVE("active"),
	SUSPENDED("suspended"),
	DELETED("deleted");
	
	private static final Map<String,ContextState> ENUM_MAP;
	
	static {
		ContextState[] array = ContextState.values();
        Map<String,ContextState> map = new ConcurrentHashMap<>(array.length);
        for (ContextState cs : array) {
            map.put(cs.getState().toLowerCase(),cs);
        }
        ENUM_MAP = Collections.unmodifiableMap(map);
    }
	
	protected String state;
	
	ContextState(String state) {
		this.state = state;
	}
	
	public String getState() {
		return state;
	}

	public static ContextState fromString(String state) {
        return ENUM_MAP.get(state.toLowerCase());
    }
	
}
