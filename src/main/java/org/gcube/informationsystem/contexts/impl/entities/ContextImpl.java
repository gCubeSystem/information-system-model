/**
 * 
 */
package org.gcube.informationsystem.contexts.impl.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.informationsystem.base.impl.entities.EntityElementImpl;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.contexts.impl.relations.IsParentOfImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;
import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.serialization.ElementMapper;
import org.gcube.informationsystem.utils.UUIDManager;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Context.NAME)
public class ContextImpl extends EntityElementImpl implements Context {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -5070590328223454087L;
	
	protected String name;
	
	protected String state;
	
	protected IsParentOf parent;
	protected List<IsParentOf> children;
	
	@JsonIgnore
	protected Map<String, Object> additionalProperties;
	
	@JsonIgnore
	/**
	 * Used to allow to have an additional property starting with '_' or '@'
	 */
	protected final Set<String> allowedAdditionalKeys;
	
	protected ContextImpl() {
		super();
		this.parent = null;
		this.children = new ArrayList<>();
		this.additionalProperties = new HashMap<>();
		this.allowedAdditionalKeys = new HashSet<>();
	}
	
	public ContextImpl(UUID uuid) {
		this(null, uuid);
	}
	
	public ContextImpl(String name) {
		this(name, null);
	}
	
	public ContextImpl(String name, UUID uuid) {
		this();
		this.name = name;
		if(uuid == null){
			uuid = UUIDManager.getInstance().generateValidUUID();
		}
		this.uuid = uuid;
		this.metadata = new MetadataImpl();
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getState() {
		return this.state;
	}

	@Override
	public void setState(String state) {
		this.state = state;
	}

	@Override
	public IsParentOf getParent() {
		return parent;
	}
	
	
	@Override
	public void setParent(UUID uuid) {
		Context parent = null;
		if(uuid!=null) {
			parent = new ContextImpl(uuid);
			parent.setMetadata(new MetadataImpl());
		}
		setParent(parent);
	}
	
	@Override
	public void setParent(Context context) {
		IsParentOf isParentOf = null;
		if(context!=null) {
			isParentOf = new IsParentOfImpl(context, this);
		}
		setParent(isParentOf);
	}
	
	@JsonSetter(value=PARENT_PROPERTY)
	protected void setParentFromJson(IsParentOf isParentOf) throws JsonProcessingException {
		if(isParentOf!=null) {
			Context parent = isParentOf.getSource();
			isParentOf.setTarget(this);
			((ContextImpl) parent).addChild(isParentOf);
		}
		setParent(isParentOf);
	}
	
	@Override
	public void setParent(IsParentOf isParentOf) {
		this.parent = isParentOf;
	}
	
	@Override
	public List<IsParentOf> getChildren() {
		return children;
	}

	@JsonSetter(value=CHILDREN_PROPERTY)
	protected void setChildrenFromJson(List<IsParentOf> children) throws JsonProcessingException {
		for(IsParentOf isParentOf : children){
			addChildFromJson(isParentOf);
		}
	}

	protected void addChildFromJson(IsParentOf isParentOf) throws JsonProcessingException {
		isParentOf.setSource(this);
		addChild(isParentOf);
	}
	
	@Override
	public void addChild(UUID uuid) {
		Context child = new ContextImpl(uuid);
		child.setMetadata(new MetadataImpl());
		addChild(child);
	}
	
	@Override
	public void addChild(Context child) {
		IsParentOf isParentOf = new IsParentOfImpl(this, child);
		this.addChild(isParentOf);
	}
	
	@Override
	public void addChild(IsParentOf isParentOf) {
		// ((ContextImpl) isParentOf.getTarget()).setParent(this);
		isParentOf.setSource(this);
		children.add(isParentOf);
	}
	
	@Override
	public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}

	@Override
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}

	@Override
	public Object getAdditionalProperty(String key) {
		return additionalProperties.get(key);
	}

	@Override
	public void setAdditionalProperty(String key, Object value) {
		
		if(!allowedAdditionalKeys.contains(key)){
			if(key.startsWith("_")) {
				return;
			}
			if(key.startsWith("@")) {
				return;
			}
		}
		
		/*
		Additional properties are not deserialized to the proper property type.
		The first attempt was to try to write a specific deserializer but it fails.	
		This fix the issue.
		*/
		try {
			if(value instanceof Map<?,?>) {
				@SuppressWarnings("unchecked")
				Map<String, Object> map = (Map<String,Object>) value;
				if(map.containsKey(Element.TYPE_PROPERTY)) {
					String reserialized = ElementMapper.getObjectMapper().writeValueAsString(map);
					Property property = ElementMapper.unmarshal(Property.class, reserialized);
					value = property;
				}
			}
		}catch (Throwable e) {
			e.getMessage();
			// Any type of error/exception must be catched
		}
		/* END of fix to properly deserialize Property types*/
		
		this.additionalProperties.put(key, value);
	}

}
