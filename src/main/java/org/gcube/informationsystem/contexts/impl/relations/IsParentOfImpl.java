/**
 * 
 */
package org.gcube.informationsystem.contexts.impl.relations;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.relations.RelationElementImpl;
import org.gcube.informationsystem.contexts.reference.entities.Context;
import org.gcube.informationsystem.contexts.reference.relations.IsParentOf;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=IsParentOf.NAME)
public final class IsParentOfImpl extends RelationElementImpl<Context, Context> implements IsParentOf {

	/**
	 * 
	 */
	private static final long serialVersionUID = 246200525751824393L;

	protected IsParentOfImpl(){
		super();
	}
	
	public IsParentOfImpl(Context source, Context target) {
		super(source, target);
	}

}
