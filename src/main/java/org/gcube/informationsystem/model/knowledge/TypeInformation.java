package org.gcube.informationsystem.model.knowledge;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.tree.NodeInformation;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface TypeInformation<T> extends NodeInformation<T> {
	
	public AccessType getAccessType(T t);
	
	public T getRoot(AccessType accessType);
	
}
