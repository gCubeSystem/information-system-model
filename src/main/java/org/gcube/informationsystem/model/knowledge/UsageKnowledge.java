package org.gcube.informationsystem.model.knowledge;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.gcube.informationsystem.base.reference.AccessType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class UsageKnowledge<U> {
	
	protected Map<String, List<U>> map;
	
	public UsageKnowledge(AccessType accessType){
		this.map = new LinkedHashMap<>();
	}
	
	public void add(String typeName, U u) {
		List<U> list = map.get(typeName);
		if(list==null) {
			list = new ArrayList<>();
			map.put(typeName, list);
		}
		list.add(u);
	}
	
	public List<U> getUsage(String typeName){
		List<U> list = map.get(typeName);
		return list;
	}
	
}
