package org.gcube.informationsystem.model.knowledge;

import java.util.Collection;

import org.gcube.informationsystem.base.reference.AccessType;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface TypesDiscoverer<T> {

	/**
	 * This function discover all available types of a certain
	 * AccessType. Allowed AccessTypes are only IS Model Types
	 * see {@link AccessType#getModelTypes()}
	 * @param accessType
	 * @return
	 */
	public Collection<T> discover(AccessType accessType);
	
}
