/**
 * 
 */
package org.gcube.informationsystem.model.reference.entities;

import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Entity
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonPropertyOrder({ Element.TYPE_PROPERTY, ModelElement.SUPERTYPES_PROPERTY, ModelElement.EXPECTED_TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY, ERElement.CONTEXTS_PROPERTY })
//@JsonDeserialize(as=EntityImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = Entity.NAME, description = "This is the base type for any Entity", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Entity extends EntityElement, ERElement {
	
	public static final String NAME = "Entity"; //Entity.class.getSimpleName();
	
}
