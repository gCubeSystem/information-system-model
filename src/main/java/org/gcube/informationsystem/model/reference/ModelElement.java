package org.gcube.informationsystem.model.reference;

import java.util.List;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.informationsystem.base.reference.Element;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonPropertyOrder({ Element.TYPE_PROPERTY, ModelElement.SUPERTYPES_PROPERTY, ModelElement.EXPECTED_TYPE_PROPERTY })
public interface ModelElement extends Element {

	public static final String SUPERTYPES_PROPERTY = "supertypes";
	
	/* 
	 * This is the key used by the deserializer to indicate the expected type 
	 * which instead has been deserialized using the best available
	 * supertype 
	 */
	public static final String EXPECTED_TYPE_PROPERTY = "expectedtype";
	
	@JsonInclude(Include.NON_EMPTY)
	@JsonGetter(value = SUPERTYPES_PROPERTY)
	public List<String> getSupertypes();
	
	@JsonInclude(Include.NON_EMPTY)
	@JsonGetter(value = EXPECTED_TYPE_PROPERTY)
	public String getExpectedtype();
	
}
