/**
 * 
 */
package org.gcube.informationsystem.model.reference.relations;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.base.reference.SchemaMixedElement;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.entities.Entity;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.serialization.AdditionalPropertiesSerializer;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Relation
 */
@Abstract
@JsonPropertyOrder({ Element.TYPE_PROPERTY, ModelElement.SUPERTYPES_PROPERTY, ModelElement.EXPECTED_TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY, ERElement.CONTEXTS_PROPERTY, Relation.PROPAGATION_CONSTRAINT_PROPERTY })
// @JsonDeserialize(as=RelationImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = Relation.NAME, description = "This is the base type for any Relation", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Relation<S extends Resource, T extends Entity> extends RelationElement<S,T>, SchemaMixedElement, ERElement {
	
	public static final String NAME = "Relation"; //Relation.class.getSimpleName();
	
	public static final String PROPAGATION_CONSTRAINT_PROPERTY = "propagationConstraint";
	
	@JsonInclude(Include.NON_NULL)
	@JsonIgnoreProperties({Resource.CONSISTS_OF_PROPERTY, Resource.IS_RELATED_TO_PROPERTY})
	@JsonGetter(value = SOURCE_PROPERTY)
	@Override
	public S getSource();
	
	@JsonIgnore
	@Override
	public void setSource(S source);
	
	@JsonGetter(value = TARGET_PROPERTY)
	@Override
	public T getTarget();
	
	@JsonIgnore
	@Override
	public void setTarget(T target);
	
	@JsonGetter(value = PROPAGATION_CONSTRAINT_PROPERTY)
	@ISProperty(name = PROPAGATION_CONSTRAINT_PROPERTY)
	public PropagationConstraint getPropagationConstraint();
	
	@JsonAnyGetter
	@JsonSerialize(using = AdditionalPropertiesSerializer.class)
	@Override
	public Map<String,Object> getAdditionalProperties();
	
	@Override
	public void setAdditionalProperties(Map<String,Object> additionalProperties);
	
	@Override
	public Object getAdditionalProperty(String key);
	
	@JsonAnySetter
	@Override
	public void setAdditionalProperty(String key, Object value);
	
}
