/**
 * 
 */
package org.gcube.informationsystem.model.reference.properties;

import java.util.Date;

import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.annotation.JsonInclude;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.impl.properties.EventImpl;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=EventImpl.class)
@TypeMetadata(name = Event.NAME, description = "This type provides information regarding an event using the Five Ws (checklist) https://en.wikipedia.org/wiki/Five_Ws", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@JsonInclude(JsonInclude.Include.NON_NULL)
public interface Event extends Property, Comparable<Event> {

	public static final String NAME = "Event"; // Event.class.getSimpleName();

	public static final String WHAT_PROPERTY = "what";
	public static final String WHEN_PROPERTY = "when";
	public static final String WHO_PROPERTY = "who";
	public static final String WHERE_PROPERTY = "where";
	public static final String WHY_PROPERTY = "why";
	public static final String HOW_PROPERTY = "how";

	@ISProperty(name = WHAT_PROPERTY, description = "WHAT happened.", readonly = true, mandatory = true, nullable = false)
	public String getWhat();
	
	public void setWhat(String what);

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Element.DATETIME_PATTERN)
	@ISProperty(name = WHEN_PROPERTY, description = "WHEN the event occured. It is represented in the format " + Element.DATETIME_PATTERN + ".", readonly = true, mandatory = true, nullable = false)
	public Date getWhen();
	
	public void setWhen(Date when);
	
	@ISProperty(name = WHO_PROPERTY, description = "WHO triggered the event.", readonly = true, mandatory = false, nullable = false)
	public String getWho();
	
	public void setWho(String who);

	@ISProperty(name = WHERE_PROPERTY, description = "The location (can be virtual) WHERE the event occurred.", readonly = true, mandatory = false, nullable = false)
	public String getWhere();
	
	public void setWhere(String where);

	@ISProperty(name = WHY_PROPERTY, description = "The reason WHY the event occurred.", readonly = true, mandatory = false, nullable = false)
	public String getWhy();
	
	public void setWhy(String why);
	
	@ISProperty(name = HOW_PROPERTY, description = "How the event occurred.", readonly = true, mandatory = false, nullable = false)
	public String getHow();
	
	public void setHow(String how);
	
}