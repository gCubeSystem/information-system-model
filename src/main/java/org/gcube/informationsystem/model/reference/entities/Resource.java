/**
 * 
 */
package org.gcube.informationsystem.model.reference.entities;

import java.util.List;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.IdentifiableElement;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.annotations.RelatedResourcesEntry;
import org.gcube.informationsystem.types.annotations.ResourceSchema;
import org.gcube.informationsystem.types.annotations.ResourceSchemaEntry;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Resource
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@Abstract
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, ModelElement.SUPERTYPES_PROPERTY, ModelElement.EXPECTED_TYPE_PROPERTY, IdentifiableElement.ID_PROPERTY, IdentifiableElement.METADATA_PROPERTY, ERElement.CONTEXTS_PROPERTY, Resource.CONSISTS_OF_PROPERTY, Resource.IS_RELATED_TO_PROPERTY })
// @JsonDeserialize(as=ResourceImpl.class) Do not uncomment to manage subclasses
@ResourceSchema(
	facets={
		@ResourceSchemaEntry(relation=ConsistsOf.class, facet=Facet.class, min=1, description="Any Resource consists of one or more Facets which describes the different aspects of the resource."),
	},
	resources= {
		@RelatedResourcesEntry(source=Resource.class, relation=IsRelatedTo.class, target=Resource.class, description="Any Resource can be related to any other resource.")
	}
)
@TypeMetadata(name = Resource.NAME, description = "This is the base type for any Resource", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Resource extends Entity {
	
	public static final String NAME = "Resource"; //Resource.class.getSimpleName();
	
	public static final String CONSISTS_OF_PROPERTY = "consistsOf";
	public static final String IS_RELATED_TO_PROPERTY = "isRelatedTo";
	
	// @JsonManagedReference
	public List<ConsistsOf<? extends Resource, ? extends Facet>> getConsistsOf();
	
	@JsonIgnore
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> List<C> getConsistsOf(Class<C> clz);
	
	@JsonIgnore
	public <F extends Facet, C extends ConsistsOf<? extends Resource, F>> List<C> getConsistsOf(Class<C> clz, Class<F> target);
	
	
	// @JsonManagedReference
	public List<IsRelatedTo<? extends Resource, ? extends Resource>> getIsRelatedTo();
	
	@JsonIgnore
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> List<I> getIsRelatedTo(Class<I> clz);
	
	
	@JsonIgnore
	public List<? extends Facet> getFacets();
	
	@JsonIgnore
	public <F extends Facet> List<F> getFacets(Class<F> clz);
	
	@JsonIgnore
	public <F extends Facet, C extends ConsistsOf<? extends Resource, F>> List<F> getFacets(Class<C> clz, Class<F> target);
	
	
	public void addFacet(UUID uuid);
	
	public <F extends Facet> void addFacet(F facet);
	
	public <C extends ConsistsOf<? extends Resource, ? extends Facet>> void addFacet(C relation);
	
	
	/*
	public void attachResource(UUID uuid);
	
	public <R extends Resource> void attachResource(R resource);
	*/
	
	public <I extends IsRelatedTo<? extends Resource, ? extends Resource>> void attachResource(I relation);
	
}
