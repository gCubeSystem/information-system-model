package org.gcube.informationsystem.model.reference.entities;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.gcube.informationsystem.base.reference.SchemaMixedElement;
import org.gcube.informationsystem.serialization.AdditionalPropertiesSerializer;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Facet
 */
@Abstract
// @JsonDeserialize(as=FacetImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = "Facet", description = "This is the base type for any Facet", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Facet extends Entity, SchemaMixedElement {
	
	public static final String NAME = "Facet"; //Facet.class.getSimpleName();
	
	@JsonAnyGetter
	@JsonSerialize(using = AdditionalPropertiesSerializer.class)
	@Override
	public Map<String,Object> getAdditionalProperties();
	
	@Override
	public void setAdditionalProperties(Map<String,Object> additionalProperties);
	
	@Override
	public Object getAdditionalProperty(String key);
	
	@JsonAnySetter
	@Override
	public void setAdditionalProperty(String key, Object value);
	
}
