/**
 * 
 */
package org.gcube.informationsystem.model.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Changelog;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * At any time entities and relations can be added or removed to/from a context or deleted. 
 * The PropagationConstraint property contained in each relation is a property  
 * which indicates the behaviour to be held on a target entity 
 * when an event related to a context occurs in the source resource 
 * or directly to the relation.
 * 
 * The default values of propagation constraints are:
 * - IsRelatedTo: delete=keep, remove=keep, add=unpropagate;
 * - ConsistsOf : delete=cascade, remove=cascade, add=propagate.
 * 
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#Propagation_Constraint
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=PropagationConstraintImpl.class)
@TypeMetadata(name = PropagationConstraint.NAME, description = "This type provides propagation constraint for Relation", version = "1.1.0")
@Changelog({
	@Change(version = "1.1.0", description = "Added 'delete' propagation constraint"),
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
})
@Final
public interface PropagationConstraint extends Property {

	public static final String NAME = "PropagationConstraint"; // PropagationConstraint.class.getSimpleName();
	
	public static final String ADD_PROPERTY = "add";
	public static final String DELETE_PROPERTY = "delete";
	public static final String REMOVE_PROPERTY = "remove";

	public enum AddConstraint {
		
		/**
		 * When an 'add' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'add' action is performed on 
		 * the relation and its target Entity.
		 */
		propagate,
		
		/**
		 * When an 'add' action is performed on the source Entity of the relation, 
		 * is performed on source relation only. Trying to perform an 'add' action on 
		 * the relation has no effects.
		 */
		unpropagate
			
	}
	
	public enum DeleteConstraint {
		
		/**
		 * When a 'delete' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'delete' action apart on
		 * the relation is performed to the target entity if it has no other incoming relations
		 */
		cascadeWhenOrphan,
		
		/**
		 * When a 'delete' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'delete' action is performed on 
		 * the relation and its target entity;
		 */
		cascade, 
		
		/**
		 * When a 'delete' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'delete' action is performed on 
		 * relation but never to the target entity.
		 */
		keep
		
	}

	
	public enum RemoveConstraint {
		
		/**
		 * When a 'remove' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'remove' action apart on
		 * the relation is performed to the target entity if it has no other incoming relations
		 */
		cascadeWhenOrphan,
		
		/**
		 * When a 'remove' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'remove' action is performed on 
		 * the relation and its target entity;
		 */
		cascade, 
		
		/**
		 * When a 'remove' action is performed on the source Entity of the relation, 
		 * or directly on the relation, then the same 'remove' action is performed on 
		 * relation but never to the target entity.
		 */
		keep
		
	}

	@ISProperty(name=ADD_PROPERTY, description = "It indicates the behaviour to implement for the target Entity when an 'add' action is performed on the source Resource. Add action is the operation of adding an instance to a context.", mandatory = true, nullable = false)
	public AddConstraint getAddConstraint();
	
	public void setAddConstraint(AddConstraint addConstraint);
	
	
	@ISProperty(name=DELETE_PROPERTY, description = "It indicates the behaviour to implement for the target Entity when a 'delete' action is performed on the source Resource. Delet action  is the operation of deleting an instance (it has an impact on all contexts).", mandatory = true, nullable = false)
	public DeleteConstraint getDeleteConstraint();
	
	public void setDeleteConstraint(DeleteConstraint deleteConstraint);
	
	
	@ISProperty(name=REMOVE_PROPERTY, description = "It indicates the behaviour to implement for the target Entity when a 'remove' action is performed on the source Resource. Remove actions is the operation of removing an instance from a context.", mandatory = true, nullable = false)
	public RemoveConstraint getRemoveConstraint();
	
	public void setRemoveConstraint(RemoveConstraint removeConstraint);
	
}
