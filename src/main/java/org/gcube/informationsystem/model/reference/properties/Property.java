/**
 * 
 */
package org.gcube.informationsystem.model.reference.properties;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonAnyGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonAnySetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.base.reference.SchemaMixedElement;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.model.impl.properties.PropertyImpl;
import org.gcube.informationsystem.model.reference.ModelElement;
import org.gcube.informationsystem.serialization.AdditionalPropertiesSerializer;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * Root Class for Property types. 
 * It creates a base common type, which is useful for management purpose.
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
// @JsonIgnoreProperties(ignoreUnknown=true)
@JsonPropertyOrder({ Element.TYPE_PROPERTY, ModelElement.SUPERTYPES_PROPERTY, ModelElement.EXPECTED_TYPE_PROPERTY })
@JsonDeserialize(as=PropertyImpl.class)
@TypeMetadata(name = Property.NAME, description = "This is the base type for any Property", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Property extends PropertyElement, SchemaMixedElement, ModelElement {
	
	public static final String NAME = "Property"; //Property.class.getSimpleName();
	
	@JsonAnyGetter
	@JsonSerialize(using = AdditionalPropertiesSerializer.class)
	@Override
	public Map<String,Object> getAdditionalProperties();
	
	@Override
	public void setAdditionalProperties(Map<String,Object> additionalProperties);
	
	@Override
	public Object getAdditionalProperty(String key);
	
	@JsonAnySetter
	@Override
	public void setAdditionalProperty(String key, Object value);
	
}
