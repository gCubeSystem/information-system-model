package org.gcube.informationsystem.model.reference.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.impl.properties.EncryptedImpl;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=EncryptedImpl.class)
@TypeMetadata(name = Encrypted.NAME, description = "This type is used to properly manage values must be stored safely (e.g. encrypted) in the IS.", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface Encrypted extends Property {
	
	public static final String NAME = "Encrypted"; // Encrypted.class.getSimpleName();
	
	public static final String VALUE = "value";
	
	@ISProperty(name=VALUE, readonly=false, mandatory=true, nullable=false, description = "The value to store safely in the IS")
	@JsonGetter(value=VALUE)
	public String getValue();
	
	@JsonSetter(value=VALUE)
	public void setValue(String value);
	
}
