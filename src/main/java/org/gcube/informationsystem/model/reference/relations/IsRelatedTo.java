/**
 * 
 */
package org.gcube.informationsystem.model.reference.relations;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.annotations.Abstract;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 * 
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#IsRelatedTo
 */
@Abstract
// @JsonDeserialize(as=IsRelatedToImpl.class) Do not uncomment to manage subclasses
@TypeMetadata(name = IsRelatedTo.NAME, description = "This is the base type for any IsRelatedTo relation", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface IsRelatedTo<S extends Resource, T extends Resource>
		extends Relation<S, T> {
	
	public static final String NAME = "IsRelatedTo"; //IsRelatedTo.class.getSimpleName();
	
}
