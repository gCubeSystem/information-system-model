/**
 * 
 */
package org.gcube.informationsystem.model.reference.properties;

import java.util.Date;

import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.types.annotations.Final;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=MetadataImpl.class)
@TypeMetadata(name = Metadata.NAME, description = "This type provides metadata per every IdentifiableElement", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
@Final
public interface Metadata extends Property {

	public static final String NAME = "Metadata"; // Metadata.class.getSimpleName();

	/**
	 * Used to set Creator when the user is not known
	 */
	public static final String UNKNOWN_USER = "UNKNOWN_USER";
	public static final String HIDDEN_FOR_PRIVACY_USER = "HIDDEN_FOR_PRIVACY";

	public static final String CREATED_BY_PROPERTY = "createdBy";
	public static final String CREATION_TIME_PROPERTY = "creationTime";
	public static final String LAST_UPDATE_BY_PROPERTY = "lastUpdateBy";
	public static final String LAST_UPDATE_TIME_PROPERTY = "lastUpdateTime";

	@ISProperty(name = CREATED_BY_PROPERTY, description = "The user that created the Entity or the Relation. It is initialized at Creation Time.", readonly = true, mandatory = true, nullable = false)
	public String getCreatedBy();

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Element.DATETIME_PATTERN)
	@ISProperty(name = CREATION_TIME_PROPERTY, description = "Creation time. It is represented in the format " + Element.DATETIME_PATTERN + ".", readonly = true, mandatory = true, nullable = false)
	public Date getCreationTime();

	@ISProperty(name = LAST_UPDATE_BY_PROPERTY, description = "The user that made the last update to the Entity or the Relation. At Creation Time, it assumes the same value of " + CREATED_BY_PROPERTY + ".", mandatory = true, nullable = false)
	public String getLastUpdateBy();

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Element.DATETIME_PATTERN)
	@ISProperty(name = LAST_UPDATE_TIME_PROPERTY, description = "Last Update time. At creation time it assumes the same value of " + CREATION_TIME_PROPERTY + ". It is represented in the format " + Element.DATETIME_PATTERN, mandatory = true, nullable = false)
	public Date getLastUpdateTime();
	
}