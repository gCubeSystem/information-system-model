/**
 * 
 */
package org.gcube.informationsystem.model.reference.relations;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.impl.relations.ConsistsOfImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * https://wiki.gcube-system.org/gcube/Facet_Based_Resource_Model#ConsistsOf
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=ConsistsOfImpl.class)
@TypeMetadata(name = ConsistsOf.NAME, description = "This is the base type for any ConsistsOf relation", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface ConsistsOf<S extends Resource, T extends Facet>
		extends Relation<S, T> {
	
	public static final String NAME = "ConsistsOf"; //ConsistsOf.class.getSimpleName();
	
}
