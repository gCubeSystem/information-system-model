/**
 * 
 */
package org.gcube.informationsystem.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonFormat;
import org.gcube.com.fasterxml.jackson.annotation.JsonProperty;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=PropagationConstraint.NAME)
public final class PropagationConstraintImpl extends PropertyImpl implements PropagationConstraint {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4708881022038107688L;

	@JsonFormat(shape=JsonFormat.Shape.STRING)
	@JsonProperty(value=ADD_PROPERTY)
	protected AddConstraint addConstraint;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING)
	@JsonProperty(value=DELETE_PROPERTY)
	protected DeleteConstraint deleteConstraint;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING)
	@JsonProperty(value=REMOVE_PROPERTY)
	protected RemoveConstraint removeConstraint;
	
	public PropagationConstraintImpl(){
		super();
	}

	@Override
	public AddConstraint getAddConstraint() {
		return this.addConstraint;
	}

	@Override
	public void setAddConstraint(AddConstraint addConstraint) {
		this.addConstraint = addConstraint;
	}
	
	@Override
	public DeleteConstraint getDeleteConstraint() {
		return this.deleteConstraint;
	}

	@Override
	public void setDeleteConstraint(DeleteConstraint deleteConstraint) {
		this.deleteConstraint = deleteConstraint;
	}
	
	@Override
	public RemoveConstraint getRemoveConstraint() {
		return this.removeConstraint;
	}

	@Override
	public void setRemoveConstraint(RemoveConstraint removeConstraint) {
		this.removeConstraint = removeConstraint;
	}

}
