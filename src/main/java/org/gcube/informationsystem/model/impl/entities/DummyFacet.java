/**
 * 
 */
package org.gcube.informationsystem.model.impl.entities;

import java.util.UUID;

import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@TypeMetadata(name = Facet.NAME, description = "This is a dummy type for Facet", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public class DummyFacet extends FacetImpl implements Facet {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -1527529288324120341L;
	
	public DummyFacet(UUID uuid) {
		super();
		this.uuid = uuid;
		this.metadata = new MetadataImpl();
	}

	public DummyFacet(){
		super();
	}
	
}
