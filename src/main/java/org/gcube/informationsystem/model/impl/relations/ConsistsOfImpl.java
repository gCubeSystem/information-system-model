/**
 * 
 */
package org.gcube.informationsystem.model.impl.relations;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=ConsistsOf.NAME)
public class ConsistsOfImpl<S extends Resource, T extends Facet> extends
		RelationImpl<S, T> implements ConsistsOf<S, T> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4903629726765659155L;

	protected ConsistsOfImpl(){
		super();
	}
	
	public ConsistsOfImpl(S source, T target) {
		this(source, target, null);
	}
	
	public ConsistsOfImpl(S source, T target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
		if(this.propagationConstraint==null) {
			this.propagationConstraint = new PropagationConstraintImpl();
			this.propagationConstraint.setAddConstraint(AddConstraint.propagate);
			this.propagationConstraint.setRemoveConstraint(RemoveConstraint.cascade);
			this.propagationConstraint.setDeleteConstraint(DeleteConstraint.cascade);
		}
	}
	
}
