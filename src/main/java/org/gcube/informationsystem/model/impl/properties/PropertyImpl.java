/**
 * 
 */
package org.gcube.informationsystem.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.properties.PropertyElementImpl;
import org.gcube.informationsystem.model.reference.properties.Property;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@JsonTypeName(value=Property.NAME)
public class PropertyImpl extends PropertyElementImpl implements Property {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1396998430221747445L;

	public PropertyImpl() {
		super();
	}
	
}
