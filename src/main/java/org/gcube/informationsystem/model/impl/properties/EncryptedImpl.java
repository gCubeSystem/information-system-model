/**
 * 
 */
package org.gcube.informationsystem.model.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.Encrypted;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value = Encrypted.NAME)
public class EncryptedImpl extends PropertyImpl implements Encrypted {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 7823031209294161865L;
	
	@JsonIgnore
	protected String value;
	
	public EncryptedImpl() {
		super();
	}
	
	@Override
	public String getValue() {
		return value;
	}
	
	@Override
	public void setValue(String value) {
		this.value = value;
	}
	
}
