/**
 * 
 */
package org.gcube.informationsystem.model.impl.relations;

import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@TypeMetadata(name = IsRelatedTo.NAME, description = "This is a dummy type for IsRelatedTo", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public class DummyIsRelatedTo<S extends Resource, T extends Resource>
		extends IsRelatedToImpl<S, T> implements IsRelatedTo<S, T>{

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 3522865762953929379L;

	public DummyIsRelatedTo(){
		super();
	}

	public DummyIsRelatedTo(S source, T target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
	}
}
