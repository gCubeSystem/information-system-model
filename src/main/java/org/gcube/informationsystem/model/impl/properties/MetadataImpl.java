/**
 * 
 */
package org.gcube.informationsystem.model.impl.properties;

import java.util.Date;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.Metadata;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Metadata.NAME)
public final class MetadataImpl extends PropertyImpl implements Metadata {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5102553511155113169L;

	protected String createdBy;
	protected Date creationTime;
	protected String lastUpdateBy;
	protected Date lastUpdateTime;

	public MetadataImpl() {
		super();
	}

	@Override
	public String getCreatedBy() {
		return createdBy;
	}

	@Override
	public Date getCreationTime() {
		return creationTime;
	}
	
	@Override
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	@Override
	public Date getLastUpdateTime() {
		return lastUpdateTime;
	}

}
