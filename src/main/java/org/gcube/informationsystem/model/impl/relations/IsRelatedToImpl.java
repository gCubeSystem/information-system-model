/**
 * 
 */
package org.gcube.informationsystem.model.impl.relations;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.properties.PropagationConstraintImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.AddConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.DeleteConstraint;
import org.gcube.informationsystem.model.reference.properties.PropagationConstraint.RemoveConstraint;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=IsRelatedTo.NAME)
public abstract class IsRelatedToImpl<S extends Resource, T extends Resource> extends
		RelationImpl<S, T> implements IsRelatedTo<S, T> {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5119376559964300102L;

	protected IsRelatedToImpl(){
		super();
	}

	public IsRelatedToImpl(S source, T target) {
		this(source, target, null);
	}
	
	public IsRelatedToImpl(S source, T target,
			PropagationConstraint propagationConstraint) {
		super(source, target, propagationConstraint);
		if(this.propagationConstraint==null) {
			this.propagationConstraint = new PropagationConstraintImpl();
			this.propagationConstraint.setAddConstraint(AddConstraint.unpropagate);
			this.propagationConstraint.setRemoveConstraint(RemoveConstraint.keep);
			this.propagationConstraint.setDeleteConstraint(DeleteConstraint.keep);
		}
	}
	
}
