/**
 * 
 */
package org.gcube.informationsystem.model.impl.entities;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.entities.EntityElementImpl;
import org.gcube.informationsystem.model.reference.ERElement;
import org.gcube.informationsystem.model.reference.entities.Entity;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Entity.NAME)
public abstract class EntityImpl extends EntityElementImpl implements Entity {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -4488771434017342703L;
	
	protected List<String> supertypes;
	protected String expectedtype;
	protected Map<UUID,String> contexts;
	
	protected EntityImpl(){
		super();
		this.metadata = null;
	}

	@Override
	public List<String> getSupertypes() {
		return this.supertypes;
	}
	
	@Override
	public String getExpectedtype() {
		return this.expectedtype;
	}
	
	public Map<UUID,String> getContexts(){
		return this.contexts;
	}
	
	@JsonSetter(value = ERElement.CONTEXTS_PROPERTY)
	protected void setContexts(Map<UUID,String> contexts) {
		this.contexts = contexts;
	}
}
