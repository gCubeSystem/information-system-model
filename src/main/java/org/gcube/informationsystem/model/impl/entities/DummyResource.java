/**
 * 
 */
package org.gcube.informationsystem.model.impl.entities;

import java.util.UUID;

import org.gcube.informationsystem.model.impl.properties.MetadataImpl;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@SuppressWarnings("unused")
@TypeMetadata(name = Resource.NAME, description = "This is a dummy type for Resource", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public class DummyResource extends ResourceImpl implements Resource {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -8522083786087905215L;

	public DummyResource(UUID uuid){
		super();
		this.uuid = uuid;
		this.metadata = new MetadataImpl();
	}
	
	public DummyResource(){
		super();
	}
	
}
