/**
 * 
 */
package org.gcube.informationsystem.model.impl.properties;

import java.util.Date;
import java.util.Objects;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.reference.properties.Event;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=Event.NAME)
public class EventImpl extends PropertyImpl implements Event {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 5102553511155113169L;

	protected String what;
	protected Date when;
	protected String who;
	protected String where;
	protected String why;
	protected String how;
	
	public EventImpl() {
		super();
	}

	@Override
	public String getWhat() {
		return what;
	}
	
	@Override
	public void setWhat(String what) {
		this.what = what;
	}
	
	@Override
	public Date getWhen() {
		return when;
	}
	
	@Override
	public void setWhen(Date when) {
		this.when = when;
	}

	@Override
	public String getWho() {
		return who;
	}
	
	@Override
	public void setWho(String who) {
		this.who = who;
	}

	@Override
	public String getWhere() {
		return where;
	}
	
	@Override
	public void setWhere(String where) {
		this.where = where;
	}

	@Override
	public String getWhy() {
		return why;
	}

	@Override
	public void setWhy(String why) {
		this.why = why;
	}

	@Override
	public String getHow() {
		return how;
	}

	@Override
	public void setHow(String how) {
		this.how = how;
	}
	
	private int compareStrings(String sThis, String sOther) {
		return 0;
	}

	@Override
	public int compareTo(Event o) {
		if(o == null) {
            return 1;
		}
		
		int compare = 0;
		if(this.when==null) {
			if(o.getWhen()==null) {
				compare = 0;
			}else {
				compare = 1;
			}
		}else {
			if(o.getWhen()==null) {
				compare = -1;
			}else {
				compare = this.when.compareTo(o.getWhen());
			}
		}
		if(compare!=0) {
			return compare;
		}
		
		compare = compareStrings(this.what,o.getWhat());
		if(compare!=0) {
			return compare;
		}
		
		compare = compareStrings(this.who,o.getWho());
		if(compare!=0) {
			return compare;
		}
		
		compare = compareStrings(where,o.getWhere());
		if(compare!=0) {
			return compare;
		}
		
		compare = compareStrings(why,o.getWhy());
		if(compare!=0) {
			return compare;
		}
		
		compare = compareStrings(how,o.getHow());
		return compare;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(how, what, when, where, who, why);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventImpl other = (EventImpl) obj;
		return Objects.equals(how, other.how) && Objects.equals(what, other.what) && Objects.equals(when, other.when)
				&& Objects.equals(where, other.where) && Objects.equals(who, other.who)
				&& Objects.equals(why, other.why);
	}
}
