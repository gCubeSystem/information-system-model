package org.gcube.informationsystem.queries.templates.reference.properties;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.base.reference.Attribute;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.queries.templates.impl.properties.TemplateVariableImpl;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.Changelog;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=TemplateVariableImpl.class)
@TypeMetadata(name = TemplateVariable.NAME, description = "This is the class used to define the a TemplateVariable", version = TemplateVariable.VERSION)
@Changelog ({
	@Change(version = TemplateVariable.VERSION, description = "The type comply with {@link Attribute} which add 'defaultValue'"),
	@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
})
public interface TemplateVariable extends PropertyElement, Attribute {

	public static final String NAME = "TemplateVariable"; // TemplateVariable.class.getSimpleName();
	public static final String VERSION = "1.1.0";
	
	@ISProperty(name = Attribute.NAME_PROPERTY, description = "The name of the Query Template Variable.", readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	@ISProperty(name = Attribute.DESCRIPTION_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	@ISProperty(name = Attribute.MAX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Integer getMax();
	
	@ISProperty(name = Attribute.MIN_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public Integer getMin();
	
	@ISProperty(name = Attribute.REGEX_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getRegexp();
	
	@ISProperty(name = Attribute.PROPERTY_TYPE_PROPERTY, readonly = false, mandatory = true, nullable = false)
	public String getPropertyType();
	
	@ISProperty(name = Attribute.DEFAULT_VALUE_PROPERTY, readonly = false, mandatory = false, nullable = true)
	public Object getDefaultValue();
	
}
