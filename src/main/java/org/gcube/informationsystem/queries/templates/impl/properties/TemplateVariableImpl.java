/**
 * 
 */
package org.gcube.informationsystem.queries.templates.impl.properties;

import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.base.impl.properties.PropertyElementImpl;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.types.PropertyTypeName;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.AttributeUtility;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=TemplateVariable.NAME)
public class TemplateVariableImpl extends PropertyElementImpl implements TemplateVariable {

	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 2628113641100130640L;
	
	protected String name;
	protected String description;
	private Integer max= null;
	private Integer min= null;
	private String regexp= null;
	private PropertyTypeName propertyTypeName = null; 
	protected Object defaultValue;
	
	public TemplateVariableImpl() {
		super();
		/* TODO consider if we want to have a default type
		 * setPropertyType(PropertyTypeName.BaseType.STRING.name());
		 */
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public Integer getMax() {
		return max;
	}
	
	@Override
	public void setMax(Integer max) {
		this.max = max;
	}
	
	@Override
	public Integer getMin() {
		return min;
	}
	
	@Override
	public void setMin(Integer min) {
		this.min = min;
	}
	
	@Override
	public String getRegexp() {
		return regexp;
	}
	
	@Override
	public void setRegexp(String regexp) {
		AttributeUtility.checkRegex(regexp, null);
		this.regexp = regexp;
	}
	
	@Override
	public String getPropertyType() {
		return propertyTypeName.toString();
	}

	@JsonSetter(value = PropertyDefinition.PROPERTY_TYPE_PROPERTY)
	public void setPropertyType(String type) {
		this.propertyTypeName = new PropertyTypeName(type);
	}
	
	@JsonIgnore
	public PropertyTypeName getPropertyTypeName() {
		return propertyTypeName;
	}
	
	@Override
	public Object getDefaultValue() {
		return defaultValue;
	}

	@Override
	public void setDefaultValue(Object defaultValue) {
		this.defaultValue = defaultValue;
	}
	
	@Override
	public String toString() {
		return "TemplateVariable ["
				+ "name=" + name 
				+ ", description=" + description
				+ ", max=" + max 
				+ ", min=" + min
				+ ", regexpr=" + regexp 
				+ ", type=" + (propertyTypeName==null ?  "null" : propertyTypeName.toString())
				+ ", defaultValue=" + (defaultValue == null ? "null" : defaultValue.toString())
				+ "]";
	}
	
}
