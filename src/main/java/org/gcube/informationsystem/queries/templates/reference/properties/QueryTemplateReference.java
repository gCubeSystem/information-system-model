package org.gcube.informationsystem.queries.templates.reference.properties;

import java.util.Map;

import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.informationsystem.model.reference.properties.Property;
import org.gcube.informationsystem.queries.templates.impl.properties.QueryTemplateReferenceImpl;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonDeserialize(as=QueryTemplateReferenceImpl.class)
@TypeMetadata(
	name = QueryTemplateReference.NAME, 
	description = "This property references a query template to invoke with the specified variables to provide.",
	version = Version.MINIMAL_VERSION_STRING
)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface QueryTemplateReference extends Property {
	
	public static final String NAME = "QueryTemplateReference"; // QueryTemplateReference.class.getSimpleName();
	
	public static final String NAME_PROPERTY = "name";
	public static final String VARIABLES_PROPERTY = "variables";

	@ISProperty(name = NAME_PROPERTY, description = "The name of the query template to refer", mandatory = true, nullable = false)
	public String getName();
	
	public void setName(String name);

	@ISProperty(name = VARIABLES_PROPERTY, description = "The query template variables values", mandatory = false, nullable = true)
	public Map<String,Object> getVariables();
	
	public void setVariables(Map<String,Object> variables);

	public void addVariable(String name, Object value);

}
