package org.gcube.informationsystem.queries.templates.impl.entities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.ObjectMapper;
import org.gcube.com.fasterxml.jackson.databind.node.ArrayNode;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.impl.entities.EntityElementImpl;
import org.gcube.informationsystem.queries.templates.reference.entities.QueryTemplate;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.serialization.ElementMapper;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonTypeName(value=QueryTemplate.NAME)
public class QueryTemplateImpl extends EntityElementImpl implements QueryTemplate {

	/**
	 * Generated Serial version UID
	 */
	private static final long serialVersionUID = -1096809036997782113L;
	
	protected String name;
	protected String description;
	
	protected ObjectMapper objectMapper;
	protected JsonNode template;
	
	
	protected Map<String, TemplateVariable> templateVariables;
	protected JsonNode params;
	
	public QueryTemplateImpl() {
		this.templateVariables = new HashMap<>();
		this.objectMapper = new ObjectMapper();
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String getTemplateAsString() throws JsonProcessingException {
		return objectMapper.writeValueAsString(template);
	}

	@Override
	public void setTemplate(String template) throws JsonProcessingException, IOException {
		this.template = objectMapper.readTree(template);
	}
	
	@Override
	public JsonNode getTemplate() {
		return template;
	}
	
	@Override
	public void setTemplate(JsonNode template) {
		this.template = template;
	}

	@Override
	public Map<String, TemplateVariable> getTemplateVariables() {
		return templateVariables;
	}
	
	@Override
	public void addTemplateVariable(TemplateVariable templateVariable) {
		String name = templateVariable.getName();
		this.templateVariables.put(name, templateVariable);
	}

	@Override
	public ObjectNode getParamsFromDefaultValues() {
		ObjectNode objectNode = objectMapper.createObjectNode();
		
		for(TemplateVariable tv : templateVariables.values()) {
			Object defaultValue = tv.getDefaultValue();
			JsonNode jsonNode = ElementMapper.getObjectMapper().valueToTree(defaultValue);
			objectNode.set(tv.getName(), jsonNode);
			
//			if(defaultValue instanceof Integer) {
//				objectNode.put(tv.getName(), (Integer) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Boolean) {
//				objectNode.put(tv.getName(), (Boolean) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Integer) {
//				objectNode.put(tv.getName(), (Integer) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Short) {
//				objectNode.put(tv.getName(), (Short) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Long) {
//				objectNode.put(tv.getName(), (Long) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Float) {
//				objectNode.put(tv.getName(), (Float) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Double) {
//				objectNode.put(tv.getName(), (Double) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Date) {
//				SimpleDateFormat sdf = new SimpleDateFormat(Element.DATETIME_PATTERN);
//				objectNode.put(tv.getName(), sdf.format((Date)defaultValue));
//				continue;
//			}
//			if(defaultValue instanceof String) {
//				objectNode.put(tv.getName(), (String) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Byte[]) {
//				objectNode.put(tv.getName(), defaultValue.toString().getBytes());
//				continue;
//			}
//			if(defaultValue instanceof Byte) {
//				objectNode.put(tv.getName(), (Byte) defaultValue);
//				continue;
//			}
//			if(defaultValue instanceof Property) {
//				JsonNode jsonNode = ElementMapper.getObjectMapper().valueToTree(defaultValue);
//				objectNode.put(tv.getName(), jsonNode);
//				continue;
//			}
			
		}
		return objectNode;
	}
	
	@Override
	public JsonNode getJsonQuery() throws Exception {
		ObjectNode objectNode = getParamsFromDefaultValues();
		return getJsonQuery(objectNode);
	}

	protected JsonNode replaceVariables(JsonNode jsonNode) throws Exception {
		Iterator<String> fieldNames = jsonNode.fieldNames();
		while(fieldNames.hasNext()) {
			String fieldName = fieldNames.next();
			JsonNode node = jsonNode.get(fieldName);
			switch (node.getNodeType()) {
				case OBJECT:
					node = replaceVariables(node);
					((ObjectNode) jsonNode).set(fieldName, node);
					break;
	
				case ARRAY:
					ArrayNode arrayNode = (ArrayNode) node;
			        for(int i = 0; i < arrayNode.size(); i++) {
			            JsonNode arrayElement = arrayNode.get(i);
			            arrayElement = replaceVariables(arrayElement);
			            arrayNode.set(i, arrayElement);
			        }
					break;
					
				case STRING:
					String value = node.asText();
					if(templateVariables.containsKey(value)) {
						JsonNode jn = params.get(value);
						if(jn == null) {
							throw new Exception("No value provided for " + value + " variables");
						}
						((ObjectNode) jsonNode).set(fieldName, jn);
					}
					break;
					
				default:
					break;
			}
		}
		return jsonNode;
	}
	
	@Override
	public JsonNode getJsonQuery(JsonNode values) throws Exception {
		this.params = values;
		JsonNode query = template.deepCopy();
		query = replaceVariables(query);
		return query;
	}

}
