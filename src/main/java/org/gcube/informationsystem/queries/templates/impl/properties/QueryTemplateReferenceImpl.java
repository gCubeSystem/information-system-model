package org.gcube.informationsystem.queries.templates.impl.properties;

import java.util.HashMap;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonTypeName;
import org.gcube.informationsystem.model.impl.properties.PropertyImpl;
import org.gcube.informationsystem.queries.templates.reference.properties.QueryTemplateReference;

/**
 * @author Luca Frosini (ISTI CNR)
 */
@JsonTypeName(value=QueryTemplateReference.NAME)
public class QueryTemplateReferenceImpl extends PropertyImpl implements QueryTemplateReference {
	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = -9079792255348990664L;

	protected String name;
	protected Map<String, Object> variables;

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public Map<String, Object> getVariables() {
		return variables;
	}

	@Override
	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	@Override
	public void addVariable(String name, Object value) {
		if(this.variables == null) {
			this.variables = new HashMap<String, Object>();
		}
		this.variables.put(name, value);
	}

}
