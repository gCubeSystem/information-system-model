package org.gcube.informationsystem.queries.templates.reference.entities;

import java.io.IOException;
import java.util.Map;

import org.gcube.com.fasterxml.jackson.annotation.JsonGetter;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnore;
import org.gcube.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.gcube.com.fasterxml.jackson.annotation.JsonSetter;
import org.gcube.com.fasterxml.jackson.core.JsonProcessingException;
import org.gcube.com.fasterxml.jackson.databind.JsonNode;
import org.gcube.com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.gcube.com.fasterxml.jackson.databind.node.ObjectNode;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.queries.templates.impl.entities.QueryTemplateImpl;
import org.gcube.informationsystem.queries.templates.reference.properties.TemplateVariable;
import org.gcube.informationsystem.types.annotations.ISProperty;
import org.gcube.informationsystem.types.reference.Change;
import org.gcube.informationsystem.types.reference.TypeMetadata;
import org.gcube.informationsystem.utils.Version;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonDeserialize(as=QueryTemplateImpl.class)
@TypeMetadata(name = QueryTemplate.NAME, description = "The type used to store Query Templates", version = Version.MINIMAL_VERSION_STRING)
@Change(version = Version.MINIMAL_VERSION_STRING, description = Version.MINIMAL_VERSION_DESCRIPTION)
public interface QueryTemplate extends EntityElement {
	
	public static final String NAME = "QueryTemplate"; //QueryTemplate.class.getSimpleName();
	
	public static final String NAME_PROPERTY = "name";
	public static final String DESCRIPTION_PROPERTY = "description";
	
	public static final String TEMPLATE_PROPERTY = "template";
	public static final String TEMPLATE_VARIABLES_PROPERTY = "templateVariables";
	
	@ISProperty(name = NAME_PROPERTY, description = "The name of the Query Template. Among UUID univocally identifiy the Query Template.", readonly = true, mandatory = true, nullable = false)
	public String getName();
	
	public void setName(String name);
	
	@ISProperty(name = DESCRIPTION_PROPERTY, description = "The description of the Query Template.", readonly = false, mandatory = true, nullable = false)
	public String getDescription();
	
	public void setDescription(String description);

	@JsonIgnore
	@ISProperty(name = TEMPLATE_PROPERTY, description = "The Query Template. It can contains query variables to be replaced to obtain a runnable query.", readonly = false, mandatory = true, nullable = false)
	public String getTemplateAsString() throws JsonProcessingException;
	
	@JsonIgnore
	public void setTemplate(String template) throws JsonProcessingException, IOException;
	
	@JsonSetter(value = TEMPLATE_PROPERTY)
	public void setTemplate(JsonNode template);
	
	@JsonGetter(value = TEMPLATE_PROPERTY)
	public JsonNode getTemplate();
	
	@JsonGetter(value = TEMPLATE_VARIABLES_PROPERTY)
	@ISProperty(name = TEMPLATE_VARIABLES_PROPERTY, description = "The Query Template Variables. It can contains Query Template Variable to be replaced to obtain a runnable query.", readonly = false, mandatory = true, nullable = false)
	public Map<String, TemplateVariable> getTemplateVariables();
	
	@JsonIgnore
	/**
	 * Adding variable which already exists, the previous value will be override.
	 * @param templateDefaultValue
	 */
	public void addTemplateVariable(TemplateVariable templateVariable);
	
	/**
	 * Create an ObjectNode which can be used as parameters to obtain a 
	 * JSON Query from the Query Template.
	 * It uses the default values provided in TemplateVariables.   
	 * @return the created object node
	 */
	@JsonIgnore
	public ObjectNode getParamsFromDefaultValues();
	
	/**
	 * @return the JsonQuery replacing the variables using the default values contained in TemplateVariables
	 * @throws Exception 
	 */
	@JsonIgnore
	public JsonNode getJsonQuery() throws Exception;
	
	/**
	 * @param values
	 * @return the JsonQuery replacing the variables using the provided values
	 * @throws Exception 
	 */
	@JsonIgnore
	public JsonNode getJsonQuery(JsonNode values) throws Exception;

	
	
}