package org.gcube.informationsystem.tree;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Node<T> implements Comparable<Node<T>> {

	public static String INDENTATION = "\t";
	
	protected T t;
	protected Tree<T> tree;
	protected Set<T> childrenElements;
	protected Set<T> descendantElements;
	
	protected Node<T> parent;
	protected Set<Node<T>> children;
	protected Set<Node<T>> descendants;
	
	public Node(T  t) {
		this.t = t;
		this.children = new TreeSet<>();
	}
	
	public Tree<T> getTree() {
		return tree;
	}

	void setTree(Tree<T> tree) {
		this.tree = tree;
	}
	
	public T getNodeElement() {
		return t;
	}
	
	public Node<T> getParent() {
		return parent;
	}
	
	public Node<T> setParent(Node<T> parent) {
		this.parent = parent;
		return this;
	}

	public Node<T> addChild(Node<T> child) {
		children.add(child);
		child.setParent(this);
		child.setTree(tree);
		return this;
	}

	public String getIdentifier() {
		return tree.getNodeInformation().getIdentifier(t);
	}

	public Set<Node<T>> getChildren() {
		return children;
	}

	public Set<Node<T>> getDescendants() {
		if(descendants == null) {
			descendants = new TreeSet<>();
			for (Node<T> child : children) {
				descendants.add(child);
				descendants.addAll(child.getDescendants());
			}
		}
		return descendants;
	}
	
	public Set<T> getChildrenElements() {
		if(childrenElements == null) {
			NodeInformation<T> ni = tree.getNodeInformation();
			childrenElements = new TreeSet<>(new Comparator<T>() {
				@Override
				public int compare(T t1, T t2) {
					return ni.getIdentifier(t1).compareTo(ni.getIdentifier(t2));
				}
			});
			for (Node<T> child : this.children) {
				childrenElements.add(child.t);
			}
		}
		return childrenElements;
	}

	public Set<T> getDescendantElements() {
		if(descendantElements == null) {
			descendantElements = new TreeSet<>(new Comparator<T>() {
				@Override
				public int compare(T t1, T t2) {
					NodeInformation<T> ni = tree.getNodeInformation();
					return ni.getIdentifier(t1).compareTo(ni.getIdentifier(t2));
				}
			});
			for (Node<T> child : children) {
				descendantElements.add(child.t);
				descendantElements.addAll(child.getDescendantElements());
			}
		}
		return descendantElements;
	}

	@Override
	public String toString() {
		return printTree(0).toString();
	}

	private StringBuffer printTree(int level) {
		
		StringBuffer stringBuffer = new StringBuffer();
		
		NodeElaborator<T> nodeElaborator = new NodeElaborator<T>() {

			@Override
			public void elaborate(Node<T> node, int level) throws Exception {
				for (int i = 0; i < level; ++i) {
					stringBuffer.append(Node.INDENTATION);
				}
				stringBuffer.append(tree.getNodeInformation().getIdentifier(node.getNodeElement()));
				stringBuffer.append("\n");
			}
						
		};
		
		try {
			elaborate(nodeElaborator, level);
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return stringBuffer;
	}

	@Override
	public int compareTo(Node<T> other) {
		if (this == other) {
			return 0;
		}
		if (other == null) {
			return -1;
		}
		if (getClass() != other.getClass()) {
			return -1;
		}
		
		return this.getIdentifier().compareTo(other.getIdentifier());
	}
	
	
	public void elaborate(NodeElaborator<T> nodeElaborator) throws Exception {
		elaborate(nodeElaborator, 0);
	}
	
	protected void elaborate(NodeElaborator<T> nodeElaborator, int level) throws Exception {
		nodeElaborator.elaborate(this, level);
		for (Node<T> child : children) {
			child.elaborate(nodeElaborator, level+1);
		}
	}
}
