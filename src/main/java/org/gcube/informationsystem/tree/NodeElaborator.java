package org.gcube.informationsystem.tree;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface NodeElaborator<T> {

	public void elaborate(Node<T> node, int level) throws Exception;

}
