package org.gcube.informationsystem.tree;

import java.util.Set;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public interface NodeInformation<T> {

	public String getIdentifier(T t);
	
	public Set<String> getParentIdentifiers(T root, T t);
	
}
