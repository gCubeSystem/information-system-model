This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Information System Model

## [v7.1.0-SNAPSHOT]

- Improved support for model knowledge [#25922]
- Additional fields (schema mixed) can be added to Context [#26102]
- Added support to define a specific implementation
- Added Event property
- Added support for generic Json Object and Json Array


## [v7.0.0]

- Reorganized E/R format [#24992]
- Added missing expected type field when the provided type does not have the exact class in the classpath 


## [v6.0.0]

- Modified models discovery to make it easier and more powerful [#24548]
- Discovery create trees of discovered types both globally and model based [#24548]
- Added possibility to declare a type Final which means that it cannot be extended [#24554]
- Fixed default value of propagation constraint of remove action for ConsistsOf to 'cascade' [#24223]
- Added "delete" propagation constraint property [#24222]
- Removed Encrypted Property Type and added Vault instead [#24655]


## [v5.0.0]

- Added the possibility to get AccessType from Type definition [#20695]
- Solved issue on unmarshalling of unknown types using best known type looking superclasses property [#20694]
- Aligned model with the model defined in Luca Frosini PhD dissertation [#20367]
- Using annotation to document the types with name, description, version and changelog [#20306][#20315]
- Generalized the way the type of a property is defined [#20516]
- Added reserved UUID management
- Added support for context names included in header among UUIDs [#22090]
- Added support for Json Query [#22047]
- Added support for Query Templates [#22091]


## [v4.1.0] [r5.0.0] - 2020-11-25

- Added support to include additional properties in Property types [#20012]


## [v4.0.0] [r4.26.0] - 2020-10-29

- Switched JSON management to gcube-jackson [#19116]


## [v3.0.0] [r4.21.0] - 2020-03-30

- Complete Model Reorganization
- Added annotations to define Resource Schema [#18213]
- IsIdenfifiedBy relation have been moved in gCube Model
- Renamed Embedded to Property [#13274]
- Renamed packages with name ending in 'embedded' to end with 'properties' [#13274]
- Removed some Property types which have been moved in gCube Model
- Added support For Encrypted Values [#12812]


## [v2.0.0] [r4.13.0] - 2018-11-20

- Added missing null checks when adding parent context in ContextImpl class
- Changed model packages
- Added RegistrationProvider which is used with ServiceLoader to dynamically discover models


## [v1.7.0] [r4.10.0] - 2018-02-26

- Added possibility to marshal list and array of ISManageable objects [#10796]


## [v1.6.0] [r4.9.0] - 2017-12-20

- Changed pom.xml to use new make-servicearchive directive [#10158]
- Added modifiedBy property in Header [#9999]
- Changed the way to marshall and unmarshall Context with parent and children [#10216]
- Improved IS Entity/Relation scanning to support multiple inheritance [#5706]


## [v1.5.0] [r4.6.0] - 2017-12-07

- Refactored getTypeByClass() method


## [v1.4.0] [r4.5.0] - 2017-06-07

- Defined a custom pattern for DateTimeFormat to support time-zone
- Added convenient methods to get desired facets or relations specifying class type
- Change the way to serialize Source resource which was ignored before. Now only the header of the Source resource is included which is enough to identify it. This enable the possibility to create a Relation together with the target entity


## [v1.3.0] [r4.3.0] - 2017-03-16

- Added deserialization support for unknown types with polymorphism support
- Improved support for json serilization and deserialization
- Added Propagation Constraint Concept


## [v1.2.0] [r4.2.0] - 2016-12-15

- Added regex support in schema definition
- Improved support for json serilization and deserialization


## [v1.1.0] [r4.1.0] - 2016-11-07

- Reorganized package and renamed relations to be compliant with well known practice
- Added Utility for json serilization and deserialization
- Added Jackson annotation to support json serilization and deserialization


## [v1.0.0] [r4.0.0] - 2016-07-28

- First Release

